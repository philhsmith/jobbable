require 'rubygems'
require 'rake'

begin
  require 'jeweler'
  Jeweler::Tasks.new do |gem|
    gem.name = "jobbable"
    gem.summary = %Q{AMQP Ruby Job Framework}
    gem.description = %Q{AMQP Ruby Job Framework}
    gem.email = "phil.h.smith@gmail.com"
    gem.homepage = "http://github.com/phs/jobbable"
    gem.authors = ["Phil Smith"]
    gem.bindir = "bin"
    gem.files =  FileList["[A-Z]*", "jobbable.gemspec", "{bin,lib,spec}/**/*"]
    gem.add_development_dependency "rspec", ">= 2.1.0"
    gem.add_development_dependency "relevance-rcov", ">= 0.9.2.1"
    gem.add_dependency 'activesupport', '>= 3.0.1'
    gem.add_dependency 'amqp', '>= 0.6.7'
    gem.add_dependency 'eventmachine', '>= 0.12.4'
  end
  Jeweler::GemcutterTasks.new
rescue LoadError
  puts "Jeweler (or a dependency) not available. " \
    "Install it with: gem install jeweler"
end

require 'rspec/core/rake_task'
RSpec::Core::RakeTask.new(:spec) do |spec|
  spec.pattern = ENV['TASK'] || 'spec/**/*_spec.rb'
end

task :spec => :check_dependencies

task :default => :spec

require 'rake/rdoctask'
Rake::RDocTask.new do |rdoc|
  version = File.exist?('VERSION') ? File.read('VERSION') : ""

  rdoc.rdoc_dir = 'rdoc'
  rdoc.title = "Jobbable #{version}"
  rdoc.rdoc_files.include('README*')
  rdoc.rdoc_files.include('lib/**/*.rb')
end
