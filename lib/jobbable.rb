require "rubygems"

gem "activesupport"
gem "eventmachine"
gem "amqp"

require "jobbable/facade"
require "jobbable/cli/command_line"
require "jobbable/client"
