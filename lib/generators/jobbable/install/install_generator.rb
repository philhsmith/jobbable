module Jobbable
  module Generators
    class InstallGenerator < Rails::Generators::Base

      def self.source_root
        @source_root ||= File.expand_path('../templates', __FILE__)
      end

      def create_configuration_file
        directory 'config', 'config'
      end

      def create_jobs_directory
        directory 'app', 'app'
      end

    protected

      def app_name
        Rails.application.class.name.split('::').first.underscore
      end

    end
  end
end
