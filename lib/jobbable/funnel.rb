require 'jobbable/agent'
require 'jobbable/amqp/publisher_mixin'
require 'jobbable/amqp/subscriber_mixin'

module Jobbable
  # An agent that aggregates and republishes messages.
  class Funnel < Agent
    include Amqp::SubscriberMixin
    include Amqp::PublisherMixin

    # Subscribe to our queues for messages to aggregate.
    #
    # On arrival, messages are handed to #perform.
    def on_start
      # Connect to AMQP
      start do
        # Subscribe to our queues, handing all messages to #perform.
        subscribe(agent_config.queues) { |message| perform message }
      end
    end

    # The un-acked messages awaiting aggregation and republishing.
    def pending_messages
      @pending_messages ||= []
    end

    # Aggregate and republish the #pending_messages together using the
    # configured routing key and exchanges.
    #
    # The aggregate message body is json, containing three special keys:
    # * original_keys: An array of the aggregated messages' routing keys
    # * original_exchanges: An array of the aggregated messages' originating
    #   exchanges
    # * messages: An array of the aggregated messages' bodies.
    #
    # #republish attempts to parse each pending message body as json before
    # including it in the 'messages' key.  If a pending message body fails to
    # parse, the raw body string itself is included instead.
    #
    # Only after republishing are the pending messages #ack'd and the list
    # cleared.
    def republish
      return if pending_messages.empty?

      # Collect the pending messages' routing keys and originating exchanges
      original_keys = pending_messages.collect { |m| m.header.routing_key }
      original_exchanges = pending_messages.collect { |m| m.header.exchange }

      # And now their message bodies.  Attempt to parse them as JSON, but
      # just include the raw bodies if they don't.
      messages = pending_messages.collect do |message|
        begin
          message.decoded_body
        rescue Jobbable::JSON::ParseError
          message.body
        end
      end

      # Republish!
      logger.info "Republishing #{pending_messages.count} messages"
      publish \
        :key => agent_config.key,
        :exchanges => agent_config.exchanges,
        :original_keys => original_keys,
        :original_exchanges => original_exchanges,
        :messages => messages

      # Ack and clear the pending messages and counts
      pending_messages.each(&:ack)
      pending_messages.clear
      message_counts.clear
    end

    # On message delivery timeout, republish the pending messages we have.
    def on_delivery_timeout
      republish
    end

    # Take a burst of work from all pop queues whose periods have elapsed.
    def on_heartbeat
      logger.debug counters
      pop_queues.each { |queue| pop queue }
    end

    # A Hash taking queues to the count of un-acked messages from that queue.
    def message_counts
      @message_counts ||= Hash.new { |h, k| h[k] = 0 }
    end

    # Record the message as pending and bump its #message_counts key.
    #
    # This method returns true if it appears to be time to republish the
    # pending messages.  Specifically, it returns true if:
    # * The passed message is from a subscribe queue who has maxed out its
    #   inflight limit
    # * The passed message is from a now-empty pop queue
    # * The total message count (across all queues) has reached the funnel's
    #   "max" configuration parameter.
    #
    # It returns false otherwise.
    def bump(message)
      pending_messages << message
      message_counts[message.queue] += 1

      queue_config = config.queues[message.queue]
      message_count = message_counts[message.queue]

      # Is this a subscribe queue whose maxed out its inflight?
      if queue_config.type == 'subscribe'
        return true if message_count >= queue_config.inflight
      end

      # Is this an empty pop queue?
      if queue_config.type == 'pop'
        return true if message.remaining_count <= 0
      end

      total = message_counts.values.inject(0) { |t, c| t + c }

      # Have we reached our max aggregate message size?
      return true if total >= agent_config.max

      # Not yet time to republish
      false
    end

    # Buffer incoming messages until they can be republished as one.
    def perform(message)
      EM.schedule do
        republish if bump message
      end
    end
  end
end
