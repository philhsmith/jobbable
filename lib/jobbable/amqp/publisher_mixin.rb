require 'jobbable/json'

require 'jobbable/amqp/configured_amqp_mixin'
require 'jobbable/config/slot_mixin'

module Jobbable
  module Amqp
    # Enables the mixer to publish messages to exchanges.
    #
    # The mixer must provide #logger and #config.
    module PublisherMixin
      include ConfiguredAmqpMixin
      include Config::SlotMixin

      # Create a routing key from the options and the job's first key routing
      # template.
      #
      # This method intentionally mutates its options hash, by removing:
      # * :state: substituted into {state}
      # * :attempt: substituted into {attempt}
      # * :priority: substituted into {priority}
      # * :host: substituted into {host}
      # * :tags: substituted into {tags}
      # * :template: a key template to use instead of the job's first
      #   configured one
      # * :key: an overriding key to return
      #
      # Any options not passed take on these defaults:
      # * :state: new
      # * :attempt: 0
      # * :priority: 0
      # * :host: any
      # * :tags: []
      #
      # If an overriding key is passed in with no other (reactive) options, it
      # is returned immediately.  If other options are present, then any slots
      # indicated by the template are first extracted from the key, to use as
      # defaults.  If the key can't be parsed (see SlotMixin#metadata_for), it
      # is returned immediately.  If a job name and a key are both passed, the
      # key must parse to the passed job for consideration.
      #
      # Values that are passed are subject to validation, which raises
      # ArgumentError on failure:
      # * :state: one of new, retry, fail, abort, crash. See SlotMixin::STATES
      # * :attempt: an integer 0 or greater
      # * :priority: an integer 0 to 9 inclusive
      # * :host: a string without any "*" or "#"s
      # * :tags: an array of strings without any "*", "#" or "."s.
      #
      # Any periods occuring in host will be translated to underscores (_).
      # Tags will will be sorted and joined with periods, or be represented
      # with a single underscore if no tags are included. Stars, pounds and
      # periods must be avoided in general as they have special meaning to
      # topic matching pattern syntax.
      #
      # Finally, it is invalid to specify the new state with any attempt other
      # than 0, or to specify retry with attempt 0.
      def routing_key(job, options = {})
        # To make the list of slot values, first take from any passed options.
        values = {}
        values[:state]    = options.delete(:state)    if options.key? :state
        values[:attempt]  = options.delete(:attempt)  if options.key? :attempt
        values[:priority] = options.delete(:priority) if options.key? :priority
        values[:host]     = options.delete(:host)     if options.key? :host
        values[:tags]     = options.delete(:tags)     if options.key? :tags

        # Get the key, if any
        key = options.delete(:key)

        # Get the template, if any
        template = options.delete(:template)

        # Was an overriding key the only thing passed in?  Just return that.
        return key if key and values.empty?

        # If an overriding key was passed, try to extract metadata from it
        # to merge with the passed values.
        #
        # After this block, job and values[:job] should both be set and agree
        if key
          # If the key can't be parsed, return it as-is
          return key unless from_key = metadata_for(config, key, :job => job)

          # and suppliment the passed values with those from the key
          values = from_key.merge values

          # Recover the job name if needed
          job ||= values[:job]
        elsif job
          # Otherwise just ensure to include :job in the values hash
          values[:job] = job
        end

        # Finally, include any missing defaults
        values = DEFAULTS.merge values

        # Take the job's first configured template if one wasn't passed
        template ||= config.jobs[values[:job]].keys.first

        # Validate state
        values[:state] = values[:state].to_s
        unless STATES.include? values[:state]
          raise ArgumentError.new("state #{values[:state].inspect} must be " \
            "one of #{STATES.join(', ')}")
        end

        # Validate priority
        if values[:priority].is_a?(String) and values[:priority] =~ /[0-9]+/
          values[:priority] = values[:priority].to_i
        end

        unless (0..9).include? values[:priority]
          raise ArgumentError.new("priority #{values[:priority].inspect} " \
            "must be a number from 0 to 9")
        end

        # Validate attempt
        if values[:attempt].is_a?(String) and values[:attempt] =~ /[0-9]+/
          values[:attempt] = values[:attempt].to_i
        end

        unless values[:attempt].is_a? Fixnum and 0 <= values[:attempt]
          raise ArgumentError.new("attempt #{values[:attempt].inspect} " \
            "must be a number 0 or more")
        end

        # Validate state / attempt combinations
        if template.include? '{state}' and template.include? '{attempt}'
          if values[:state] == 'new' and 0 < values[:attempt]
            raise ArgumentError.new("new jobs must have attempt 0")
          end

          if values[:state] == 'retry' and 0 == values[:attempt]
            raise ArgumentError.new("retried jobs need attempt 1 or more")
          end
        end

        # Validate, substitute and scrub host
        values[:host] = values[:host].to_s
        if values[:host].include? '*' or values[:host].include? '#'
          raise ArgumentError.new("host #{values[:host].inspect} may " \
            "not contain * or #")
        end
        values[:host] = hostname if values[:host] == 'this'
        values[:host] = values[:host].tr('.', '_')

        # Validate and pack tags
        %w{. * #}.each do |illegal|
          malformed = values[:tags].select { |tag| tag.include? illegal }
          raise ArgumentError.new("tags #{malformed.join(' ')} may not " \
            "contain #{illegal}") if malformed.any?
        end
        values[:tags] = values[:tags].sort.join('.')
        values[:tags] = '_' if values[:tags].empty?

        # Convert to string keys
        values = Hash[values.collect { |k, v| [k.to_s, v] }]

        # Substitute values into the template
        expand_slots template, values
      end

      # What exchanges should the given job be published to?
      #
      # This method intentionally mutates its options hash.
      def exchanges_for_job(job, options = {})
        # If :exchange and/or :exchanges is passed, remove and use them.
        # Otherwise use the exchanges configured on the job.
        exchanges = if options.key? :exchange or options.key? :exchanges
          [options.delete(:exchange)].compact +
            (options.delete(:exchanges) || [])
        else
          config.jobs[job].exchanges
        end

        # Expand the 'all' exchange
        exchanges = config.exchanges.keys if exchanges.include? 'all'

        exchanges
      end

      # Publish a job.
      #
      # Publishing a message ultimately requires three things: the exchanges
      # to publish to, the routing key to publish under and the message
      # itself. These may be passed directly with the :exchange (or
      # :exchanges), :key and :message options.
      #
      # As elsewhere, the exchanges are identified by their configuration
      # section name, and not their AMQP protocol name defined in the
      # exchange's "name" property.  If multiple exchanges are named, a
      # message will be published to all of them.
      #
      # If these options are not passed, :job must be included and name a job
      # configuration.  The missing pieces will be taken from there:
      # * the job's "exchanges" property determines the publishing exchanges
      # * the job's "key" property is used as the routing key template
      # * remaining, unrecognized options are serialized in a JSON hash to
      #   make the message body
      #
      # If a job's routing key template is used (or if :template is passed
      # instead of :key) then a substitution step is taken to create the
      # routing key from the template.  Occurances of "dynamic slots", which
      # are certain variable names enclosed in {}s, are replaced with either
      # passed options of the same name or default values. See #routing_key.
      #
      # Other options:
      # * :logger: a logger to use instead of the mixer's implicit logger.
      # * :priority: an integer from 0 to 9 inclusive.  This will be passed
      #   along as the AMQP message priority, but beware!  If your broker is
      #   RabbitMQ, don't expect priorities to do much.
      # * :content_type: the messages's amqp content type.  Defaults to
      #   application/json.
      # * :state: see #routing_key
      # * :attempt: see #routing_key
      # * :priority: see #routing_key
      # * :host: see #routing_key
      # * :tags: see #routing_key
      #
      # If you are publishing jobs from within other jobs, you may want to
      # take advantage of Flight's contextual support.  See Flight#publish.
      #
      # This will create an AMQP connection if needed.
      # See ConfiguredAmqp#with_connection.
      def publish(options = {})
        amqp.with_connection do
          # Avoid mutating the passed hash
          options = options.dup

          # Convert all string keys to symbols
          options.keys.select { |k| k.is_a? String }.each do |k|
            if options.key?(k.to_sym) and options[k] != options[k.to_sym]
              raise ArgumentError.new("passed both #{k.inspect} and " \
                "#{k.to_sym.inspect}")
            end
            options[k.to_sym] = options.delete k
          end

          # Pluck the job name
          job = options.delete(:job)

          # Choose the logger to use
          logger = options.delete(:logger) || self.logger

          # Cry about missing :job, unless we have all we need.
          unless options[:key] and (options[:exchange] or options[:exchanges])
            raise ArgumentError.new(":job is required") unless job
          end

          # Save the priority, content_type and overriding message, if any
          priority = options[:priority]
          content_type = options.delete(:content_type) || "application/json"
          message = options.delete(:message)

          # Get the exchanges to publish to.  This mutates options
          exchanges = exchanges_for_job(job, options)

          # Compute the job's routing key.  This mutates options
          key = routing_key job, options

          # The message is the JSON serialization of the remaining options.
          # If there are remaining options and an explicit message was passed,
          # attempt to merge the two.
          if message and !options.empty?
            begin
              from_message = Jobbable::JSON.decode message

              # The passed message is json, but is it a hash?  If not, do
              # nothing, which will ignore the remaining options and publish
              # the message as-is.
              if from_message.is_a? Hash
                # Symbolize keys
                from_message = Hash[from_message.collect do |k, v|
                  [k.to_sym, v]
                end]

                # Merge the remaining options, giving the options precedence.
                options = from_message.merge options

                # Forget that the message was passed, forcing us to serialize
                # the now-merged options to make the published message.
                message = nil
              end
            rescue Jobbable::JSON::ParseError => not_json
              logger.error "Could not parse overriding message to merge " \
                "remaining options.  Publishing override without alteration."
              logger.error not_json
            end
          end

          message ||= Jobbable::JSON.encode options

          # Put the actual publishing options back
          options = {
            :key          => key,
            :content_type => content_type
          }
          options[:priority] = priority.to_i if priority

          # Publish the message on each of the selected exchanges
          logger.info "Publishing #{key} to #{exchanges.join(', ')}"
          exchanges.each do |exchange|
            amqp.publish(exchange, message, options)
          end
        end
      end

    end
  end
end
