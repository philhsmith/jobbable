require 'eventmachine'

# Address an issue surrounding Basis.GetEmpty messages in the AMQP gem.
require "jobbable/monkey/amqp/get_empty"

require 'jobbable/amqp/configured_amqp_mixin'
require 'jobbable/amqp/message'

module Jobbable
  module Amqp
    # Enables the mixer to pull messages from queues.
    #
    # The mixer must provide #logger and #config.
    module SubscriberMixin
      include ConfiguredAmqpMixin

      # The last observed values for remaining message counts by pop queue.
      def remaining_pop_messages
        @remaining_pop_messages ||= {}
      end

      # Perform a burst's worth of work from the named queue.
      #
      # If the last reported message count is less than the burst, only the
      # remainder is requested. At least one message is always requested.
      def pop(queue)
        count = [
          config.queues[queue].burst,
          remaining_pop_messages[queue]
        ].compact.min
        count = 1 if count < 1
        count.times { amqp.queue(queue).pop }
      end

      # The pop queues currently subscribed to.
      def pop_queues
        @pop_queues ||= []
      end
      attr_writer :pop_queues

      # How long do we wait before giving up on new messages?
      def delivery_timeout
        @delivery_timeout ||= 10
      end
      attr_writer :delivery_timeout

      # The timer enforcing message delivery timeout.
      attr_accessor :delivery_timer

      # (Re)start the subscription delivery timeout.
      def restart_delivery_timeout
        if delivery_timer
          delivery_timer.cancel
          self.delivery_timer = nil
        end

        if respond_to?(:on_delivery_timeout) and (delivery_timeout || 0) > 0
          self.delivery_timer = EM::Timer.new(delivery_timeout) do
            on_delivery_timeout # I'm bored! You're boring, zoidberg
          end
        end
      end

      # Subscribe to the given queues, passing incoming messages to the block.
      #
      # The special queue 'all' is expanded to all configured queues.
      #
      # The passed queues with type 'pop' are recorded in #pop_queues.  When
      # receiving messages from pop queues, the queue's remaining message
      # count is recorded in #remaining_pop_messages.
      #
      # Messages sent by the broker to indicate a popped queue is empty are
      # ignored.
      #
      # If needed, a connection is established.
      # See ConfiguredAmqp#with_connection.
      def subscribe(queues, &block)
        amqp.with_connection do
          queues = config.queues.keys.sort if queues.include? 'all'
          pop_queues.concat(queues.select do |queue|
            config.queues[queue].type == 'pop'
          end)

          # Start the message delivery timeout
          restart_delivery_timeout

          queues.each do |queue|
            amqp.subscribe(queue) do |header, body|
              # Ignore vacuous messages coming from empty queues
              if header.nil? or header.empty?
                remaining_pop_messages[queue] = 0
              else
                counters.bump :started

                # Record the number of remaining messages from pop queues
                if config.queues[queue].type == 'pop'
                  remaining_pop_messages[queue] = header.message_count
                end

                # Restart the timeout
                restart_delivery_timeout

                block.call Message.new(self, queue, header, body)
              end
            end
          end
        end
      end

      # Unsubscribe from the given queues.
      #
      # The special queue 'all' is expanded to all configured queues.
      #
      # The passed queues with type 'pop' are removed from #pop_queues.
      def unsubscribe(queues)
        queues = config.queues.keys.sort if queues.include? 'all'
        queues.each do |queue|
          pop_queues.delete queue
          amqp.unsubscribe queue
        end
      end

      # A hash to track some basic statistics.
      def counters
        @counters ||= Hash.new { |h, k| h[k] = 0 }.tap do |counters|
          class <<counters

            # Increment the named counter by 1 on the reactor.
            def bump(key)
              EM.schedule { self[key] += 1 }
            end

            # Format the counters for display.
            def to_s
              names = keys.sort_by { |name| name.to_s }

              # How wide are the names and gaps rounded up to a multiple of 4?
              name_width = names.collect { |name| name.to_s.size }.max || 0
              name_width += 1
              name_width += (-name_width % 4)

              # How wide are the values, rounded up to a multiple of 4 - 1?
              value_width = values.collect { |value| value.to_s.size }.max || 0
              value_width += (-(value_width + 1) % 4)

              names.collect do |name|
                "#{name.to_s.ljust(name_width)}" \
                  "#{self[name].to_s.rjust(value_width)}"
              end.join(" ")
            end

          end

          # Initialize some counters off the bat (by referencing them)
          counters[:started]
          counters[:acked]
        end
      end

    end
  end
end
