require 'jobbable/logger'
require 'jobbable/json'

module Jobbable
  module Amqp
    # An AMQP message that is aware of its originating subscriber.
    class Message

      def initialize(subscriber, queue, header, body, acked = true)
        @subscriber, @queue, @header, @body, @acked =
          subscriber, queue, header, body, acked
      end

      # The subscriber that created this message.
      attr_reader :subscriber

      # The name of the queue this message was taken from.
      attr_reader :queue

      # The AMQP header associated with the message.
      attr_reader :header

      # The AMQP body associated with the message.
      attr_reader :body

      # What JSON-decoded message was supplied in the body?
      def decoded_body
        @decoded_body ||= begin
          Jobbable::JSON.decode body
        rescue Jobbable::JSON::ParseError
          logger.error "Unparsable message #{body.inspect}"
          raise
        end
      end

      # Has the body already been JSON decoded?
      def decoded?
        !!@decoded_body
      end

      # Does the queue expect an ack back?
      def acked?
        @acked
      end

      # This message's id for purposes of reporting, etc.
      #
      # This is not globally unique.
      def id
        "#{header.routing_key}:#{object_id}"
      end

      # The owning subscriber's config.
      def config
        subscriber.config
      end

      # The owning subscriber's logger.
      def original_logger
        subscriber.logger
      end

      # The owning subscriber's counters.
      def counters
        subscriber.counters
      end

      # The logger.
      #
      # This is not (by default) the subscriber logger.  Instead it is an
      # adapter that slips the message id into logged messages.
      def logger
        @logger ||= PrefixLogger.new(original_logger) { id.to_s }
      end
      attr_writer :logger

      # Called when messages are acked.
      #
      # Does nothing, but may be overrode.
      def on_ack
      end

      # Ack the message, retiring it.
      #
      # Once acked, the #on_ack is called.
      def ack
        header.ack if acked?
        counters.bump :acked
        on_ack
      end

      # The number of remaining messages on the (pop) queue.
      #
      # This is only defined for messages delivered from pop queues.
      # It is nil otherwise.
      def remaining_count
        header.message_count if config.queues[queue].type == 'pop'
      end

    end
  end
end
