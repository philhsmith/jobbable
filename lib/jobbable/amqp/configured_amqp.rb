require 'eventmachine'
require 'monitor'

# Address a reconnection delay issue in the AMQP gem.
require "jobbable/monkey/amqp/reconnect_delay"

require 'jobbable/facade'

module Jobbable
  module Amqp
    # A facade for MQ that is aware of jobbable configuration.
    #
    # Many methods of this class deal with queues, exchanges and bindings by
    # name.  Unless specifically called out otherwise, these names are the
    # subsection labels in the jobbable configuration.  For example, consider
    # a snippet of YAML:
    #
    #   queues:
    #     the_name_you_want:
    #       name: the_name_appearing_on_the_amqp_wire
    #       inflight: 5
    #
    # When referring to this queue (in #bind, for example), the correct name
    # is "the_name_you_want", not "the_name_appearing_on_the_amqp_wire".
    class ConfiguredAmqp
      include MonitorMixin

      attr_reader :config

      def initialize(config)
        super()
        @config = config
      end

      # The logger.
      def logger
        Jobbable.logger
      end

      # The AMQP connection.
      attr_reader :connection
      def connection=(value)
        (@connection = value).tap do
          channels.clear
          bound_exchanges.clear
        end
      end

      # Is there an existing connection?
      def connected?
        !connection.nil?
      end

      # The Eventmachine thread.
      attr_accessor :em_thread

      # The monitor condition variable signaling EM has stopped.
      attr_accessor :em_stopped

      # How long to wait (in seconds) until reconnecting after a disconnect.
      def reconnect_delay
        @reconnect_delay ||= 1
      end
      attr_writer :reconnect_delay

      # Halve #reconnect_delay, making it no smaller than 1.
      #
      # If the new delay is greater than 1, restart the relaxation timer.
      def relax_reconnect_delay
        self.reconnect_delay = [reconnect_delay / 2, 1].max
        self.relax_reconnect_delay_timer = nil
        start_relax_reconnect_delay_timer if 1 < reconnect_delay
      end

      # A timer (signature) calling #relax_reconnect_delay.
      attr_accessor :relax_reconnect_delay_timer

      # Start a timer calling #relax_reconnect_delay.
      def start_relax_reconnect_delay_timer
        self.relax_reconnect_delay_timer ||= EM.add_timer(31) do
          relax_reconnect_delay
        end
      end

      # How should we react to disconnects?
      #
      # Specifically, should we attempt to reconnect?  This should return true
      # to reconnect immediately, false to not reconnect at all, or a second
      # delay to do so only after that interval of time.
      #
      # This method doubles and returns #reconnect_delay, as well as starting
      # the timer used to bring it back down.
      #
      # This requires the AMQP patch in jobbable/monkey/amqp/reconnect_delay
      # (which should be loaded automatically.)
      #
      # See also
      # http://github.com/tmm1/amqp/issues#issue/3
      # http://github.com/melito/amqp/commit/e206ee1
      def on_disconnect
        start_relax_reconnect_delay_timer
        (self.reconnect_delay *= 2).tap do |delay|
          logger.warn "Disconnected from AMQP, retrying in #{delay} seconds."
        end
      end

      # Create a new AMQP connection.
      #
      # If a block is passed, it will be invoked once the connection is made.
      #
      # This will fire up eventmachine in a new thread if necessary.
      def start(&block)
        if connected?
          block.call if block
          return
        end

        connect_params = {
          :host       => config.host,
          :port       => config.port,
          :vhost      => config.virtual_host,
          :reconnect  => lambda { on_disconnect }
        }

        connect_params[:user]    = config.username     if config.username
        connect_params[:pass]    = config.password     if config.password
        connect_params[:logging] = config.amqp_logging if config.amqp_logging

        if config.connection_timeout
          connect_params[:timeout] = config.connection_timeout
        end

        scrubbed_password = connect_params.dup
        scrubbed_password.delete(:reconnect) # Proc#to_s is ugly

        if scrubbed_password.key? :pass
          scrubbed_password.merge!(:pass => '***')
        end

        logger.info "Connecting to AMQP at #{scrubbed_password.inspect}"

        # Make some condition variables gating entry and exit if starting EM
        if EM.reactor_running?
          em_started = nil
          self.em_stopped = nil
        else
          em_started = new_cond
          self.em_stopped = new_cond
        end

        # This will connect and signal EM has started (if we are starting it)
        connect = lambda do
          MQ.error { |message| logger.error message }

          AMQP.connect(connect_params).callback do |connection|
            logger.debug "Connected to AMQP"
            self.connection = connection

            # Call the block *after* the connection has been set.
            block.call if block

            # If someone is waiting on us, signal them to continue.
            synchronize { em_started.signal } if em_started
          end
        end

        # If we need to start EM, do so and block
        if not EM.reactor_running?
          synchronize do

            # The new thread
            self.em_thread = Thread.new do
              begin
                logger.info "Starting eventmachine in a new thread"
                EM.error_handler { |exception| logger.error exception }
                EM.run(&connect)

                # Once EM has finished, signal its exit
                synchronize { em_stopped.signal } if em_stopped
              ensure
                logger.info "Exiting eventmachine"
                self.connection = nil
              end
            end

            # And the block.  The signal is up in the AMQP.connect callback
            em_started.wait
          end
        else
          # EM is already running, just connect on the reactor thread
          EM.schedule(&connect)
        end
      end

      # Close the AMQP connection.
      #
      # If #start fired up eventmachine in a new thread, it is shut down.
      #
      # If a block is passed, it is invoked once the connection is closed.
      def stop(&block)
        unless connected?
          block.call if block
          return
        end

        logger.info "Closing AMQP connection"

        # This closes the connection and signals EM to exit if we started it.
        close = lambda do
          connection.close do
            # Clear the connection
            self.connection = nil

            logger.debug "Connection closed"

            # Stop EM if we started it
            if em_thread
              logger.debug "Stopping eventmachine"
              EM.stop_event_loop
            end

            block.call if block
          end
        end

        # If we created the EM thread, wait until it exits
        if em_thread
          synchronize do
            # Close the connection
            close.call

            # Wait for it clean up, and then clear the thread
            em_stopped.wait
            self.em_thread = nil
          end
        else
          # If we aren't tearing down EM, just disconnect.
          close.call
        end
      end

      # Run the given block with an amqp connection, creating one if needed.
      #
      # Establishing the connection takes place within eventmachine, and so
      # will be asynchronous (unless #start explictly fires up and waits for
      # EM.) As such do not expect #with_connection to run the block
      # immediately or return its value.
      #
      # Note that none of the other methods use #with_connection, it is up to
      # client code to guard their calls as necessary.
      def with_connection(&block)
        if connected?
          block.call
        else
          start(&block)
        end
        nil
      end

      # AMQP channels, organized by name.
      def channels
        @channels ||= {}
      end
      attr_writer :channels

      # Get an AMQP channel by name, creating it if necessary.
      #
      # A NotConnectedError is raised unless a connection has been made with
      # #start.
      def channel(name = 'default')
        raise NotConnectedError.new("Not connected") unless connection
        channels[name] ||= MQ.new(connection)
      end

      # Materialize a configured exchange from its configuration name.
      def exchange(exchange)
        exchange_config = config.exchanges[exchange]

        options = Hash[%w{passive durable auto_delete internal}.collect do |p|
          [p.to_sym, exchange_config[p]]
        end]

        options[:nowait]      = !exchange_config.wait
        options[:no_declare]  = !exchange_config.declare

        # Beware, channel.send is not Object#send.
        case exchange_config.type
        when "topic"
          channel.topic  exchange_config.name, options
        when "fanout"
          channel.fanout exchange_config.name, options
        when "direct"
          channel.direct exchange_config.name, options
        else
          logger.error "Unknown exchange type #{exchange_config.type} "\
            "for #{exchange}"
        end
      end

      # Fetch the status of a queue by its configuration name.
      #
      # #queue_status may be called either synchronously or asynchronously,
      # asynchronous invocation is used if the optional callback block is
      # passed.
      #
      # If an asynchronous callback is provided, it is called once the status
      # information arrives.  If the callback takes a single argument, the
      # remaining message count will be passed.  If it takes two, the number
      # of consumers will also be passed.
      #
      # If no callback is provided, execution blocks until the status
      # information arrives.  In this case the remaining message count is
      # returned.  If a synchronous invocation is attempted on the EM reactor
      # thread, a DeadlockError is raised instead.
      #
      # If the queue does not yet exist, it will not be created.
      #
      # See MQ::Queue#status in the amqp gem.
      def queue_status(queue, options = {}, &block)
        if EM.reactor_thread? and block.nil?
          raise DeadlockError.new("refusing to block reactor thread")
        end

        queue_config = config.queues[queue]

        # If we have an asynchronous callback, just hand it off and be done.
        # Note that :passive => true ensures absent queues are not created.
        if block
          channel.queue(queue_config.name, :passive => true).status(&block)
          return
        end

        # The remaining message count that is ultimately returned
        message_count = nil

        # Create a condition variable gating when the status comes in
        status_received = new_cond
        synchronize do
          # 1. Start a timeout to save ourselves in case of wire error
          timer = EM::Timer.new(options[:timeout] || 5) do
            synchronize { status_received.signal }
          end

          # 2. Then make the request..
          channel.queue(queue_config.name, :passive => true).status do |c|
            # 4. EM reactor records the count and releases the CV
            message_count = c
            synchronize { status_received.signal }
          end

          # 3. Wait for the EM thread to handle the response
          status_received.wait

          # 5. Control resumes on the original thread, which cancels the timer
          # and returns the count
          timer.cancel
        end

        message_count
      end

      # Materialize a configured queue from its configuration name.
      #
      # We associate a channel with each queue so that we may enforce
      # per-queue QoS prefetch_count limits (which we expose as the inflight
      # configuration option.)
      def queue(queue)
        queue_config = config.queues[queue]

        options = Hash[%w{passive durable exclusive auto_delete}.collect do |p|
          [p.to_sym, queue_config[p]]
        end]

        options[:nowait] = !queue_config.wait

        channel(queue).queue queue_config.name, options
      end

      # Bind the given queue to the given exchange.
      def bind(exchange, queue, options = {})
        bind_options = {}
        bind_options[:key]    =   options[:key]   if options.key? :key
        bind_options[:nowait] =  !options[:wait]  if options.key? :wait

        logger.debug "Binding #{queue} to #{exchange}"
        queue(queue).bind exchange(exchange).name, bind_options
      end

      # Unbind the given queue from the given exchange.
      def unbind(exchange, queue, options = {})
        unbind_options = {}
        unbind_options[:key]    =   options[:key]   if options.key? :key
        unbind_options[:nowait] =  !options[:wait]  if options.key? :wait

        logger.debug "Unbinding #{queue} from #{exchange}"
        queue(queue).unbind exchange(exchange).name, unbind_options
      end

      # Bind all queues configured on the given exchange.
      def bind_queues(exchange)
        logger.info "Binding queues to #{exchange}"

        binds = config.binds.select do |name, bind|
          ['all', exchange].include? bind.exchange
        end

        binds.each do |name, bind|
          options = {
            :key  =>  bind.key,
            :wait =>  bind.wait
          }

          # Expand 'all' queues
          queues = bind.queue == 'all' ? config.queues.keys : [bind.queue]

          # Loop over selected queues and exchanges, call bind
          queues.each do |queue|
            bind exchange, queue, options
          end
        end
      end

      # The set of exchanges that have had their queues bound this connection.
      def bound_exchanges
        @bound_exchanges ||= {}
      end
      attr_writer :bound_exchanges

      # Ensure configured queues are bound to the given exchange.
      #
      # This delegates to #bind_queues on the first invocation for a given
      # exchange within a connection.  Subsequent calls for the same exchange
      # do nothing.
      def ensure_bound(exchange)
        bound_exchanges[exchange] ||= begin
          bind_queues exchange
          true
        end
      end

      # Subscribe to the named queue with the given block.
      #
      # Despite the name, this also registers callbacks for synchronous "pop"
      # queues.
      def subscribe(queue, &block)
        logger.info "Subscribing to queue #{queue}"

        queue_config = config.queues[queue]

        # TODO: do we care about exposing these parameters?
        options = {
          :ack    => true,
          :nowait => false
        }

        case queue_config.type
        when 'subscribe'
          name, queue = queue, queue(queue)

          # Cancel any previous subscription, since we can't have two and
          # the block from the latest invocation should win.
          if queue.subscribed?
            logger.warn "Dropping previous subscription on queue #{name}"
            queue.unsubscribe
          end

          # Set the prefetch on this channel to the queue's inflight limit.
          channel(name).prefetch(queue_config.inflight)

          queue.subscribe(options, &block)
        when 'pop'
          queue(queue).pop(options, &block)
        else
          logger.error "Unknown queue type #{queue_config.type} for #{queue}"
        end
      end

      # Unsubscribe from the named queue.
      #
      # Do nothing for unsubscribed or pop queues (which do not have
      # persistent subscriptions).
      def unsubscribe(queue)
        logger.info "Unsubscribing from queue #{queue}"

        return unless config.queues[queue].type == 'subscribe'

        queue = queue(queue)
        queue.unsubscribe if queue.subscribed?
      end

      # Publish a message to the named exchange.
      def publish(exchange, message, options = {})
        ensure_bound exchange unless options.delete(:bind) == false
        exchange(exchange).publish message, options
      end

    end
  end
end
