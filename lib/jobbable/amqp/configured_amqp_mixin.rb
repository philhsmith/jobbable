require 'jobbable/amqp/configured_amqp'

module Jobbable
  module Amqp
    # Enables the mixer to establish and use configured AMQP connections.
    #
    # The mixer must provide #logger and #config.
    module ConfiguredAmqpMixin

      # A configuration-aware AMQP connection.
      def amqp
        @amqp ||= ConfiguredAmqp.new(config)
      end
      attr_writer :amqp

      # Open the underlying AMQP connection.
      #
      # See ConfiguredAmqp#start.
      def start(&block)
        amqp.start(&block)
      end

      # Close the underlying AMQP connection.
      #
      # See ConfiguredAmqp#stop.
      def stop(&block)
        amqp.stop(&block)
      end

      # Fetch the status of a queue by name.
      #
      # See ConfiguredAmqp#queue_status.
      #
      # This will create an AMQP connection if needed.
      # See ConfiguredAmqp#with_connection.
      def queue_status(queue, options = {}, &block)
        amqp.with_connection do
          amqp.queue_status(queue, options, &block)
        end
      end

    end
  end
end
