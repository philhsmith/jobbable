# We don't use OrderedHash, but we find AS::JSON occasionally wants it
require 'active_support/ordered_hash'
require 'active_support/json'

module Jobbable
  # A facade wrapping an underlying json encoder/decoder.
  module JSON

    # Encode the given object as a JSON string.
    def encode(o)
      o == {} ? "{}" : ActiveSupport::JSON.encode(o)
    end

    # This is mixed into decoding errors for the convenience of rescuers.
    module ParseError
    end

    # Decode the given json string into an object.
    #
    # Any raised StandardErrors will include Jobbable::JSON::ParseError.
    def decode(json)
      # Shamelessly stolen from ActiveSupport::JSON::Encoder.escape
      escaped = json.gsub(/([\xC0-\xDF][\x80-\xBF]|
                            [\xE0-\xEF][\x80-\xBF]{2}|
                            [\xF0-\xF7][\x80-\xBF]{3})+/nx) do |s|
        s.unpack("U*").pack("n*").unpack("H*")[0].gsub(/.{4}/n, '\\\\u\&')
      end

      escaped = escaped.gsub(/([\x80-\xFF])/nx) do |s|
        '\\u' + s.unpack("C").pack("n").unpack("H*")[0]
      end

      begin
        ActiveSupport::JSON.decode escaped
      rescue StandardError => parse_error
        # Mix in our facade exception
        parse_error.extend ParseError

        # and re-raise
        raise
      end
    end

    # Make instance methods class methods also
    extend self

  end
end
