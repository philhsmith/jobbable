module Jobbable
  # A mixin to easily open the configured logger.
  #
  # The mixer must provide #config.
  module LoggerMixin

    # Will writing to the configured log file be a problem?
    def check_log_file
      return if %w{syslog none -}.include? config.log_file

      log_dir = File.dirname(config.full_log_file)

      if File.exist?(config.full_log_file)
        raise CantOpenLogError.new(
          "#{config.full_log_file} isn't writable"
        ) unless File.writable?(config.full_log_file)
      elsif File.exist?(log_dir)
        unless File.writable?(log_dir)
          raise CantOpenLogError.new("#{log_dir} isn't writable")
        end
      else
        raise CantOpenLogError.new("#{log_dir} doesn't exist")
      end
    end

    # Install the configured logger.
    def open_logger
      # Can we write to the new file?
      check_log_file

      # Set the new logger
      Jobbable.logger = case config.full_log_file
      when 'syslog'
        SyslogLogger.new
      when 'none'
        NULL_LOGGER
      when '-'
        CONSOLE_LOGGER
      else
        PrefixLogger.new(IOLogger.new(config.full_log_file)) do |m, s|
          "#{Time.now.strftime('%b %d %H:%M:%S')} " \
            "#{Process.pid.to_s.rjust(5, ' ')} " \
            "#{s.to_s.upcase.rjust(5, ' ')}"
        end
      end
    end

  end

  # A mixin to format exceptions with or without backtraces.
  module ExceptionFormatter
    # Should backtraces be logged in addition to exception messages?
    attr_accessor :logs_backtraces

    # Format the given exception with its backtrace.
    def format_exception(exception)
      if logs_backtraces == false or !exception.is_a? Exception
        exception.to_s
      else
        lines = ["#{exception} (#{exception.class})"]
        lines += (exception.backtrace || ["outta nowhere"]).collect do |f|
          "\tfrom #{f}"
        end
        lines.join("\n")
      end
    end
  end

  # A logger that sends messages to the passed IO or named file.
  class IOLogger
    include ExceptionFormatter

    attr_reader :destination

    def initialize(destination = nil)
      @destination = destination
    end

    [:debug, :info, :warn, :error, :fatal].each do |severity|
      define_method severity do |message|
        message = format_exception(message) if message.is_a? Exception
        io.puts message
      end
    end

    def io
      @io ||= open
    end
    attr_writer :io

    def open
      if destination.is_a? String
        File.open(destination, "a").tap { |f| f.sync = true }
      elsif destination.is_a? IO
        destination
      else
        STDOUT
      end
    end

    def close
      if @io
        @io.close unless [STDOUT, STDERR].include? @io
        @io = nil
      end
    end
  end

  # The logger that sends things to STDOUT.
  #
  # It omits exception stack traces.
  CONSOLE_LOGGER = IOLogger.new.tap do |logger|
    logger.logs_backtraces = false
  end

  # The logger that sends things to STDERR.
  #
  # It omits exception stack traces.
  STDERR_LOGGER = IOLogger.new(STDERR).tap do |logger|
    logger.logs_backtraces = false
  end

  # The logger that swallows everything.
  NULL_LOGGER = Object.new.tap do |logger|
    class <<logger
      include ExceptionFormatter

      [:debug, :info, :warn, :error, :fatal].each do |severity|
        define_method severity do |message|
        end
      end

      def close; end
    end
  end

  # A logger adapter that includes a prefix on each message.
  class PrefixLogger
    attr_accessor :wrapped, :prefix

    def initialize(wrapped, &prefix)
      @wrapped, @prefix = wrapped, prefix
    end

    [:debug, :info, :warn, :error, :fatal].each do |severity|
      define_method severity do |message|
        message = format_exception(message) if message.is_a? Exception
        @wrapped.send(severity,
          [prefix_for(message, severity), message].compact.join(' '))
      end
    end

    def close
      wrapped.close
    end

    def format_exception(exception)
      if wrapped.respond_to? :format_exception
        wrapped.format_exception(exception)
      else
        exception
      end
    end

    def prefix_for(message, severity)
      return nil unless prefix

      prefix_for_message = prefix.call(*[message, severity][0...prefix.arity])
      if prefix_for_message and prefix_for_message.empty?
        prefix_for_message = nil
      end
      prefix_for_message
    end
  end

  # A logger that sends to Syslog.
  class SyslogLogger
    include ExceptionFormatter

    {
      :fatal  => :crit,
      :error  => :err,
      :warn   => :warning,
      :info   => :info,
      :debug  => :debug
    }.each do |local_method, syslog_method|
      define_method local_method do |message|
        open unless open?
        message = format_exception(message) if message.is_a? Exception

        # Syslog interprets printf format strings, which we don't want.
        # So, escape them
        Syslog.send syslog_method, message.to_s.gsub("%", "%%")
      end
    end

    def open?
      defined? Syslog and Syslog.opened?
    end

    def open
      require 'syslog'
      close
      Syslog.open 'jobbable'
    end

    def close
      Syslog.close if open?
    end
  end
end
