require 'jobbable/configuration'

module Jobbable
  # Enables mixers to load jobbable configuration.
  module ConfigMixin

    # The directory to resolve relative paths from.
    attr_accessor :working_directory

    # The environment to load from jobbable configuration.
    def environment
      @environment || "development"
    end
    attr_writer :environment

    # The jobbable configuration used by this client.
    def config
      unless @config
        @config = Configuration.new
        configure!
      end

      @config
    end
    attr_writer :config

    # The config file to load.
    #
    # Unless explicitly set, config/jobbable.yml is used if it exists.
    # If it does not, ./jobbable.yml will be tried.
    # If that does not exist, no config file will be used.
    def config_file
      @config_file ||= begin
        in_config = File.expand_path('config/jobbable.yml', working_directory)
        in_starting = File.expand_path('jobbable.yml', working_directory)

        if File.exist?(in_config)
          in_config
        elsif File.exist?(in_starting)
          in_starting
        else
          nil
        end
      end
    end
    attr_writer :config_file

    # Command line options to configure with.
    def argv
      @argv || []
    end
    attr_writer :argv

    # Command-specific options left over after general command line options.
    def command_argv
      @command_argv ||= []
    end
    attr_writer :command_argv

    # Reload configuration.
    def configure!
      self.command_argv = config.configure!(
        environment, config_file, argv, working_directory)
    end

  end
end
