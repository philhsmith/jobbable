require 'jobbable/flight'
require 'jobbable/monkey/core/exception'

# Capture flights that raise exceptions bubbling through proc calls.
#
# If a proc originating from a flight (or a module mixed into a flight) raises
# an exception, then we should be able to find the flight as the 'self' in the
# proc's binding.  Or barring that, we may find it as the @flight ivar on that
# bound self.
#
# This holds even if the proc was *called* elsewhere, such as within EM as a
# callback.
#
# If we detect a flight, it is set on the exception which is then re-raised.
class Proc

  def call_with_flight_capture(*args)
    begin
      call_without_flight_capture(*args)
    rescue Exception => exception
      capture_flight_on(exception)
      raise
    end
  end

  unless instance_methods.include? "call_without_flight_capture"
    alias_method :call_without_flight_capture, :call
    alias_method :call, :call_with_flight_capture
  end

  def braces_with_flight_capture(*args)
    begin
      braces_without_flight_capture(*args)
    rescue Exception => exception
      capture_flight_on(exception)
      raise
    end
  end

  unless instance_methods.include? "braces_without_flight_capture"
    alias_method :braces_without_flight_capture, :[]
    alias_method :[], :braces_with_flight_capture
  end

  def capture_flight_on(exception)
    exception.raising_flight ||= begin
      bound_self = eval("self", binding)
      [
        bound_self,
        bound_self.instance_variable_get("@flight")
      ].detect do |flight|
        flight.is_a?(Jobbable::Flight)
      end
    end
  end

end
