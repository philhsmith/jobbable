# Add an attribute to record the flight that raised this exception, if any.
class Exception
  attr_accessor :raising_flight
end
