require 'mq'

# The AMQP protocol does not easily distinguish incorrect credentials from
# other events that break the connection.  While we normally want to
# reconnect, obviously on a bad user/pass we do not want to do so immediately
# as this would become an accidental DOS attack on the broker.
#
# A simple solution is to add a delay before reconnecting.  This monkey patch
# does so, heavily inspired by and/or stolen from
# http://github.com/melito/amqp/commit/e206ee1
#
# By all means head over and bump http://github.com/tmm1/amqp/issues#issue/3
# if my hacking into the amqp gem annoys you.
module AMQP
  class <<self

    # Add the :reconnect key to the default settings hash.
    #
    # It may be a boolean, numeric, or a parameter-less proc returning a
    # boolean or numeric.  It dictates the policy for reconnecting after an
    # unexpected disconnect:
    # * true:    reconnect immediately (default)
    # * false:   do not reconnect at all
    # * Numeric: reconnect after the delay in seconds
    #
    # If passed, a proc will be called per-disconnect.
    def settings_with_reconnect
      @settings ||= settings_without_reconnect.merge(:reconnect => true)
    end

    # And the patch
    unless methods.include? "settings_without_reconnect"
      alias_method :settings_without_reconnect, :settings
      alias_method :settings, :settings_with_reconnect
    end

  end

  module Client

    # Reconnect to the AMQP broker in the configured way.
    #
    # Note this approach is somewhat different from melito's patch above.
    # Particularly, if the client is not configured to reconnect, the
    # (original) #reconnect method is never called.
    def reconnect_with_policy(force = false)
      again = @settings[:reconnect]
      again = again.call if again.is_a? Proc

      if again == false
        # Do not reconnect
        raise Error, "Could not reconnect to server " \
          "#{@settings[:host]}:#{@settings[:port]}"
      elsif again.is_a?(Numeric)
        # Reconnect after a delay
        EM.add_timer(again) { reconnect_without_policy(true) }
      elsif again or again.nil?
        # Reconnect immediately
        reconnect_without_policy(true)
      else
        raise Error, "Don't understand reconnection action #{again.inspect}"
      end
    end

    # The patch
    unless instance_methods.include? "reconnect_without_policy"
      alias_method :reconnect_without_policy, :reconnect
      alias_method :reconnect, :reconnect_with_policy
    end

  end
end
