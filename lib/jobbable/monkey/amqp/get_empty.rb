require 'mq'

# AMQP may deliver vacuous messages for Get (pop) requests to empty queues.
#
# When this occurs, the amqp gem will deliver the message, since queue
# emptiness is an interesting fact.  The header object returned to client code
# however will contain a hidden nil ivar, making virtually every method call
# result in a NoMethodError exception.
#
# This monkey patch adds MQ::Header#empty? that clients can safely call to
# distinguish this case.
#
# By all means head over and bump http://github.com/tmm1/amqp/issues#issue/23
# if my hacking into the amqp gem annoys you.
class MQ
  class Header

    # Is the associated message a Basic.GetEmpty?
    #
    # This holds when a synchronous, pop request is made to an empty queue.
    def empty?
      @header.nil?
    end

  end
end
