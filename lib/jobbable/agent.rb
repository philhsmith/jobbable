require 'eventmachine'

require 'jobbable/facade'

module Jobbable
  # A process spawned and watched by a parent.
  class Agent

    class <<self
      # The known subclasses of Agent.
      def subclasses
        @subclasses ||= {}
      end

      # Note subclasses as they are loaded.
      #
      # Containing modules are dropped from the hash key, before they are
      # underscored and symbolized.
      def inherited(subclass)
        name = subclass.name.split("::").last.
          gsub(/([A-Z])/) { "_#{$1.downcase}" }[1..-1]
        subclasses[name] = subclass
      end
    end

    attr_reader :config, :agent_config

    def initialize(config, agent_config)
      @config, @agent_config = config, agent_config
    end

    # The logger.
    def logger
      Jobbable.logger
    end

    # The number of heartbeats that have elapsed.
    def beat
      @beat ||= 0
    end
    attr_writer :beat

    # Called periodically by EM.
    def heartbeat
      # Bump the counter
      self.beat += 1

      # Alert subclasses if they are interested
      on_heartbeat if respond_to? :on_heartbeat

      # Clear the old timer and start one for the next beat
      self.heartbeat_timer = nil
      start_heartbeat_timer
    end

    # A timer (signature) calling #heartbeat.
    attr_accessor :lifespan_timer

    # Run #shutdown after the configured delay.
    def start_lifespan_timer
      self.lifespan_timer ||= EM.add_timer(agent_config.lifespan) do
        # Don't fear the reaper
        logger.warn "Dying of old age"
        shutdown
      end if agent_config.lifespan > 0
    end

    # A timer (signature) calling #heartbeat.
    attr_accessor :heartbeat_timer

    # Run #heartbeat after the configured delay.
    def start_heartbeat_timer
      self.heartbeat_timer ||= EM.add_timer(agent_config.heartbeat) do
        heartbeat
      end
    end

    # Kick off the main processing loop.
    def main
      EM.threadpool_size = agent_config.threads
      EM.set_max_timers agent_config.timers
      EM.error_handler { |exception| logger.error exception }

      # This blocks forever
      EM.run do
        start_lifespan_timer
        on_start if respond_to? :on_start
        start_heartbeat_timer
      end
    end

    # Gracefully stop the agent.
    def shutdown
      logger.warn "Shutting down"
      EM.stop_event_loop
    end

    # Call the block, trapping and logging StandardErrors and ScriptErrors.
    #
    # If the block does not raise an exception, return its value.  If it does,
    # return nil.
    def guard(message = nil, &block)
      begin
        block.call
      rescue StandardError, ScriptError => exception
        logger.error message if message
        logger.error exception
        nil
      end
    end

  end
end
