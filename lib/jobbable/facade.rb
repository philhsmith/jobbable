require 'jobbable/logger'

module Jobbable
  # The root of jobbable exceptions.
  class Error < StandardError; end

  # Raised when it appears a pid file can't be written to.
  class CantWritePidError < Error; end

  # Raised when it appears a log file can't be written to.
  class CantOpenLogError < Error; end

  # Raised when attempting to make AMQP calls without a connection.
  class NotConnectedError < Error; end

  # Raised when a synchronous action would have resulted in a dead lock.
  class DeadlockError < Error; end

  # Some global accessors
  class <<self
    attr_accessor :parent, :worker

    def config;       parent.config;      end
    def environment;  parent.environment; end

    def client
      @client || worker
    end
    attr_writer :client

    def publish(options = {})
      client.publish options
    end

    def queue_status(queue, options = {}, &block)
      client.queue_status queue, options, &block
    end

    # The logger.
    def logger
      @logger ||= NULL_LOGGER
    end

    # Install a new logger.
    #
    # If the previous logger responds to #close, it is called.
    def logger=(new_logger)
      logger.close if @logger and @logger.respond_to?(:close)
      @logger = new_logger
    end
  end
end
