require 'jobbable/cli/command'
require 'jobbable/worker'
require 'jobbable/funnel'

module Jobbable
  # A process that spawns and monitors agent processes.
  class Parent < Cli::Command

    # The current, living agent metadata by pid.
    def agents
      @agents ||= {}
    end

    # The list of agent configs that need to be respawned.
    def spawnable_agents
      # A hash taking agent metadata hashes to total counts.
      counts = Hash[Agent.subclasses.keys.inject([]) do |total, agent_type|
        total + config.send(:"#{agent_type}s").collect do |name, config|
          [{ :type => agent_type, :name => name }, config.count]
        end
      end]

      # Discount living agents
      agents.values.
        collect { |meta| { :type => meta[:type], :name => meta[:name] } }.
        each { |type_and_name| counts[type_and_name] -= 1 }

      # Expand and sort the remainder
      counts.inject([]) do |total, pair|
        type_and_name, count = pair.first, pair.last

        if count > 0
          total + [type_and_name] * count
        else
          total
        end
      end.sort_by { |meta| [meta[:type], meta[:name]] }
    end

    # Spawn and start a new agent using the given agent metadata.
    def spawn(metadata)
      agent_type = metadata[:type]
      agent_name = metadata[:name]

      # Only spawn known types
      unless Agent.subclasses.key? agent_type
        raise ArgumentError.new("Unknown agent type #{agent_type.inspect}")
      end

      # The agent's config
      agent_config = config.send(:"#{agent_type}s")[agent_name]

      # The new agent
      agent = Agent.subclasses[agent_type].new(config, agent_config)

      # Capture our pid so the child process may watch for changes.
      the_parent_pid = parent_pid

      child_pid = fork do
        # Change our name for ps, top, etc
        $0 = "job #{agent_type} #{agent_name}"

        agent_pid = Process.pid
        write_pid "jobbable-#{agent_type}-#{agent_pid}.pid", agent_pid

        # Chdir *back* unless we want to stay at /
        Dir.chdir config.working_directory unless config.chdir

        # Unregister USR1 and trap TERM to the agent's #shutdown
        trap("USR1", "DEFAULT")
        trap("TERM") do
          logger.warn "Received SIGTERM"
          agent.shutdown
        end

        Thread.new do
          # Watch for parent process death.  When the parent dies, the orphans
          # will be given a new parent.  Therefore watch for a change in the
          # parent pid.
          sleep 1 while the_parent_pid == Process.ppid
          logger.fatal "Orphaned, exiting"
          exit 2
        end

        # Install the parent as the global singleton
        Jobbable.parent = self

        begin
          # Kick off the agent
          agent.main
          logger.warn "Exited normally"
        rescue StandardError, ScriptError => exception
          logger.fatal exception
          raise
        ensure
          remove_pid "jobbable-#{agent_type}-#{agent_pid}.pid"
        end
      end

      logger.info "Spawning #{agent_name} #{agent_type} #{child_pid}"

      # Record when to kill the agent, unless it should live forever
      metadata[:reap_at] = Time.now + agent_config.lifespan +
        agent_config.afterlife unless agent_config.lifespan == 0

      agents[child_pid] = metadata
      agent
    end

    # Daemonize and spawn agents.
    def start
      # Become a background process.
      # Please note this changes our pid!
      daemonize!

      # Change our name for ps, top, etc
      $0 = "jobbable"

      # Record our (new) pid
      write_parent_pid

      # Shutdown on Ctrl-C
      trap("TERM") do
        logger.warn "Shutting down"
        shutdown
      end

      # Restart on signal USR1 (there is no Ctrl sequence)
      trap("USR1") do
        logger.warn "Restarting"
        self.old_pidfile = true
        shutdown
      end

      begin
        # Block, watching and respawning agents
        spawn_agents
        logger.warn "Exiting normally"
      rescue StandardError => exception
        logger.fatal exception
        raise
      ensure
        remove_parent_pid
      end
    end

    # Make escalating attempts to kill agents whose reap_at has elapsed.
    #
    # A single SIGTERM is initially sent.  If the agent survives for a minute
    # after this signal, SIGKILLs are sent on each invocation.  This should
    # amount to about 1 SIGKILL per second (see #spawn_agents.)
    #
    # Recall that agents are removed from consideration in #bury_agents, only
    # after the OS confirms that they are dead.
    def reap_agents
      # What time is it?
      now = Time.now

      agents.each do |agent_pid, metadata|
        # When should this agent be killed?
        reap_at = metadata[:reap_at]
        name = metadata[:name]
        type = metadata[:type]

        # Not yet.
        next unless reap_at and reap_at <= now

        if reap_at <= now - 60
          # This agent has survived a full minute, kill it
          logger.error "#{name.capitalize} #{type} #{agent_pid} " \
            "has failed for the last time! Sending SIGKILL"
          Process.kill "KILL", agent_pid
        elsif not metadata[:sigterm_sent]
          # This agent has recently elapsed its timeout, but not yet received
          # a SIGTERM.  Send one.
          logger.warn "Sending SIGTERM to #{name} #{type} #{agent_pid}"
          Process.kill "TERM", agent_pid
          metadata[:sigterm_sent] = true
        end
      end
    end

    # What's the pid of the next unconsidered dead agent?
    #
    # This does not block, but returns nil if there is none.
    def next_dead_agent
      begin
        Process.wait(0, Process::WNOHANG | Process::WUNTRACED)
      rescue Errno::ECHILD => there_are_no_children
        nil
      end
    end

    # Strike any dead agents from the books.
    def bury_agents
      while agent_pid = next_dead_agent
        metadata = agents.delete agent_pid
        name = metadata[:name].capitalize
        type = metadata[:type]

        logger.warn "#{name} #{type} #{agent_pid} died"

        begin
          remove_pid "jobbable-#{type}-#{agent_pid}.pid"
          logger.warn "#{name} #{type} #{agent_pid} left its pid file, " \
            "removing it"
        rescue Errno::ENOENT => the_child_already_cleaned_it_up
          # burp
        end
      end
    end

    # Block forever watching and respawning agents processes.
    def spawn_agents
      until shutting_down? and agents.empty?
        # Create agents up to the configured counts
        spawnable_agents.each do |metadata|
          spawn metadata
        end unless shutting_down?

        sleep 1

        # Hunt down any agents that have gone AWOL
        reap_agents

        # And clean up any dead ones
        bury_agents
      end
    end

    # Should the parent process clean up and exit?
    def shutting_down?
      @shutting_down
    end

    # Flush the agents and indicate the parent should exit.
    def shutdown
      @shutting_down = true

      # What time is it?
      now = Time.now

      # Collect everyone on the next reap
      agents.values.each { |metadata| metadata[:reap_at] = now }
    end

  end
end
