require 'eventmachine'
require 'jobbable/json'

require 'jobbable/logger'
require 'jobbable/config/slot_mixin'

module Jobbable
  # A message representing a job in flight.
  #
  # This should only be mixed into Messages.
  module Flight
    include Config::SlotMixin
    include ExceptionFormatter

    # The originating subscriber for a Flight should be a Worker.
    def worker
      subscriber
    end

    # The job's metadata, reconstructed from its routing key.
    def metadata
      @metadata ||= metadata_for config, header.routing_key
    end

    # What (json-decoded) message was supplied in the body?
    def message
      decoded_body
    end

    # Notify observers of the given event.
    #
    # The event should be an underscored symbol.
    def notify(event)
      worker.notify(self, event)
    end

    # A timer watching for job timeout.
    attr_accessor :timer

    # Start the timeout.
    def start_timeout
      self.timer = EM::Timer.new(config.jobs['a_job'].timeout) do
        logger.warn "Timeout!"
        reattempt
      end
    end

    # Cancel the timeout and notify observers.
    def on_ack
      if timer
        timer.cancel
        self.timer = nil
      end

      notify :ack
    end

    # The options this job was published with, near as can be had.
    #
    # Flights may mutate this, and such changes will be reflected in calls to
    # #publish, for example.
    def params
      @params ||= metadata.merge(message)
    end

    # Publish a job, using this job's params as defaults.
    #
    # All unspecified options will be taken from the current flight's params,
    # except :state and :attempt.  If neither are specified, they are set to
    # 'new' and 0 respectively.  If either are passed, then the other (if not
    # passed) is taken from params.
    #
    # Thus publishing a new job will create it fresh while still allowing this
    # flight to republish itself as needed (see #reattempt.)
    #
    # Notice that calling #publish without any parameters publishes a new,
    # distinct copy of the current job.
    def publish(options = {})
      # If the message body has already successfully decoded, use #params as
      # the context.  If not, use the non-message metadata instead, and
      # explicitly pass the message body along in the :message key.
      #
      # PublisherMixin#publish is smart enough to attempt to merge excess
      # options and the explicit body if necessary, and can gracefully deal
      # with bad message JSON.
      #
      # This way, if the message is unparsable we can at least still publish
      # (likely to the crash exchange.)
      to_publish = if decoded?
        params
      else
        metadata.merge :message => body
      end

      # Toss on the flight logger
      to_publish = to_publish.merge :logger => logger

      # Reset the state and attempt so long as neither is explicitly passed.
      if (options.keys & [:state, :attempt]).empty?
        to_publish.merge! :state => 'new', :attempt => 0
      end

      # Include other explicit options
      to_publish.merge! options

      worker.publish to_publish
    end

    # End this flight in success.
    def complete
      logger.info "Completed"
      ack
      notify :complete
    end

    # End this flight in unexpected error.
    #
    # It will be republished to the crash exchange, if one is configured.
    def crash(reason = "Crashed!")
      logger.error reason

      if config.exchanges.key? 'crash'
        publish :exchange => 'crash', :state => 'crash',
          'reason' => format_exception(reason)
      end

      ack
      notify :crash
    end

    # End this flight in expected error other than retry exhaustion.
    #
    # It will be republished to the abort exchange, if one is configured.
    def abort(reason = "Aborted!")
      logger.error reason

      if config.exchanges.key? 'abort'
        publish :exchange => 'abort', :state => 'abort',
          'reason' => format_exception(reason)
      end

      ack
      notify :abort
    end

    # End this flight and retry it later if possible.
    #
    # #retry might be a more natural name, but this is reserved in ruby.
    #
    # If the job is out of attempts, it will be republished to the fail
    # exchange, if one is configured.
    #
    # If it is not out of attempts, it will be republished to its normal
    # exchanges, in the retry state on the next attempt.  In this case any
    # additional options are republished with the job, allowing it to save
    # state between attempts.
    def reattempt(options = {})
      if metadata[:attempt] + 1 < config.jobs[metadata[:job]].attempts
        logger.warn "Reattempting"
        publish options.merge(
          :state => 'retry', :attempt => metadata[:attempt] + 1)
      else
        logger.error "Out of attempts!"
        if config.exchanges.key? 'fail'
          publish :exchange => 'fail', :state => 'fail'
        end
      end

      ack
      notify :reattempt
    end

    # Defer an operation to be processed off-thread.
    #
    # Once it completes, the given callback (if any) will be invoked with the
    # result on the reactor thread. See EM.defer in eventmachine's docs.
    #
    # It is preferable to call this method over EM.defer directly, as
    # Flight#defer will recover from and log any exceptions that bubble up in
    # the deferred block: EM.defer will silently drop the work on the floor.
    #
    # Worker#rescue is called, wrapping the deferred block.  If it raises any
    # exceptions, they will be processed by the worker's exception handler
    # chain.  In that case the callback (if present) will not be invoked.
    #
    # If you want to process exceptions in the callback, catch them in the
    # deferred block and pass them out yourself.
    def defer(op = nil, callback = nil, &block)
      op ||= block

      # Did the deferred operation complete without being rescued?
      success = false

      # If present, wrap the callback to invoke only conditionally on success
      if callback
        original_callback = callback

        callback = lambda do |result|
          if success
            if original_callback.arity == 0
              original_callback.call
            else
              original_callback.call(result)
            end
          end
        end
      end

      EM.defer(lambda do
        worker.rescue(self) do
          op.call.tap { success = true }
        end
      end, callback)
    end

  end
end
