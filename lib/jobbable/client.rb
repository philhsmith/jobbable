require 'jobbable/facade'
require 'jobbable/config_mixin'
require 'jobbable/amqp/publisher_mixin'

module Jobbable
  # A configured interface for publishing jobs.
  #
  # See Amqp::PublisherMixin.
  class Client
    include ConfigMixin
    include Amqp::PublisherMixin

    # The logger.
    def logger
      Jobbable.logger
    end

  end
end
