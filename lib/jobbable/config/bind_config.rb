require 'jobbable/config/config_hash'
require 'jobbable/config/slot_mixin'

module Jobbable
  module Config
    # A container for queue binds.
    # 'Binding' might be a more natural name, but ruby reserves #binding.
    class BindConfig < ConfigHash
      include SlotMixin

      config_property :exchange,    :default => 'default',
        :description => "The exchange to bind to, or 'all'"

      config_property :queue,       :default => 'default',
        :description => "The queue to bind to, or 'all'"

      config_property :key,         :default => '#',
        :description => "The bind key to use"

      # bind-specific configuration options.
      # These defaults match the amqp gem.  See it for details.
      # Beware, jobbable's authors haven't bothered to grok the consequences
      # of changing some of these flags.
      config_property :wait,        :default => false,  :type => :boolean,
        :description => "Wait for the broker's response"

      # Bind keys must conform to topic-matching syntax.
      def valid?
        super

        # Bind keys must be dot-seperated tuples of #, * or strings without
        # # and *. In otherwords, any mix of #, * and regular letters is
        # illegal in the same dot-delimited segment.
        unless key =~ ROUTING_KEY
          errors['key'] << "contains malformed routing key #{key.inspect}"
        end

        errors.empty?
      end
    end
  end
end
