require 'socket'

module Jobbable
  module Config
    # Enables mixers to manipulate slot patterns and substitutions.
    module SlotMixin

      # The legal states a job may be published in.
      STATES = %w{new retry fail abort crash}.freeze

      # The known dynamic slots that can appear in routing key templates.
      SLOTS = %w{job attempt state priority host tags}.freeze

      # Matches routing keys usable for fast topic matching.
      ROUTING_KEY = /^([^#*.]+|[#*])(\.([^#*.]+|[#*]))*$/

      # Regexp captures for each slot.
      CAPTURES = {
        'job'       => "([^.]+)",
        'attempt'   => "(0|[1-9][0-9]*)",
        'state'     => "(#{STATES.join('|')})",
        'priority'  => "([0-9])",
        'host'      => "([^.]+)",
        'tags'      => "(.+)"
      }.freeze

      # Default slot values.
      DEFAULTS = {
        :job      => 'default',
        :state    => 'new',
        :attempt  => 0,
        :priority => 0,
        :host     => 'any',
        :tags     => [].freeze
      }.freeze

      # What dynamic slots are declared in the given string?
      def slot_names(string)
        string.scan(/\{(#{SLOTS.join("|")})\}/).flatten.uniq
      end

      # Construct a regex matching the dynamic slots for the given string.
      def slot_pattern(string)
        Regexp.new('^' + expand_slots(Regexp.escape(string), CAPTURES) + '$')
      end

      # Extract slots values from the string using the template.
      def slot_values(template, string)
        # Extract the dynamic slot names from the template
        names = slot_names(template).collect(&:to_sym)

        # Build a slot regex from the template and extract the values
        values = string.scan(slot_pattern(template)).flatten.reject(&:empty?)

        # Bad string
        return nil unless names.size == values.size

        # Construct the name-value hash
        hash = Hash[names.zip(values)]

        # Convert attempt and priority to integers
        hash[:attempt]  = hash[:attempt].to_i   if hash.key? :attempt
        hash[:priority] = hash[:priority].to_i  if hash.key? :priority

        # Translate underscores in host back to periods
        hash[:host].tr!('_', '.') if hash.key? :host

        # Split tags
        if hash.key? :tags
          hash[:tags] = '' if hash[:tags] == '_'
          hash[:tags] = hash[:tags].split('.')
        end

        # Include missing defaults
        hash = DEFAULTS.merge hash

        # Dup the default tags array
        hash[:tags] = hash[:tags].dup if hash[:tags] === DEFAULTS[:tags]

        hash
      end

      # Extract slot values from the first matching configured job template.
      #
      # Templates are tried in a deterministic order, which is arbitrary save
      # that the default job templates are always tried last.
      #
      # The :job option may be passed to consider only configured templates.
      def metadata_for(config, string, options = {})
        # Get the configured job names in sorted order, sans defaults
        job_names = if options[:job]
          options[:job]
        else
          (config.jobs.keys - [DEFAULTS[:job]]).sort
        end

        # For each configured job..
        job_names.collect do |job_name|
          # Don't bother examining its templates if the name doesn't appear
          next unless string =~ %r{#{job_name}}

          # Otherwise consider its routing key templates in turn
          config.jobs[job_name].keys.each do |template|
            # Attempt to match the given string to the template
            next unless values = slot_values(template, string)

            # And return the extracted hash if the extracted job name agrees
            return values if values[:job] == job_name
          end
        end

        # Now try the default templates
        config.jobs[DEFAULTS[:job]].keys.each do |template|
          # Attempt to match the given string to the template
          next unless values = slot_values(template, string)
          return values
        end unless options[:job]

        # Otherwise there is no match
        nil
      end

      # Substitute the given slot values into the string.
      #
      # The values hash should use string keys.
      def expand_slots_in_string(string, values)
        string.gsub(
          /(?:\\)?\{(#{values.keys.join("|")})(?:\\)?\}/) { values[$1] }
      end

      # Substitute the given slot values into the given string, array or hash.
      #
      # This will recurse into arrays and hashes, substituting into any
      # strings found there.
      #
      # Objects with types other than string, array and hash are passed
      # through unchanged.
      #
      # The values hash should use string keys.
      def expand_slots(o, values)
        case o
        when String
          expand_slots_in_string o, values
        when Array
          o.collect { |e| expand_slots e, values }
        when Hash
          Hash[o.collect do |k, v|
            [expand_slots(k, values), expand_slots(v, values)]
          end]
        else
          o
        end
      end

      # Enumerate over each legal expansion of the slot values.
      def each_slot_expansion(slots, jobs, attempts, &block)
        slots(slots, jobs, attempts).each(&block)
      end

      # Factory method for creating Slots.
      def slots(slots, jobs, attempts)
        Slots.new(slots, jobs, attempts)
      end

      # An Enumerable over slot values this job might be published with.
      class Slots
        include Enumerable

        attr_reader :slots, :jobs, :attempts

        # Create an enumerator over the slot names, assuming a list of job
        # names and attempts.
        #
        # The attempts argument may be an integer or a hash.  Hashes should
        # map the passed job names to integers.  It should also contain a
        # 'default' key if 'job' is not itself one of the slots being iterated
        # over.
        #
        # Unknown slots names are rejected with an ArgumentError, as is the
        # 'tags' slot (since there isn't a reasonable way to iterate over
        # them.)
        def initialize(slots, jobs, attempts)
          # Since we can't iterate over known tags, forbid them
          unknown_slots = slots - (SLOTS - %w{tags})
          raise ArgumentError.new("Unknown slot(s) #{unknown_slots.join(" ")}"
            ) if unknown_slots.any?

          @slots, @jobs, @attempts = slots, jobs, attempts
        end

        class <<self
          # What is the hostname of this machine?
          def hostname
            @hostname ||= Socket.gethostname
          end

          attr_writer :hostname
        end

        # What is the hostname of this machine?
        def hostname
          self.class.hostname
        end

        # How many attempts should we assume for the given job?
        def attempts_for(job)
          if attempts.is_a? Hash
            attempts[job]
          else
            attempts
          end
        end

        # Enumerate over legal combinations of the requested slot values.
        #
        # Each yielded object is a hash taking the requested slot names as
        # symbolic keys to values that the slot might take on:
        # * :job ranges over the job names passed at construction
        # * :state ranges over the five states new, retry, fail, abort, crash.
        # * :attempt ranges from 0 up to the current job's configured number
        #   of attempts (exclusive.)  There are two exceptions.  Combinations
        #   with state 'new' only ever have attempt 0.  Combinations with
        #   state 'retry' never have attempt 0.  If 'job' is not one of the
        #   slots being iterated over, the attempts from the default job is
        #   used.
        # * :priority ranges from 0 to 9 inclusive.
        # * :host takes on the values 'any' and the current hostname, with
        #   periods replaced with underscores.
        #
        # While it is a slot, 'tags' may not be iterated over.
        def each
          ranges = {
            "job"       => jobs,
            "state"     => STATES,
            "priority"  => 0..9,
            "host"      => ["any", hostname.tr('.', '_')]
          }

          # There's probably some continuation-passing-ish way to chain some
          # lambdas together and make this iterator without explicitly
          # building the list, but the space is small and I don't care.

          combinations = [{}]

          # Ensure attempt appears after state and job if they are present.
          # This lets us adjust the legal attempt range based on the given
          # job/state combination.
          ordered_slots = \
          if slots.include?('attempt') and (slots & %w{job state}).any?
            slots.dup.tap do |ordered|
              ordered.delete "attempt"
              ordered.push "attempt" # make it last
            end
          else
            slots
          end

          ordered_slots.each do |slot|
            combinations.collect! do |combination|
              range = if slot == 'attempt'
                attempts = attempts_for(combination['job'] || 'default')

                lower = combination['state'] == 'retry'  ? 1 : 0
                upper = combination['state'] == 'new'    ? 1 : attempts

                lower...upper
              else
                ranges[slot]
              end

              range.collect do |value|
                { slot => value }.merge(combination)
              end
            end.flatten!
          end

          if block_given?
            combinations.each { |combination| yield combination }
          else
            combinations.each
          end
        end
      end
    end
  end
end
