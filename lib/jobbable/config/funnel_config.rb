require 'jobbable/config/agent_config'

module Jobbable
  module Config
    # A container for funnel-specific configuration.
    class FunnelConfig < AgentConfig
      config_property :queues,    :default => %w{default},  :type => Array,
        :description => "The queues to take work from"

      config_property :max,       :default => 20,           :type => Integer,
        :description => "The largest number of messages to aggregate at once"

      config_property :exchanges, :default => ['default'],  :type => Array,
        :description => "Exchanges to publish to"

      config_property :key, :default => "default.0.new.0.any._",
        :description => "Routing key to publish with"

      # There are no funnels by default
      defaults['count'] = 0
    end
  end
end
