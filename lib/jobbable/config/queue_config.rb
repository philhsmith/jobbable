require 'jobbable/config/config_hash'

module Jobbable
  module Config
    # A container for queue-specific configuration.
    class QueueConfig < ConfigHash
      TYPES = %w{subscribe pop}.freeze

      config_property :type,        :default => 'subscribe',
        :description => "One of #{TYPES.join(', ')}"

      config_property :name,        :default => 'jobbable',
        :description => "The AMQP name of the queue"

      config_property :inflight,    :default => 10,     :type => Integer,
        :description => "Number of concurrent jobs (subscribe only)"

      config_property :burst,       :default => 1,      :type => Integer,
        :description => "Number of jobs to take when pulling work (pop only)"

      # Queue-specific configuration options.
      # These defaults match the amqp gem.  See it for details.
      # Beware, jobbable's authors haven't bothered to grok the consequences
      # of changing some of these flags.
      config_property :passive,     :default => false,  :type => :boolean,
        :description => "Do not create if non-existant"

      config_property :durable,     :default => false,  :type => :boolean,
        :description => "Persist to non-volatile storage"

      config_property :exclusive,   :default => false,  :type => :boolean,
        :description => "Make private (implies auto-delete)"

      config_property :auto_delete, :default => false,  :type => :boolean,
        :description => "Delete after last consumer finishes"

      config_property :wait,        :default => false,  :type => :boolean,
        :description => "Wait for the broker's response"

      # Queue types must be ones of subscribe, or pop.
      def valid?
        super

        # Queue types must be ones of subscribe, or pop
        unless type.nil? or TYPES.include?(type)
          errors['type'] << "must be either subscribe, or pop"
        end

        errors.empty?
      end
    end
  end
end
