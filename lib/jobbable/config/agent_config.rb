require 'jobbable/config/config_hash'

module Jobbable
  module Config
    # A container for general agent configuration.
    class AgentConfig < ConfigHash
      config_property :count,         :default => 1,      :type => Integer,
        :description => "The number of agent processes"

      config_property :threads,       :default => 7,      :type => Integer,
        :description => "The eventmachine deferment thread pool size"

      config_property :timers,        :default => 100000, :type => Integer,
        :description => "The eventmachine max timer count"

      config_property :heartbeat,     :default => 10,     :type => Integer,
        :description => "Seconds between heartbeats"

      config_property :lifespan,      :default => 900,    :type => Integer,
        :description => "How long to live in seconds, or 0 to disable"

      config_property :afterlife,     :default => 120,    :type => Integer,
        :description => "Seconds after lifespan before being killed"
    end
  end
end
