require 'jobbable/config/config_hash'

module Jobbable
  module Config
    # A container for exchange-specific configuration.
    class ExchangeConfig < ConfigHash
      TYPES = %w{topic direct fanout}.freeze

      config_property :type,  :default => 'topic',
        :description => "One of #{TYPES.join(', ')}"

      config_property :name,  :default => 'jobbable',
        :description => "The AMQP name of the exchange"

      # Exchange-specific configuration options.
      # These defaults match the amqp gem.  See it for details.
      # Beware, jobbable's authors haven't bothered to grok the consequences
      # of changing some of these flags.
      config_property :passive,     :default => false,  :type => :boolean,
        :description => "Do not create if non-existant"

      config_property :durable,     :default => false,  :type => :boolean,
        :description => "Persist to non-volatile storage"

      config_property :auto_delete, :default => false,  :type => :boolean,
        :description => "Delete after last consumer finishes"

      config_property :internal,    :default => false,  :type => :boolean,
        :description => "Make unusable by publishers"

      config_property :wait,        :default => false,  :type => :boolean,
        :description => "Wait for the broker's response"

      config_property :declare,     :default => true,   :type => :boolean,
        :description => "Assume declaration is required"

      # Exchange types must one of topic, direct or fanout.
      def valid?
        super

        # Exchange types must be ones of topic, direct, or fanout
        unless type.nil? or TYPES.include?(type)
          errors['type'] << "must be either topic, direct or fanout"
        end

        errors.empty?
      end
    end
  end
end
