require 'jobbable/config/agent_config'

module Jobbable
  module Config
    # A container for worker-specific configuration.
    class WorkerConfig < AgentConfig
      config_property :queues,  :default => %w{default},  :type => Array,
        :description => "The queues to take work from"
    end
  end
end
