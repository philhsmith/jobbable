require 'optparse'

module Jobbable
  module Config
    # A Hash with explicit getters and setters for certain type-specific keys.
    class ConfigHash < Hash
      class <<self

        # Propogate configuration property metadata to subclasses.
        def inherited(child)
          child.defaults.merge!(defaults)
          child.types.merge!(types)
          child.descriptions.merge!(descriptions)
          child.optional.merge!(optional)
        end

        def config_property(name, options = {})
          name = name.to_s

          defaults.merge!     name => options[:default].freeze
          descriptions.merge! name => options[:description]
          types.merge!        name => (options[:type] || String)
          optional.merge!     name => (options[:optional] || false)

          define_method(name.to_sym) { self[name] }
          define_method(:"#{name}=") { |value| self[name] = value }
        end

        def properties
          defaults.keys.sort
        end

        def defaults;     @defaults ||= {};     end
        def types;        @types ||= {};        end
        def descriptions; @descriptions ||= {}; end
        def optional;     @optional ||= {};     end

        # Declare all int, string, bool and array CLI options on an OptParser.
        #
        # The CLI values are set on the object returned by the block.
        def declare_simple_options(opts, options = {}, &block)
          prefix = "#{options[:prefix]}-" if options[:prefix]

          properties.select do |o|
            [Integer, String].include? types[o]
          end.sort.each do |o|
            next unless descriptions[o]

            default_value = defaults[o]
            default_value = 'none' if default_value.nil?
            opts.on("--#{prefix}#{o.tr('_', '-')} #{o[0..0].upcase}",
              types[o], "#{descriptions[o]} (#{default_value})") do |v|
              block.call.send :"#{o}=", v
            end
          end

          properties.select { |o| types[o] == :boolean }.sort.each do |o|
            next unless descriptions[o]

            opts.on("--[no-]#{prefix}#{o.tr('_', '-')}",
              "#{descriptions[o]} (#{defaults[o]})") do |v|
              block.call.send :"#{o}=", v
            end
          end

          properties.select { |o| types[o] == Array }.sort.each do |o|
            next unless descriptions[o]

            plural = o.tr('_', '-')
            singular = plural.gsub(/s$/, '')
            abbrev = o[0..0].upcase

            opts.on("--#{prefix}add-#{singular} #{abbrev}", String,
              "Add a #{singular.tr('-', ' ')}") do |v|
              block.call.send(o) << v
            end

            opts.on("--#{prefix}remove-#{singular} #{abbrev}", String,
              "Remove a #{singular.tr('-', ' ')}") do |v|
              block.call.send(o).delete(v)
            end

            opts.on("--#{prefix}clear-#{plural}", String,
              "Remove all #{plural.tr('-', ' ')}") do
              block.call.send(o).clear
            end
          end

          opts
        end
      end

      def initialize(*args, &block)
        super
        defaults!
      end

      def defaults;     self.class.defaults;      end
      def properties;   self.class.properties;    end
      def types;        self.class.types;         end
      def descriptions; self.class.descriptions;  end
      def optional;     self.class.optional;      end

      def declare_simple_options(opts, options = {}, &block)
        self.class.declare_simple_options(opts, options, &block)
      end

      # Enumerate over required, nil properties.
      def each_missing_property(&block)
        properties.select do |p|
          self[p].nil? and not optional[p]
        end.each { |p| block.call(p) }
      end

      # Enumerate over properties of a given declared type.
      def each_property_of_type(type, &block)
        properties.select { |p| types[p] == type }.each { |p| block.call(p) }
      end

      # Set this configuration hash to its defaults values.
      def defaults!
        merge! defaults

        # Dup default values of frozen attributes.
        properties.each { |p| self[p] = self[p].dup if self[p].frozen? }
      end

      # What errors were found during validation?
      def errors
        @errors ||= Hash.new { |h, k| h[k] = [] }
      end

      # Is this configuration usable?
      def valid?
        errors.clear

        # Presence checks
        each_missing_property { |p| errors[p] << "is required" }

        # Pretty much all integers must be non-negative
        each_property_of_type(Integer) do |p|
          errors[p] << "can't be negative" if self[p] and self[p] < 0
        end

        # We are valid so long as we have no errors.
        errors.empty?
      end

      # Raises any errors found if not valid.
      def validate!
        return if valid?

        all_errors = errors.collect do |key, messages|
          messages.collect { |message| "#{key} #{message}" }
        end.flatten

        raise ArgumentError.new(all_errors.join("\n"))
      end

      # Return a pure-hash copy.
      def to_hash
        {}.merge(self)
      end
    end
  end
end
