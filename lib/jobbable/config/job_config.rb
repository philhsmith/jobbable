require 'jobbable/config/config_hash'
require 'jobbable/config/slot_mixin'

module Jobbable
  module Config
    # A container for job-specific configuration.
    class JobConfig < ConfigHash
      include SlotMixin

      config_property :timeout,   :default => 180,          :type => Integer,
        :description => "How long a job may run before being killed"

      config_property :attempts,  :default => 5,            :type => Integer,
        :description => "How many times a job may be retried"

      config_property :exchanges, :default => ['default'],  :type => Array,
        :description => "Exchanges to publish to"

      config_property :keys,                                :type => Array,
        :default => [SLOTS.collect { |slot| "{#{slot}}" }.join(".")],
        :description => "Routing key templates to publish and dispatch with"

      # Validate this job configuration.
      #
      # * Publishing keys must conform to topic-matching syntax
      # * Publishing keys must start with {job} and contain {attempt}
      # * Publishing keys must only include known 'dynamic slots'
      #
      # Dynamic slots are variables appearing between {}s, and are populated
      # on publish with information about the job to publish.  The known ones
      # are defined in SlotMixin::SLOTS.
      def valid?
        super

        keys.each do |key|
          # templates are dotted tuples of #, * or strings without # and *.
          unless key =~ ROUTING_KEY
            errors['keys'] << "has malformed key template #{key.inspect}"
          end

          # Job key templates must start with {job} and include {attempt}
          errors['keys'] << "must start with {job}" unless key =~ /^\{job\}/

          unless key.include? '{attempt}'
            errors['keys'] << "must have {attempt}"
          end

          # Job key templates must include only known dynamic slots
          key.scan(/\{([^}]*)\}/).flatten.each do |slot|
            unless SLOTS.include? slot
              errors['keys'] << "has unknown slot {#{slot}}"
            end
          end
        end

        errors.empty?
      end

    end
  end
end
