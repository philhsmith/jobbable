module Jobbable
  module Dispatcher

    # A convenience module for dispatcher implementations.
    #
    # Implementations must provide a #dispatch(flight) method, which is the
    # only contract between a Dispatcher and a Worker.
    module Abstract

      # Resolve a name into a module or class constant.
      #
      # If the camel-cased name is not found, try to require a file named
      # after the underscored name and look again.
      def constantize(name)
        # underscore the name for filename
        file_name = name.gsub(/^([A-Z])/) { $1.downcase }
        file_name.gsub!(/([A-Z])/) { "_#{$1.downcase}" }

        # camelize it for the constant name.
        type_name = name.capitalize.gsub(/_([a-z])/) { $1.upcase }

        return Object.const_get(type_name) rescue require file_name
        Object.const_get(type_name)
      end

      # The constantized job named in the given flight's metadata.
      def job_type(flight)
        constantize flight.metadata[:job]
      end

    end
  end
end
