require 'jobbable/dispatcher'

module Jobbable
  module Dispatcher

    # Resolves jobs by assuming they name classes.
    #
    # Constantized job names are instantiated as classes without arguments.
    # The instances need to have a #flight= setter and a #perform that takes
    # no arguments.
    class ClassDispatcher
      include Abstract

      def dispatch(flight)
        job_type(flight).new.tap do |job|
          job.flight = flight
        end.perform
      end
    end

  end
end
