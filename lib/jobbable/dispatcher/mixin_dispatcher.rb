require 'jobbable/dispatcher'

module Jobbable
  module Dispatcher

    # Resolves jobs by assuming they name modules.
    #
    # Constantized job names are mixed into flights.  They must provide
    # #perform, which is called with no arguments.
    class MixinDispatcher
      include Abstract

      def dispatch(flight)
        flight.extend job_type(flight)
        flight.perform
      end
    end

  end
end
