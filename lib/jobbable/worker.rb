require 'jobbable/monkey/core/proc'

require 'jobbable/agent'
require 'jobbable/dispatcher/mixin_dispatcher'
require 'jobbable/flight'
require 'jobbable/amqp/publisher_mixin'
require 'jobbable/amqp/subscriber_mixin'

module Jobbable
  # An agent that performs work taken from queues.
  class Worker < Agent
    include Amqp::SubscriberMixin
    include Amqp::PublisherMixin

    # The dispatcher maps flights to code to perform them.
    def dispatcher
      @dispatcher ||= Dispatcher::MixinDispatcher.new
    end
    attr_writer :dispatcher

    # Add the configured paths to $:.
    def append_require_paths
      (config.full_require_paths - $:).each { |path| $: << path }
    end

    # Require the configured autorequire.
    def autorequire
      require config.autorequire if config.autorequire
    end

    # Take a burst of work from all pop queues whose periods have elapsed.
    def on_heartbeat
      logger.debug counters
      pop_queues.each { |queue| pop queue }
    end

    # Gracefully stop the worker.
    def shutdown
      stop { super }
    end

    # Subscribe to our queues for work.
    #
    # Before subscribing, the worker appends the configured load paths and
    # loads the autorequire if any.  All exceptions reported by EM are sent to
    # #rescue.
    def on_start
      # Install this worker as the global one
      Jobbable.worker = self

      # Send exceptions that hit EM through the rescue handler chain
      EM.error_handler { |exception| self.rescue exception }

      # Connect to AMQP
      start do
        # Add any user load paths and requires
        append_require_paths
        autorequire

        # Ensure special error exchanges for flights are bound, if present
        %w{crash abort fail}.each do |error_exchange|
          if config.exchanges.key? error_exchange
            amqp.ensure_bound error_exchange
          end
        end

        # And subscribe to our queues, handing all messages to #perform.
        subscribe(agent_config.queues) { |message| perform message }
      end
    end

    # Perform the job embodied by the given message.
    #
    # If the flight (or dispatcher) raises an exception, it will be passed to
    # the worker's exception handler chain via #rescue.  This holds even in
    # callbacks invoked by EM after this method completes: please see the
    # monkey patch we apply to Proc and Exception.
    def perform(message)
      # Mixin awareness that the message is a job in flight.
      message.extend Flight

      # Start the job's timeout.
      message.start_timeout

      # Notify observers that the job is starting.
      notify message, :perform

      # Dispatch it.
      self.rescue(message) do
        dispatcher.dispatch message
      end
    end

    # Exception handlers applicable to all jobs.
    def exception_handlers
      @exception_handlers ||= []
    end
    attr_writer :exception_handlers

    # What exception handlers should be used for the given flight?
    #
    # This will always include the worker-wide ones.  If the flight responds
    # to #exception_handlers, it will include those first as well.
    def exception_handlers_for(flight)
      flight_handlers = guard("Couldn't get flight's exception handlers") do
        if flight.respond_to? :exception_handlers
          handlers = flight.exception_handlers

          unless handlers.is_a? Array
            raise TypeError.new("#{handlers.inspect} is not an Array")
          end

          handlers
        end
      end || []

      flight_handlers + exception_handlers
    end

    # Handle the given exception with a chain of registered callbacks.
    #
    # This may be called with either arguments or a block.  If arguments are
    # given, the first must be an exception.  The second argument is optional,
    # but if present must be a flight.
    #
    # If a block is given, it is immediately run without arguments.  If it
    # raises an exception, the exception is treated as if passed as the first
    # argument.  A flight may still be passed as the (now) only positional
    # argument.  If the block runs successfully we return the its value and do
    # nothing.
    #
    # If a flight is passed, or one is found on the exception, the exception
    # handlers returned by #exception_handlers_for are invoked.  Each handler
    # will be called in turn with rescue(exception, flight) until one such
    # call evaluates true.
    #
    # It is assumed that the handler will (for example) reattempt or abort the
    # flight itself if desired.  If a handler raises an exception, it will be
    # logged and the next handler will be invoked with the original exception
    # (handler exceptions are not themselves handled.)
    #
    # Handlers are reused between calls, so state will be shared across all
    # invocations.
    #
    # If no registered handler claims the exception, the flight crashes.
    #
    # If no flight is found the exception is merely logged.
    def rescue(*args, &block)
      if args.empty? and !block
        raise ArgumentError.new("must be called with arguments or a block")
      end

      # Since we mutate them
      args = args.dup

      if block
        begin
          return block.call
        rescue StandardError, ScriptError => exception
          # Do nothing, but keep the assignment to the local
        end
      else
        exception = args.shift
      end

      flight = args.shift || exception.raising_flight

      # No flight?  Just log it.
      unless flight
        logger.error exception
        return exception
      end

      # Get the handlers appropriate to this flight
      handlers = exception_handlers_for(flight)

      # Try each until one returns true
      handled = handlers.any? do |handler|
        guard "Exception handler itself raised an error" do
          handler.rescue(exception, flight)
        end
      end

      # Or just crash if nobody claimed the exception.
      flight.crash exception unless handled

      # And return the exception itself
      exception
    end

    # Observers interested in all flights.
    def observers
      @observers ||= []
    end
    attr_writer :observers

    # What observers are interested in the given flight?
    #
    # This will always include the worker-wide ones.  If the flight responds
    # to #observers, it will include those first as well.
    def observers_for(flight)
      flight_observers = guard("Couldn't get flight's observers") do
        if flight.respond_to? :observers
          observers = flight.observers

          unless observers.is_a? Array
            raise TypeError.new("#{observers.inspect} is not an Array")
          end

          observers
        end
      end || []

      flight_observers + observers
    end

    # Notify all interested observers of the given event on the given flight.
    #
    # The interested observers are those returned from #observers_for.
    #
    # Observers are notified by calling a method named after the event, with
    # the prefix "on_".  So, if the event is :complete, the sought method will
    # be #on_complete.
    #
    # If the observer responds to the callback method, it will be called with
    # the flight.
    #
    # Observers are reused between calls, so state will be shared across all
    # invocations.
    def notify(flight, event)
      observers_for(flight).each do |observer|
        guard "Couldn't notify flight observer of #{event}" do
          if observer.respond_to? :"on_#{event}"
            observer.send :"on_#{event}", flight
          end
        end
      end
    end

  end
end
