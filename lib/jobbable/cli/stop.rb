require 'jobbable/cli/command'

module Jobbable
  module Cli
    class Stop < Command

      summary "Gracefully stop a cluster of workers"

      # Signal an existing parent to gracefully stop.
      def perform
        begin
          logger.warn "Stopping #{parent_pid}"
          Process.kill "TERM", parent_pid
        rescue Errno::ENOENT => no_parent_pid_file
          logger.info no_parent_pid_file
        rescue Errno::ESRCH => pid_file_appears_stale
          logger.warn "The pid file appears to be stale, removing"
          remove_parent_pid
        end
      end

    end
  end
end
