require 'jobbable/json'

require 'jobbable/cli/command'
require 'jobbable/amqp/publisher_mixin'

module Jobbable
  module Cli
    class Publish < Command
      include Amqp::PublisherMixin

      summary "Publish messages to exchanges"

      # Declare publishing options.
      def build_command_option_parser(opts)
        opts.separator "  --exchange and --tag may be passed many times."
        opts.separator "  --file and --count cannot both be passed at once."
        opts.separator ""

        opts.on("--job J", "Publish job J") do |j|
          opts.options['job'] = j
        end

        opts.on("--key K", "Publish with routing key K") do |k|
          opts.options['key'] = k
        end

        opts.on("--message M", "Publish literal message M") do |m|
          opts.options['message'] = m
        end

        opts.on("--exchange E", "Publish to exchange E") do |e|
          (opts.options['exchanges'] ||= []) << e
        end

        opts.on("--file F", "Publish messages from json file F") do |f|
          opts.options['file'] = f
        end

        opts.on("--count C", Integer, "Publish C identical messages") do |c|
          opts.options['count'] = c
        end

        opts.on("--attempt A", "Publish with attempt A") do |a|
          opts.options['attempt'] = a
        end

        opts.on("--type T", "Publish with content type T") do |t|
          opts.options['content_type'] = t
        end

        opts.on("--host H", "Publish with host H") do |h|
          opts.options['host'] = h
        end

        opts.on("--priority P", "Publish with priority P") do |p|
          opts.options['priority'] = p
        end

        opts.on("--state S", "Publish with state S") do |s|
          opts.options['state'] = s
        end

        opts.on("--tag T", "Publish with tag T") do |t|
          (opts.options['tags'] ||= []) << t
        end

        opts.on("--template T", "Publish with routing key template T") do |t|
          opts.options['template'] = t
        end
      end

      # Publish a job through the framework from the command line.
      def perform
        options = parse_command_argv

        file = options.delete 'file'
        count = options.delete 'count'
        raise ArgumentError.new(
          "Either --file and --count may be passed, but not both"
        ) if file and count

        parse_and_publish = lambda do |io|
          io.each do |line|
            publish Jobbable::JSON.decode(line).merge(options)
          end
        end

        start

        # Publish the message(s)
        if file == '-'
          parse_and_publish[STDIN]
        elsif file
          File.open(file, 'r') { |io| parse_and_publish[io] }
        else
          (count || 1).times { publish options }
        end

        stop
      end

    end
  end
end
