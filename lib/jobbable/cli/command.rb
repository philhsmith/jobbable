require 'optparse'
require 'fileutils'

require 'jobbable/facade'
require 'jobbable/cli/command_line'
require 'jobbable/logger'

module Jobbable
  module Cli
    # Superclass for all jobbable actions callable from the command line.
    class Command
      include LoggerMixin

      class <<self
        # The verb corrosponding to this command.
        def verb
          name.split("::").last.downcase
        end

        # All subclass summaries, by type name
        def summaries
          @summaries ||= {}
        end

        # Set or get a one-liner description of the command itself.
        def summary(value = nil)
          Command.summaries[verb] = value if value
          Command.summaries[verb]
        end

        # The legal verbs that may be invoked from the command line.
        #
        # This list is the sorted keys of .summaries.
        def verbs
          summaries.keys.sort
        end
      end

      attr_reader :command_line

      def initialize(command_line)
        @command_line = command_line
      end

      # The jobbable configuration used by this command.
      def config
        command_line.config
      end

      # The environment to use from the config file.
      def environment
        command_line.environment
      end

      # The config file to load.
      def config_file
        command_line.config_file
      end

      # General command line options passed to the command.
      def argv
        command_line.argv
      end

      # Command-specific options left over after general command line options.
      def command_argv
        command_line.command_argv
      end

      # The logger.
      def logger
        command_line.logger
      end

      # Tailors the given OptionParser for command-specific options.
      #
      # This method does nothing, but subclasses may override it.
      def build_command_option_parser(opts)
        :not_modified
      end

      # An OptionParser suitable for parsing command-specific options.
      #
      # Subclasses may modify the returned parser by overriding
      # #build_command_option_parser.
      def command_option_parser
        verb = self.class.verb

        OptionParser.new.tap do |opts|
          class <<opts
            def options
              @options ||= {}
            end
          end

          opts.separator ""
          opts.separator "#{Command.summaries[verb]}."
          opts.separator ""
          opts.separator "#{verb.capitalize} Options:"
          opts.separator ""

          if build_command_option_parser(opts) == :not_modified
            opts.banner = "Usage: jobbable #{verb} [environment] " \
              "[path/to/jobbable.yml] [general options]"
          else
            opts.banner = "Usage: jobbable #{verb} [environment] " \
              "[path/to/jobbable.yml] [general options] -- [#{verb} options]"
          end

          opts.on_tail("-h", "--help", "Show this message") do
            warn opts
            exit 1
          end
        end
      end

      # Parse command_argv with command_option_parser, returning the options.
      def parse_command_argv
        # Passed nothing?  Show help.
        command_argv << '--help' if command_argv.empty?

        parser = command_option_parser
        parser.parse command_argv

        parser.options
      end

      # The pid written in the parent pid file.
      def parent_pid
        @parent_pid ||= read_pid("jobbable.pid")
      end

      # Do we own the current parent pid file, or the old one?
      def old_pidfile?
        @old_pidfile
      end
      attr_writer :old_pidfile

      # Record our pid in the parent pid file.
      def write_parent_pid
        write_pid "jobbable.pid", Process.pid
      end

      # Move the parent pid file to the old parent pid file.
      def age_parent_pid
        begin
          rename_pid "jobbable.pid", "jobbable-old.pid"
        rescue CantWritePidError
          zombie_pid = read_pid("jobbable-old.pid")

          begin
            Process.kill "KILL", zombie_pid
            logger.warn "Found old parent pid file for #{zombie_pid}, killed"
          rescue Errno::ESRCH
            logger.warn "Found stale moved parent pid file, removed it"
          end

          remove_pid "jobbable-old.pid"

          retry
        end
      end

      # Remove the parent pid file.
      #
      # If the old pid file is being used (when shutting down for restart),
      # remove it instead.
      def remove_parent_pid
        remove_pid old_pidfile? ? "jobbable-old.pid" : "jobbable.pid"
      end

      # Is the pid_directory writable (and so exists)?
      def check_pid_directory
        unless File.writable?(config.pid_directory)
          raise CantWritePidError.new(
            "#{config.pid_directory} doesn't exist or isn't writable")
        end
      end

      # Whine if the named pid file already exists.
      def check_pid_file(pid_file)
        pid_file = File.expand_path(pid_file, config.pid_directory)
        if File.exist?(pid_file)
          raise CantWritePidError.new("#{pid_file} already exists")
        end
        pid_file
      end

      # Write a pid to a file in the #pid_directory, unless it already exists.
      def write_pid(pid_file, pid)
        File.open(check_pid_file(pid_file), 'w') { |f| f << pid }
      end

      # Rename an existing pid file in the #pid_directory.
      #
      # This refuses to overwrite existing pid files.
      def rename_pid(old_name, new_name)
        old_name = File.expand_path(old_name, config.pid_directory)
        FileUtils.mv old_name, check_pid_file(new_name)
      end

      # Read the pid written in the given file.
      def read_pid(pid_file)
        File.read(File.expand_path(pid_file, config.pid_directory)).to_i
      end

      # Remove the given file from the #pid_directory.
      def remove_pid(pid_file)
        File.delete(File.expand_path(pid_file, config.pid_directory))
      end

      # Do OS-level voodoo to become a background process
      # TODO: this is all posix-specific.
      def daemonize!
        return unless config.daemon

        exit 0 if fork
        Process.setsid
        exit 0 if fork

        STDIN.reopen "/dev/null"
        STDOUT.reopen "/dev/null", "a"
        STDERR.reopen STDOUT

        Dir.chdir "/"
        File.umask 0000
      end

    end
  end
end
