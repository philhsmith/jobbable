require 'jobbable/cli/command'
require 'jobbable/parent'

module Jobbable
  module Cli
    class Start < Command

      summary "Start a cluster of worker processes"

      def perform
        Parent.new(command_line).instance_eval do
          check_pid_directory
          check_pid_file "jobbable.pid"
          open_logger
          start
        end
      end

    end
  end
end
