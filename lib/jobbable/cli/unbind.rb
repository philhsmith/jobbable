require 'jobbable/cli/command'
require 'jobbable/amqp/configured_amqp_mixin'

module Jobbable
  module Cli
    class Unbind < Command
      include Amqp::ConfiguredAmqpMixin

      summary "Unbind a queue from an exchange"

      # Declare unbinding options.
      def build_command_option_parser(opts)
        opts.on("--exchange E", "Unbind from exchange E") do |e|
          opts.options[:exchange] = e
        end

        opts.on("--queue Q", "Unbind queue E") do |q|
          opts.options[:queue] = q
        end

        opts.on("--key key", "Unbind routing key K") do |k|
          opts.options[:key] = k
        end
      end

      # Unbind the given queue from the given exchange.
      def perform
        options = parse_command_argv

        unless exchange = options.delete(:exchange)
          raise ArgumentError.new("--exchange is required")
        end

        unless queue = options.delete(:queue)
          raise ArgumentError.new("--queue is required")
        end

        start
        amqp.unbind exchange, queue, options
        stop
      end

    end
  end
end
