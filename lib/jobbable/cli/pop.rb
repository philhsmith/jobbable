require 'eventmachine'

require 'jobbable/facade'
require 'jobbable/cli/command'
require 'jobbable/logger'
require 'jobbable/amqp/subscriber_mixin'

module Jobbable
  module Cli
    class Pop < Command
      include Amqp::SubscriberMixin

      summary "Pop messages off of a queue"

      # Declare popping options.
      def build_command_option_parser(opts)
(
<<-NOTES
  When writing messages as JSON, each message is written as a bare hash (and
  not wrapped in array.)  Beware, this means that consuming parsers might need
  to be called in a loop.

  Note: --queue may be passed multiple times.

NOTES
).split("\n").each { |line| opts.separator line }

        opts.options.merge!(
          :queues   => [],
          :file     => "-",
          :count    => 0,
          :timeout  => 2
        )

        opts.on("--queue Q", "Pop from queue Q") do |q|
          opts.options[:queues] << q
        end

        opts.on("--file F", "Path to write messages to or - (-)") do |f|
          opts.options[:file] = f
        end

        opts.on("--count C", Integer,
          "Number of messages, or 0 to disable (0)") do |c|
            opts.options[:count] = c
        end

        opts.on("--timeout T", Integer,
          "Delivery timeout in seconds, or 0 to disable (2)") do |t|
            opts.options[:timeout] = t
        end
      end

      # The maximum number of messages to pop.
      attr_accessor :count

      # Shut down.
      def on_delivery_timeout
        shutdown
      end

      # Close the amqp connection and quit.
      def shutdown
        stop { EM.stop_event_loop }
      end

      # Subscribe to the queues, writing messages as json to the given io.
      #
      # Note this is what actually runs eventmachine.
      def pop_messages(queues, io)
        EM.run do
          # Subscribe to the queues
          subscribe queues do |message|

            # Write the message
            io << Jobbable::JSON.encode({
              :key => message.header.routing_key,
              :exchange => message.header.exchange,
              :message => message.body
            })

            io << "\n"

            # And ack it
            message.ack

            # Shutdown if we've drained enough
            shutdown if count and 0 < count and count <= counters[:started]

          end
        end
      end

      # Pop one or more messages off of a queue to a file.
      def perform
        options = parse_command_argv

        # No queues? Nothing to do
        if options[:queues].empty?
          raise ArgumentError.new("No queues given, pass some with --queue")
        end

        # Set the count and timeout
        self.count = options[:count]
        self.delivery_timeout = options[:timeout]

        # Open the IO we'll write to and pop the messages.
        if options[:file] == "-"
          # Log to stderr to avoid interfering with output
          Jobbable.logger = STDERR_LOGGER
          pop_messages options[:queues], STDOUT
        else
          File.open(options[:file], "w") do |file|
            pop_messages options[:queues], file
          end
        end
      end

    end
  end
end
