require 'yaml'

require 'jobbable/cli/command'

module Jobbable
  module Cli
    class Show < Command

      summary "Show the effective configuration"

      # Describe the current environment.
      def perform
        # Show the effective configuration.
        logger.info YAML.dump(config.to_hash)
      end

    end
  end
end
