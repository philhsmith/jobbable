require 'jobbable/cli/command'
require 'jobbable/cli/start'

module Jobbable
  module Cli
    class Restart < Command

      summary "Gracefully restart a cluster of workers"

      # Signal a graceful restart or just start if no instance was found.
      def perform
        begin
          # Move the parent pid to the old parent pid
          age_parent_pid

          # Get the old parent's pid
          old_parent_pid = read_pid "jobbable-old.pid"

          # Signal restart
          logger.warn "Restarting #{old_parent_pid}"
          Process.kill "USR1", old_parent_pid
        rescue Errno::ENOENT => no_parent_pid_file
          logger.info "No running jobbable found, starting"
        rescue Errno::ESRCH => pid_file_appears_stale
          logger.warn "The pid file appears to be stale, starting"
          remove_pid "jobbable-old.pid"
        ensure
          Start.new(command_line).perform
        end
      end

    end
  end
end
