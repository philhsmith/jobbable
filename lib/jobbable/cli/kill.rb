require 'jobbable/cli/command'

module Jobbable
  module Cli
    class Kill < Command

      summary "Immediately stop a cluster of workers"

      # Forcefully stop an existing parent.
      def perform
        begin
          logger.warn "Killing #{parent_pid}"
          Process.kill "KILL", parent_pid
          remove_parent_pid
        rescue Errno::ENOENT => no_parent_pid_file
          logger.warn no_parent_pid_file
        rescue Errno::ESRCH => pid_file_appears_stale
          logger.warn "The pid file appears to be stale, removing"
          remove_parent_pid
        end
      end

    end
  end
end
