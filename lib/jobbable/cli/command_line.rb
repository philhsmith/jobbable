require 'jobbable/facade'
require 'jobbable/logger'
require 'jobbable/config_mixin'
require 'jobbable/cli/kill'
require 'jobbable/cli/pop'
require 'jobbable/cli/publish'
require 'jobbable/cli/restart'
require 'jobbable/cli/show'
require 'jobbable/cli/start'
require 'jobbable/cli/stop'
require 'jobbable/cli/unbind'

module Jobbable
  module Cli
    class CommandLine
      include ConfigMixin

      # The name of the command to run.
      attr_accessor :verb

      # The logger.
      def logger
        Jobbable.logger
      end

      # Entry point for controlling jobbable from the command line.
      def main(argv)
        # Log to stderr
        Jobbable.logger = STDERR_LOGGER

        # Save the starting directory
        self.working_directory = Dir.pwd

        # Pluck the verb, environment and config file off, save the rest.
        self.verb         = argv.delete_at(0) unless argv.first =~ /^--/
        self.environment  = argv.delete_at(0) unless argv.first =~ /^--/
        self.config_file  = argv.delete_at(0) unless argv.first =~ /^--/
        self.argv         = argv

        # Dump help and die if called with an unknown (or no) verb
        argv << "--help" unless Command.verbs.include?(verb)

        # Force the configuration to load.
        config

        logger.info "Loading %s with %s" % [
          config_file ? "#{environment} from #{config_file}" : "nothing",
          argv.empty? ? "no command line options" : argv.join(' ')
        ]

        # Perform the requested action (by creating it and calling #perform)
        begin
          eval(verb.capitalize).new(self).perform
        rescue CantOpenLogError => exception
          puts exception.to_s
          exit 1
        rescue StandardError => exception
          logger.fatal exception
          exit 1
        end unless argv.include? "--help"
      end

    end
  end
end
