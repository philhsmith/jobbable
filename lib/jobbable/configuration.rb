require 'yaml'

require 'jobbable/config/config_hash'
require 'jobbable/config/slot_mixin'
require 'jobbable/config/job_config'
require 'jobbable/config/exchange_config'
require 'jobbable/config/queue_config'
require 'jobbable/config/bind_config'
require 'jobbable/config/worker_config'
require 'jobbable/config/funnel_config'
require 'jobbable/cli/command'

module Jobbable
  # General options.
  class Configuration < Config::ConfigHash
    include Config::SlotMixin

    config_property :debug, :default => false, :type => :boolean,
      :description => "Dump out extra debugging information"

    config_property :daemon, :default => true, :type => :boolean,
      :description => "Detach and run in the background"

    config_property :require_paths, :default => [], :type => Array,
      :description => "Search paths for job classes"

    config_property :autorequire, :default => nil, :optional => true,
      :description => "File to require before starting any jobs"

    config_property :log_file, :default => "jobbable.log",
      :description => "Path to the log file, or one of syslog, - or none"

    config_property :pids, :default => ".",
      :description => "Where to write pid files"

    config_property :chdir, :default => false, :type => :boolean,
      :description => "Change the directory to root?"

    config_property :host, :default => "localhost",
      :description => "Host of the AMQP broker"

    config_property :port, :default => 5672, :type => Integer,
      :description => "Port of the AMQP broker"

    config_property :virtual_host, :default => "/",
      :description => "AMQP virtual host to connect to"

    config_property :username, :default => nil, :optional => true,
      :description => "AMQP user to connect as"

    config_property :password, :default => nil, :optional => true,
      :description => "Password of the AMQP user"

    config_property :connection_timeout,
      :default => 5, :optional => true, :type => Integer,
      :description => "AMQP connection timeout in seconds"

    config_property :amqp_logging, :default => false, :type => :boolean,
      :description => "Enables low-level AMQP logging"

    # The directory to resolve relative paths from.
    attr_accessor :working_directory

    CHILDREN = [{
        :singular => 'job',
        :plural   => 'jobs',
        :type     => Config::JobConfig
      }, {
        :singular => 'exchange',
        :plural   => 'exchanges',
        :type     => Config::ExchangeConfig
      }, {
        :singular => 'queue',
        :plural   => 'queues',
        :type     => Config::QueueConfig
      }, {
        :singular => 'bind',
        :plural   => 'binds',
        :type     => Config::BindConfig
      }, {
        :singular => 'worker',
        :plural   => 'workers',
        :type     => Config::WorkerConfig
      }, {
        :singular => 'funnel',
        :plural   => 'funnels',
        :type     => Config::FunnelConfig
    }].freeze

    CHILDREN.each do |child|
      # Collection of child configurations by name, equipped with a default.
      define_method child[:plural].to_sym do
        self[child[:plural]] ||= Hash.new do |h, k|
          h[k] = h['default'].dup

          # If the child has a 'name' property, set it to the config name
          h[k].name = k if h[k].respond_to? :name=

          h[k]
        end.tap { |h| h['default'] = child[:type].new }
      end
    end

    # Parse the given array of words as if it were command line options.
    def from_command_line(argv)
      # Dup to avoid mutating the array.
      argv = argv.dup
      option_parser.parse! argv
      argv
    end

    # Build an OptionParser for reading command line options.
    def option_parser
      # Do not memoize, as our extensions make it stateful between parses
      OptionParser.new do |opts|
        opts.banner = "Usage: jobbable <action> [environment] " \
          "[path/to/jobbable.yml] [options] [-- action-specific options]"
(
<<-NOTES

The default environment is "development".

The default config file is sought at config/jobbable.yml and ./jobbable.yml.

Actions:

NOTES
).split("\n").each { |line| opts.separator line }

        Cli::Command.verbs.each do |verb|
          summary = Cli::Command.summaries[verb]
          opts.separator " " * 8 + verb.ljust(29) + summary
        end

        opts.separator ""
        opts.separator "Options:"

        opts.on_tail("-h", "--help", "Show this message") do
          warn opts
          exit 1
        end

        declare_simple_options(opts) { self }

        # The parser needs to recall the last child configuration discussed
        class <<opts
          attr_accessor *CHILDREN.collect { |child| child[:singular].to_sym }
        end

        CHILDREN.each do |child|
          plural    = child[:plural].to_sym
          singular  = child[:singular].to_sym

          # Options to change the 'current' considered child configuration
          opts.on("--#{singular} NAME",
            "Configure #{singular} NAME (default)") do |c|
            # Reference the child configuration now, forcing it to be created
            (send plural)[c]

            # And remember it for future child options
            opts.send :"#{singular}=", c
          end

          # Set child CLI options on the last mentioned child, or 'default'.
          child[:type].declare_simple_options(opts, :prefix => singular) do
            (send plural)[opts.send(singular) || 'default']
          end

          # This darkest voodoo is how one aborts an option parse..
          opts.on("--", "Stop parsing options") { throw :terminate }
        end
      end
    end

    # Load the given environment from the given config file.
    def from_file(file, environment)
      file = File.expand_path(file, working_directory) if working_directory
      unless yaml = YAML.load_file(file)[environment]
        raise ArgumentError.new("Unknown environment #{environment.inspect}")
      end

      # For each child subsection
      CHILDREN.each do |child|
        child_configs = send child[:plural]

        # If there's a declared configuration section
        if child_yaml = yaml.delete(child[:plural])

          # First set any declared defaults
          child_configs['default'].merge!(child_yaml.delete('default') || {})

          # Then merge in the rest of the declared child configs
          child_yaml.each { |name, config| child_configs[name].merge! config }

        end
      end

      merge! yaml
    end

    # Expand dynamic exchange, queue, bind and worker configs.
    #
    # If the name of such a configuration subsection includes a "dynamic slot"
    # such as {job}, then this method will delete that subsection and replace
    # it with possibly several copies.  Each copy will be renamed by
    # substituting known job names for {job}, which will also be substituted
    # into any occurances of {job} in the subsections property values.
    #
    # For example, one might trivially declare a single queue for each job and
    # bind them all to the default exchange with
    #
    #   queues:
    #     "{job}": { name: "{job}" }
    #
    #   binds:
    #     default:
    #       exchange: default
    #       key: "reject everything"
    #
    #     "{job}":  { queue: "{job}", key: "{job}.#" }
    #
    # Other dynamic slots may be included: job, attempt, state, priority and
    # host (but not tags).  More than one may be included, in which case an
    # expanded subsection will be added for each legal combination of slot
    # values.
    #
    # When an expanded subsection name collides with an existing subsection,
    # the existing subsection wins.  This allows exceptions to the general
    # template to be called out on a per-combination basis.
    #
    # When iterating over jobs, only configured ones will be considered.  Job
    # subsections themselves are not expanded, with one exception.  Job
    # exchanges may include {job}, in which case the job's own name (only)
    # will be substituted.  In this case the job section name should not
    # include {job}.
    def expand!
      # First expand just {job} in job exchanges to the job's own name
      jobs.each do |name, config|
        config.exchanges = expand_slots config.exchanges, 'job' => name
      end

      # Grab each job's configured attempt count.
      attempts = Hash[jobs.collect { |name, config| [name, config.attempts] }]

      # For each kind of child configuration BESIDES jobs
      CHILDREN.reject { |flavor| flavor[:singular] == 'job' }.each do |flavor|
        # Get the collection of child configurations
        children = send flavor[:plural]

        # Record which configuration names include slots to expand
        slots_to_expand = Hash[children.keys.collect do |name|
          [name, slot_names(name)]
        end].reject! { |name, slots| slots.empty? }

        # For each dynamic, template child config
        slots_to_expand.each do |name, slots|
          # Remove the template config
          template_config = children.delete name

          # And create as many copies of it as we need
          each_slot_expansion(slots, jobs.keys, attempts) do |expansion|
            # Name this new copy
            expanded = expand_slots name, expansion

            # Don't overwrite child configs with the expanded name
            next if children.key? expanded

            # Otherwise fill it with expanded values from the template config
            template_config.each do |property, value|
              children[expanded][property] = expand_slots value, expansion
            end
          end
        end
      end
    end

    # Is this configuration usable?
    #
    # Aside from validating its own properties, child configurations and
    # relationships between them are validated.
    #
    # Particularly,
    #
    # * Exchanges may not be declared with the configuration name 'all'
    # * Queues may not be declared with the configuration name 'all'
    # * Binds must refer to declared exchanges and queues, or 'all'
    # * Workers must refer to declared queues, or 'all'
    # * Funnels must refer to declared queues, or 'all'
    # * Jobs must refer to declared exchanges, or 'all'
    def valid?
      super

      CHILDREN.each do |child|
        configs = send child[:plural]

        configs.each do |name, config|
          errors.merge! Hash[config.errors.collect do |property, error|
            [property, "#{error} for #{child[:singular]} #{name}"]
          end] unless config.valid?
        end
      end

      # Exchanges can't be named 'all' (the AMQP name is unrestricted)
      if exchanges.keys.include?("all")
        errors['exchanges'] << "can't be named 'all'"
      end

      # Queues can't be named 'all' (the AMQP name is unrestricted)
      if queues.keys.include?("all")
        errors['queues'] << "can't be named 'all'"
      end

      # Binds must reference declared queues and exchanges (by their config
      # name) or 'all'
      binds.each do |name, bind|
        unless (['all'] + queues.keys).include? bind.queue
          errors['queue'] << \
            "has unknown queue '#{bind.queue.inspect}' on bind #{name}"
        end

        unless (['all'] + exchanges.keys).include? bind.exchange
          errors['exchange'] << \
            "has unknown exchange '#{bind.exchange.inspect}' on bind #{name}"
        end
      end

      # Workers must reference declared queues (by their config name) or 'all'
      workers.each do |name, worker|
        worker.queues.reject do |key|
          (['all'] + queues.keys).include?(key)
        end.each do |key|
          errors['queues'] << \
            "has unknown queue '#{key.inspect}' on worker #{name}"
        end
      end

      # Funnels must reference declared queues (by their config name) or 'all'
      funnels.each do |name, funnel|
        funnel.queues.reject do |key|
          (['all'] + queues.keys).include?(key)
        end.each do |key|
          errors['queues'] << \
            "has unknown queue '#{key.inspect}' on funnel #{name}"
        end
      end

      # Jobs must reference declared exchanges (by their config name) or 'all'
      jobs.each do |name, job|
        job.exchanges.reject do |key|
          (['all'] + exchanges.keys).include?(key)
        end.each do |key|
          errors['exchanges'] << \
            "has unknown exchange '#{key.inspect}' on job #{name}"
        end
      end

      # We are valid so long as we have no errors.
      errors.empty?
    end

    # Reload, expand and validate configuration from the file and options.
    def configure!(environment, config_file, command_line, working_directory)
      clear!

      self.working_directory = working_directory

      # Set this as early as possible
      self.debug = command_line.include? '--debug'
      CONSOLE_LOGGER.logs_backtraces = debug
      STDERR_LOGGER.logs_backtraces = debug

      from_file config_file, environment if config_file
      remainder = from_command_line command_line
      expand!
      validate!

      remainder
    end

    # #require_paths relative to #working_directory.
    def full_require_paths
      if working_directory
        require_paths.collect do |path|
          File.expand_path(path, working_directory)
        end
      else
        require_paths
      end
    end

    # #log_file relative to #working_directory.
    #
    # When log_file is none or syslog, this does not change it.
    def full_log_file
      if working_directory.nil? or %w{syslog none -}.include? log_file
        log_file
      else
        File.expand_path(log_file, working_directory)
      end
    end

    # #pids relative to #working_directory.
    def pid_directory
      working_directory ? File.expand_path(pids, working_directory) : pids
    end

    # Reset this configuration to the default state.
    def clear!
      defaults!

      CHILDREN.each do |child|
        configs = send child[:plural]

        configs.delete_if { |name, config| name != 'default' }
        configs['default'].defaults!
      end
    end

    # Overridden to convert child configurations as well.
    def to_hash
      super.tap do |hash|
        CHILDREN.each do |child|
          plural = child[:plural]

          hash[plural] = Hash[send(plural).collect do |name, child|
            [name, child.to_hash]
          end]
        end
      end
    end

  end
end
