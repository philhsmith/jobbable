require 'spec_helper'
require 'jobbable/parent'

describe Jobbable::Parent do

  # An example Agent subclass.
  class Special < Jobbable::Agent; end

  before :each do
    @command_line = mock("command line")
    @config = Jobbable::Configuration.new
    @command_line.stub!(:config).and_return(@config)

    # Silence the logger
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end
    @command_line.stub!(:logger).and_return(@logger)

    @parent = Jobbable::Parent.new(@command_line)

    # Neuter the relevant system calls
    @parent.stub!(:fork).and_return(1000) # returns as the parent
    @parent.stub!(:trap)
    @parent.stub!(:warn)
    @parent.stub!(:exit)
    @parent.stub!(:sleep)
    @parent.stub!(:daemonize!)

    Process.stub!(:pid).and_return(100)
    Process.stub!(:ppid).and_return(200)
    Process.stub!(:kill)
    Process.stub!(:wait)
    Dir.stub!(:chdir)
    Dir.stub!(:pwd).and_return('/the/current/working/directory')
    Thread.stub!(:new)
    @now = Time.now
    Time.stub!(:now).and_return(@now)

    # And some details about the configuration
    @config.stub!(:from_file)
    @config.stub!(:warn)
    @config.stub!(:exit)
    @config.stub!(:validate!)

    # Pretend our Special Agents have legit configs.
    @config.stub!(:specials).and_return(Hash.new do |h, k|
      h[k] = Jobbable::Config::AgentConfig.new
    end)
  end

  it "is a command" do
    @parent.should be_a Jobbable::Cli::Command
  end

  it "has a hash of agent metadata keyed by pid" do
    @parent.agents.should be_a Hash
  end

  describe "#spawnable_agents" do
    before :each do
      # First forget other agent counts
      Jobbable::Agent.subclasses.keys.each do |agent_type|
        @config.send(:"#{agent_type}s").each do |name, config|
          config.count = 0
        end
      end

      # Now some explicit counts
      @config.specials['james'].count = 3
      @config.specials['maxwell'].count = 5
      @config.specials['brock'].count = 2
    end

    it "is the remainder of config counts not taken by living agents" do
      @parent.agents.merge!(
        1 => { :type => "special", :name => "james" },
        2 => { :type => "special", :name => "maxwell" },
        3 => { :type => "special", :name => "maxwell" },
        4 => { :type => "special", :name => "maxwell" },
        5 => { :type => "special", :name => "maxwell" },
        6 => { :type => "special", :name => "brock" },
        7 => { :type => "special", :name => "brock" }
      )

      @parent.spawnable_agents.should == [
        { :type => "special", :name => "james" },
        { :type => "special", :name => "james" },
        { :type => "special", :name => "maxwell" }
      ]
    end

    it "gracefully ignores an over-abundance of agents" do
      @parent.agents.merge!(
        1 => { :type => "special", :name => "james" },
        2 => { :type => "special", :name => "maxwell" },
        3 => { :type => "special", :name => "maxwell" },
        4 => { :type => "special", :name => "maxwell" },
        5 => { :type => "special", :name => "maxwell" },
        6 => { :type => "special", :name => "brock" },
        7 => { :type => "special", :name => "brock" },

        # Notice that we have extra brocks for some reason.
        8 => { :type => "special", :name => "brock" },
        9 => { :type => "special", :name => "brock" },
        10 => { :type => "special", :name => "brock" },
        11 => { :type => "special", :name => "brock" }
      )

      @parent.spawnable_agents.should == [
        { :type => "special", :name => "james" },
        { :type => "special", :name => "james" },
        { :type => "special", :name => "maxwell" }
      ]
    end
  end

  describe "#spawn" do
    before :each do
      @agent_config = @config.specials['james']

      @agent = mock("agent")
      Special.stub!(:new).with(@config, @agent_config).and_return(@agent)

      @parent.stub!(:fork).and_return(1000) # returns as the parent
      @parent.stub!(:parent_pid).and_return(1000)
      @parent.stub!(:write_pid)
      @parent.stub!(:remove_pid)

      @agent.stub!(:main)
      @agent.stub!(:pid=)
    end

    it "takes an agent metadata hash and creates an agent" do
      Special.should_receive(:new).with(
        @config, @agent_config).and_return(@agent)
      @parent.spawn :type => 'special', :name => 'james'
    end

    it "whines about unknown agent types" do
      lambda do
        @parent.spawn :type => 'spy', :name => 'james'
      end.should raise_error ArgumentError
    end

    it "forks" do
      @parent.should_receive(:fork).and_return(nil)
      @parent.spawn :type => 'special', :name => 'james'
    end

    describe "when forking as the parent" do
      it "inserts the agent type and name under its pid in #agents" do
        @parent.spawn :type => 'special', :name => 'james'
        @parent.agents[1000][:type].should == 'special'
        @parent.agents[1000][:name].should == 'james'
      end

      it "sets the time to reap as now plus the lifespan and afterlife" do
        @parent.spawn :type => 'special', :name => 'james'

        @parent.agents[1000][:reap_at].should == @now +
          @config.specials['james'].lifespan +
          @config.specials['james'].afterlife
      end

      it "records no timestamp if the lifespan is disabled" do
        @config.specials['james'].lifespan = 0
        @parent.spawn :type => 'special', :name => 'james'

        @parent.agents[1000].should_not have_key :reap_at
      end

      it "returns the new agent" do
        @parent.spawn(:type => 'special', :name => 'james').should == @agent
      end
    end

    describe "when forking as the child" do
      before :each do
        # Yielding causes the block to run
        @parent.stub!(:fork).and_yield
        Process.stub!(:pid).and_return(@agent_pid = 2000)
      end

      it "chdirs back to the starting directory if configured to" do
        @config.chdir = false
        Dir.should_receive(:chdir).with(@config.working_directory)
        @parent.spawn :type => 'special', :name => 'james'
      end

      it "does not chdir if not configured to" do
        @config.chdir = true
        Dir.should_not_receive(:chdir)
        @parent.spawn :type => 'special', :name => 'james'
      end

      it "records the pid" do
        Process.should_receive(:pid).and_return(@agent_pid)
        @parent.should_receive(:write_pid).with(
          "jobbable-special-#{@agent_pid}.pid", @agent_pid)
        @parent.spawn :type => 'special', :name => 'james'
      end

      it "unregisters SIGUSR1" do
        @parent.should_receive(:trap).with("USR1", "DEFAULT")
        @parent.spawn :type => 'special', :name => 'james'
      end

      it "reregisters SIGTERM to call Agent#shutdown" do
        # Yielding causes the block to run
        @parent.should_receive(:trap).with("TERM").and_yield
        @agent.should_receive(:shutdown)
        @parent.spawn :type => 'special', :name => 'james'
      end

      it "starts a thread watching for parent process death" do
        @parent.should_receive(:parent_pid).and_return(1000)
        Process.should_receive(:ppid).and_return(1)
        @parent.should_receive(:exit).with(2)

        # Yielding causes the block to run
        Thread.should_receive(:new).and_yield

        @parent.spawn :type => 'special', :name => 'james'
      end

      it "sets the Jobbable.parent singleton to the parent" do
        @parent.spawn :type => 'special', :name => 'james'
        Jobbable.parent.should == @parent
      end

      it "calls the agent's main" do
        @agent.should_receive(:main)
        @parent.spawn :type => 'special', :name => 'james'
      end

      it "removes the pid file on exit" do
        @parent.should_receive(:remove_pid).with(
          "jobbable-special-#{@agent_pid}.pid")
        @parent.spawn :type => 'special', :name => 'james'
      end

      it "removes the pid even on error" do
        @agent.should_receive(:main).and_raise("explosions")
        @parent.should_receive(:remove_pid).with(
          "jobbable-special-#{@agent_pid}.pid")
        @parent.spawn(:type => 'special', :name => 'james') rescue nil
      end
    end
  end

  describe "#start" do
    before :each do
      @parent.stub!(:fork).and_return(nil) # returns as the child
      @parent.stub!(:spawn_agents)
      @parent.stub!(:write_parent_pid)
      @parent.stub!(:remove_parent_pid)
    end

    it "daemonizes" do
      @parent.should_receive(:daemonize!)
      @parent.start
    end

    it "registers #shutdown on SIGTERM" do
      @parent.should_receive(:shutdown)

      # Yielding causes the block to run
      @parent.should_receive(:trap).with("TERM").and_yield

      @parent.start
    end

    it "registers #shutdown on SIGUSR1" do
      @parent.should_receive(:shutdown)

      # Yielding causes the block to run
      @parent.should_receive(:trap).with("USR1").and_yield

      @parent.start

      @parent.old_pidfile?.should be_true
    end

    it "records the pid" do
      @parent.should_receive(:write_parent_pid)
      @parent.start
    end

    it "spawns agents" do
      @parent.should_receive(:spawn_agents)
      @parent.start
    end

    it "cleans up the pid file on exit" do
      @parent.stub!(:spawn_agents)
      @parent.should_receive(:remove_parent_pid)
      @parent.start
    end

    it "cleans up the pid file even on exception" do
      @parent.stub!(:spawn_agents).and_raise("explosions!")
      @parent.should_receive(:remove_parent_pid)
      @parent.start rescue nil
    end
  end

  describe "#spawn_agents" do
    before :each do
      # One pass (it's called twice in a loop..)
      @parent.stub!(:shutting_down?).and_return(false, false, true)

      @parent.stub!(:spawnable_agents).and_return([])
      @parent.stub!(:reap_agents)
      @parent.stub!(:bury_agents)
    end

    it "spins in a sleep loop guarded by shutting_down? and agents.empty?" do
      @parent.stub!(:shutting_down?).and_return(false, true, true)

      # skip the first call, since it is short-circuited
      @parent.stub!(:agents).and_return({ :not => :empty }, {})

      @parent.should_receive(:sleep).twice.with(1)
      @parent.spawn_agents
    end

    it "spawns spawnable agents" do
      @parent.should_receive(:spawnable_agents).and_return([
        { :type => "stooge", :name => "curly" },
        { :type => "stooge", :name => "curly" },
        { :type => "stooge", :name => "larry" },
        { :type => "stooge", :name => "moe" }
      ])

      @parent.should_receive(:spawn).twice.
        with(:type => "stooge", :name => "curly")
      @parent.should_receive(:spawn).with(:type => "stooge", :name => "larry")
      @parent.should_receive(:spawn).with(:type => "stooge", :name => "moe")

      @parent.spawn_agents
    end

    it "reaps truant agents" do
      @parent.should_receive(:reap_agents)
      @parent.spawn_agents
    end

    it "buries dead agents" do
      @parent.should_receive(:bury_agents)
      @parent.spawn_agents
    end

    it "buries, but does not spawn agents while shutting down" do
      @parent.stub!(:shutting_down?).and_return(true)
      @parent.stub!(:agents).and_return({ :not => :empty }, {})

      @parent.should_not_receive(:spawnable_agents)
      @parent.should_not_receive(:spawn)
      @parent.should_receive(:bury_agents)

      @parent.spawn_agents
    end
  end

  describe "#reap_agents" do
    before :each do
      @an_agent = { :type => 'special', :name => 'james' }
    end

    it "SIGTERMs children whose reap_at has elapsed" do
      @parent.agents[1] = @an_agent.merge(:reap_at => @now - 5)
      Process.should_receive(:kill).with("TERM", 1)
      @parent.reap_agents
      @parent.agents[1][:sigterm_sent].should be_true
    end

    it "does not SIGTERM children whose reap_at has not elapsed" do
      @parent.agents[1] = @an_agent.merge(:reap_at => @now + 5)
      Process.should_not_receive(:kill).with("TERM", 1)
      @parent.reap_agents
    end

    it "does not SIGTERM children that have no reap_at" do
      @parent.agents[1] = @an_agent
      Process.should_not_receive(:kill).with("TERM", 1)
      @parent.reap_agents
    end

    it "does not SIGTERM the same child again" do
      @parent.agents[1] = @an_agent.merge(
        :reap_at => @now - 5, :sigterm_sent => true)
      Process.should_not_receive(:kill).with("TERM", 1)
      @parent.reap_agents
    end

    it "SIGKILLs children whose reap_at elapsed more than a minute ago" do
      @parent.agents[1] = @an_agent.merge(
        :reap_at => @now - 60, :sigterm_sent => true)
      Process.should_receive(:kill).with("KILL", 1)
      @parent.reap_agents
    end

    it "will continue to SIGKILL repeatedly" do
      @parent.agents[1] = @an_agent.merge(
        :reap_at => @now - 61, :sigterm_sent => true)
      Process.should_receive(:kill).with("KILL", 1)
      @parent.reap_agents
    end
  end

  describe "#bury_agents" do
    before :each do
      @parent.agents[17] = { :type => 'special', :name => 'default' }
    end

    it "notes dead agents" do
      Process.should_receive(:wait).with(
        0, Process::WNOHANG | Process::WUNTRACED).and_return(17, nil)
      @parent.bury_agents

      @parent.agents.should_not have_key 17
    end

    it "cleans up dead agent pid files as needed" do
      Process.stub!(:wait).with(
        0, Process::WNOHANG | Process::WUNTRACED).and_return(17, nil)

      @parent.should_receive(:remove_pid).with("jobbable-special-17.pid")

      @parent.bury_agents
    end
  end

  describe "#shutdown" do
    it "sets shutting_down" do
      @parent.shutdown
      @parent.should be_shutting_down
    end

    it "sets each child's reap_at to the current time" do
      @parent.agents.merge!(
        17 => { :type => 'special', :name => 'james' },
        32 => { :type => 'special', :name => 'james' },
        99 => { :type => 'special', :name => 'brock' }
      )

      @parent.shutdown

      @parent.agents.each { |pid, agent| agent[:reap_at].should == @now }
    end
  end

end
