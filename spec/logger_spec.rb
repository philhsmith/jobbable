require 'spec_helper'
require 'jobbable/logger'

describe Jobbable::LoggerMixin do
  before :each do
    @config = Jobbable::Configuration.new

    @mixer = Object.new.tap do |mixer|
      class <<mixer
        include Jobbable::LoggerMixin
      end
    end

    @mixer.stub!(:config).and_return(@config)
  end

  describe "#check_log_file" do
    describe "if the expanded log file exists" do
      it "whines if it is not writable" do
        full_log_file = @config.full_log_file

        File.should_receive(:exist?).with(full_log_file).and_return(true)
        File.should_receive(:writable?).with(full_log_file).and_return(false)

        lambda do
          @mixer.check_log_file
        end.should raise_error Jobbable::CantOpenLogError
      end
    end

    describe "if the log file does not exist, but its directory does" do
      it "whines if the directory is not writable" do
        full_log_file = @config.full_log_file
        log_dir = File.dirname(full_log_file)

        File.stub!(:exist?).with(full_log_file).and_return(false)
        File.should_receive(:exist?).with(log_dir).and_return(true)
        File.should_receive(:writable?).with(log_dir).and_return(false)

        lambda do
          @mixer.check_log_file
        end.should raise_error Jobbable::CantOpenLogError
      end
    end

    describe "if the log directory does not exist" do
      it "whines" do
        full_log_file = @config.full_log_file
        log_dir = File.dirname(full_log_file)

        File.stub!(:exist?).with(full_log_file).and_return(false)
        File.stub!(:exist?).with(log_dir).and_return(false)

        lambda do
          @mixer.check_log_file
        end.should raise_error Jobbable::CantOpenLogError
      end
    end

    %w{syslog none -}.each do |special_file|
      it "does nothing if log_file is #{special_file}" do
        @config.log_file = special_file

        File.stub!(:exist?).and_return(true)
        File.stub!(:writable?).and_return(false)

        lambda { @mixer.check_log_file }.should_not raise_error
      end
    end
  end

  describe "#open_logger" do
    before :each do
      File.stub!(:open).and_return(@open_file = mock("open file").tap do |f|
        f.stub!(:sync=)
        f.stub!(:puts)
        f.stub!(:close)
      end)

      @mixer.stub!(:check_log_file)
      Jobbable.stub!(:logger=)
    end

    it "points the logger at log_file if it is not syslog or none" do
      @config.log_file = "log/jobbable.log"
      File.should_receive(:open).with(
        "log/jobbable.log", 'a').and_return(@open_file)

      Jobbable.should_receive(:logger=)

      logger = @mixer.open_logger
      logger.should be_a Jobbable::PrefixLogger
      logger.wrapped.should be_a Jobbable::IOLogger
      logger.wrapped.io.should == @open_file

      Time.should_receive(:now).and_return(Time.at(0))
      Process.should_receive(:pid).and_return(17)
      logger.prefix_for("some message", :info).should == \
        "Dec 31 16:00:00    17  INFO"
    end

    it "checks and whines if the log file is not writable" do
      exception = Jobbable::CantOpenLogError.new
      @mixer.should_receive(:check_log_file).and_raise(exception)
      lambda do
        @mixer.open_logger
      end.should raise_error Jobbable::CantOpenLogError
    end

    it "sets the logger to the console logger log_file is -" do
      @config.log_file = "-"
      File.should_not_receive(:open)
      logger = @mixer.open_logger
      logger.should be_a Jobbable::IOLogger
      logger.io.should == STDOUT
    end

    it "sets the logger to a SyslogLogger if log_file is syslog" do
      @config.log_file = "syslog"
      @mixer.open_logger.should be_a Jobbable::SyslogLogger
    end

    it "sets the logger to the null logger if log_file is none" do
      @config.log_file = "none"
      @mixer.open_logger.should == Jobbable::NULL_LOGGER
    end
  end
end

describe Jobbable::ExceptionFormatter do
  before :each do
    @mixer = Object.new.tap do |mixer|
      class <<mixer
        include Jobbable::ExceptionFormatter
      end
    end

    @with_backtrace = StandardError.new("with backtrace")
    @with_backtrace.stub!(:backtrace).and_return([
      "fileA:lineA",
      "fileB:lineB",
    ])

    @without_backtrace = StandardError.new("without backtrace")
  end

  describe "#logs_backtraces" do
    it "is an attribute" do
      @mixer.logs_backtraces = :flag
      @mixer.logs_backtraces.should == :flag
    end
  end

  describe "#format_exception" do
    it "formats an exception as a string" do
      @mixer.format_exception(@with_backtrace).should == (<<EOS
with backtrace (StandardError)
	from fileA:lineA
	from fileB:lineB
EOS
).strip
    end

    it "handles exceptions without backtraces" do
      @mixer.format_exception(@without_backtrace).should == (<<EOS
without backtrace (StandardError)
	from outta nowhere
EOS
).strip
    end

    it "is the message.to_s if backtrace logging is false" do
      @mixer.logs_backtraces = false
      @mixer.format_exception(@with_backtrace).should == "with backtrace"
    end

    it "is the message.to_s if it is not an exception" do
      @mixer.format_exception(:not_an_exception).should == 'not_an_exception'
    end
  end
end

describe Jobbable::IOLogger do
  before :each do
    @destination = mock("destination")
    @destination.stub!(:is_a?).and_return(false)

    @io = mock("IO")
    @logger = Jobbable::IOLogger.new @destination
  end

  it "sends messages to its #io" do
    @logger.should_receive(:io).and_return(@io)
    @io.should_receive(:puts).with("the message")
    @logger.info "the message"
  end

  it "mixes in ExceptionFormatter" do
    @logger.should be_a Jobbable::ExceptionFormatter
  end

  describe "#io" do
    it "lazy-initializes to #open" do
      @logger.io = nil
      @logger.should_receive(:open).and_return(@io)
      @logger.io.should == @io
    end
  end

  describe "#open" do
    it "opens io as a filename for append and sets sync if it is a string" do
      @destination.should_receive(:is_a?).with(String).and_return(true)

      File.should_receive(:open).with(@destination, "a").and_return(@io)
      @io.should_receive(:sync=).with(true)

      @logger.open.should == @io
    end

    it "is the destination if it is an IO" do
      @destination.should_receive(:is_a?).with(IO).and_return(true)
      @logger.open.should == @destination
    end

    it "is STDOUT otherwise" do
      @logger.open.should == STDOUT
    end
  end

  describe "#close" do
    it "closes the io" do
      @logger.io = @io
      @io.should_receive(:close)
      @logger.close
    end

    it "refuses to close STDOUT" do
      STDOUT.should_not_receive(:close)
      Jobbable::IOLogger.new(STDOUT).close
    end

    it "refuses to close STDERR" do
      STDERR.should_not_receive(:close)
      Jobbable::IOLogger.new(STDERR).close
    end
  end
end

describe Jobbable::CONSOLE_LOGGER do
  it "is an IOLogger pointed at STDOUT" do
    Jobbable::CONSOLE_LOGGER.should be_a Jobbable::IOLogger
    Jobbable::CONSOLE_LOGGER.io.should == STDOUT
  end

  it "does not log backtraces" do
    Jobbable::CONSOLE_LOGGER.logs_backtraces.should == false
  end
end

describe Jobbable::STDERR_LOGGER do
  it "is an IOLogger pointed at STDERR" do
    Jobbable::STDERR_LOGGER.should be_a Jobbable::IOLogger
    Jobbable::STDERR_LOGGER.io.should == STDERR
  end

  it "does not log backtraces" do
    Jobbable::STDERR_LOGGER.logs_backtraces.should == false
  end
end

describe Jobbable::NULL_LOGGER do
  it "drops messages on the floor" do
    Jobbable::NULL_LOGGER.should_not_receive(:puts)
    Jobbable::NULL_LOGGER.should_not_receive(:warn)
    Syslog.should_not_receive(:info)

    Jobbable::NULL_LOGGER.info "the message"
  end
end

describe Jobbable::PrefixLogger do
  before :each do
    @wrapped = mock("underlying logger")
    @logger = Jobbable::PrefixLogger.new(@wrapped) { "prefix" }
  end

  %w{fatal error warn info debug}.each do |level|
    it "sends #{level} to the wrapped logger, prefixed by the block value" do
      @wrapped.should_receive(level.to_sym).with("prefix message")
      @logger.send level, "message"
    end
  end

  it "delegates #close to the underlying logger" do
    @wrapped.should_receive(:close)
    @logger.close
  end

  it "sends #format_exception to the wrapped logger, if it responds to it" do
    @wrapped.should_receive(:respond_to?).with(
      :format_exception).and_return(true)

    exception = StandardError.new("explosions!")
    @wrapped.should_receive(:format_exception).with(
      exception).and_return(:formatted_exception)

    @logger.format_exception(exception).should == :formatted_exception
  end

  it "passes exceptions through if the wrapped logger does not " \
    "respond to #format_exception" do

    @wrapped.should_receive(:respond_to?).with(
      :format_exception).and_return(false)

    exception = StandardError.new("explosions!")
    @logger.format_exception(exception).should == exception
  end
end

# Just so we don't need to require in the real one
module Syslog
  # Override the private method of the same name
  def self.open; end
end

describe Jobbable::SyslogLogger do
  before :each do
    Syslog.stub!(:open)
    Syslog.stub!(:opened?).and_return(true)
    Syslog.stub!(:close)

    @logger = Jobbable::SyslogLogger.new
    @logger.stub!(:require)
  end

  {
    :fatal  => :crit,
    :error  => :err,
    :warn   => :warning,
    :info   => :info,
    :debug  => :debug
  }.each do |local_method, syslog_method|
    before :each do
      Syslog.stub!(syslog_method)
    end

    it "sends #{local_method} to syslog's #{syslog_method}" do
      Syslog.should_receive(syslog_method).with("the message")
      @logger.send local_method, "the message"
    end

    it "escapes percent signs" do
      Syslog.should_receive(syslog_method).with(
        "the %%s printf %%d reactive %%f %% message")
      @logger.send local_method, "the %s printf %d reactive %f % message"
    end

    it "lazily opens" do
      @logger.should_receive(:open?).and_return(false)
      @logger.should_receive(:open)
      @logger.send local_method, "the message"
    end
  end

  describe "#open" do
    it "requires and opens Syslog" do
      @logger.should_receive(:require).with("syslog")
      Syslog.should_receive(:open)
      @logger.open
    end
  end

  describe "#close" do
    it "closes Syslog if it is open" do
      Syslog.should_receive(:opened?).and_return(true)
      Syslog.should_receive(:close)
      @logger.close
    end
  end
end
