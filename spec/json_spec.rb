require 'spec_helper'
require 'jobbable/json'

describe Jobbable::JSON do
  describe ".encode" do
    it "delegates to Jobbable::JSON.encode" do
      ActiveSupport::JSON.should_receive(:encode).with(:o).and_return("json")
      Jobbable::JSON.encode(:o).should == "json"
    end

    it "skips delegating in the case of encoding an empty hash" do
      ActiveSupport::JSON.should_not_receive(:encode)
      Jobbable::JSON.encode({}).should == "{}"
    end
  end

  describe ".decode" do
    it "delegates to Jobbable::JSON.decode" do
      ActiveSupport::JSON.should_receive(:decode).with("json").and_return(:o)
      Jobbable::JSON.decode("json").should == :o
    end

    it "javascript-escapes raw non-ascii unicode characters first" do
      json = "\302\256" # the registered trademark symbol
      ActiveSupport::JSON.should_receive(:decode).
        with("\\u00ae").and_return(:o)
      Jobbable::JSON.decode(json).should == :o
    end

    it "mixes Jobbable::JSON::ParseError into raised exceptions" do
      ActiveSupport::JSON.should_receive(:decode).and_raise(StandardError)

      lambda do
        Jobbable::JSON.decode "json"
      end.should raise_error(Jobbable::JSON::ParseError)
    end
  end
end
