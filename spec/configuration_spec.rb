require 'spec_helper'

require "erb"
require "ostruct"

require 'jobbable/configuration'

describe Jobbable::Configuration do
  before :each do
    @config = Jobbable::Configuration.new
  end

  {
    :debug              => false,
    :daemon             => true,
    :require_paths      => [],
    :autorequire        => nil,
    :log_file           => "jobbable.log",
    :pids               => ".",
    :chdir              => false,
    :host               => "localhost",
    :port               => 5672,
    :virtual_host       => "/",
    :username           => nil,
    :password           => nil,
    :connection_timeout => 5,
    :amqp_logging       => false
  }.each do |option, default_value|

    it "includes #{option} with default value #{default_value.inspect}" do
      @config.send(option).should == default_value
      @config.send :"#{option}=", :new_value
      @config.send(option).should == :new_value
    end

  end

  it "mixes in slots" do
    @config.should be_a Jobbable::Config::SlotMixin
  end

  describe "#jobs" do
    it "is a hash taking 'default' to a JobConfig" do
      @config.jobs['default'].should be_a(Jobbable::Config::JobConfig)
    end

    it "lazily sets unknown keys to copies of 'default'" do
      @config.jobs['some_job'].should == @config.jobs['default']
      # Recall "equal" tests object identity (i.e. memory address)
      @config.jobs['some_job'].should_not equal @config.jobs['default']
    end
  end

  describe "#exchanges" do
    it "is a hash taking 'default' to an ExchangeConfig" do
      @config.exchanges['default'].should \
        be_a(Jobbable::Config::ExchangeConfig)
    end

    it "lazily sets unknown keys to copies of 'default', except name" do
      (@config.exchanges['default'].keys - %w{name}).each do |p|
        @config.exchanges['some_exchange'][p].should == \
          @config.exchanges['default'][p]
      end
    end

    it "sets the default 'name' property to the configuration name" do
      @config.exchanges['some_exchange'].name.should == 'some_exchange'
    end
  end

  describe "#queues" do
    it "is a hash taking 'default' to a QueueConfig" do
      @config.queues['default'].should be_a(Jobbable::Config::QueueConfig)
    end

    it "lazily sets unknown keys to copies of 'default', except name" do
      (@config.queues['default'].keys - %w{name}).each do |p|
        @config.queues['some_queue'][p].should == @config.queues['default'][p]
      end
    end

    it "sets the default 'name' property to the configuration name" do
      @config.queues['some_queue'].name.should == 'some_queue'
    end
  end

  describe "#binds" do
    it "is a hash taking 'default' to a BindConfig" do
      @config.binds['default'].should be_a(Jobbable::Config::BindConfig)
    end

    it "lazily sets unknown keys to copies of 'default'" do
      @config.binds['some_bind'].should == @config.binds['default']
      # Recall "equal" tests object identity (i.e. memory address)
      @config.binds['some_bind'].should_not equal @config.binds['default']
    end
  end

  describe "#workers" do
    it "is a hash taking 'default' to a WorkerConfig" do
      @config.workers['default'].should be_a(Jobbable::Config::WorkerConfig)
    end

    it "lazily sets unknown keys to copies of 'default'" do
      @config.workers['some_worker'].should == @config.workers['default']
      # Recall "equal" tests object identity (i.e. memory address)
      @config.workers['some_worker'].should_not equal \
        @config.workers['default']
    end
  end

  describe "#funnels" do
    it "is a hash taking 'default' to a FunnelConfig" do
      @config.funnels['default'].should be_a(Jobbable::Config::FunnelConfig)
    end

    it "lazily sets unknown keys to copies of 'default'" do
      @config.funnels['some_funnel'].should == @config.funnels['default']
      # Recall "equal" tests object identity (i.e. memory address)
      @config.funnels['some_funnel'].should_not equal \
        @config.funnels['default']
    end
  end

  describe "#from_command_line" do
    it "parses an ARGV-like array of strings" do
      @config.from_command_line \
        %w{--log-file /some/file.log --pids /some/pid/dir}
      @config.log_file.should == "/some/file.log"
      @config.pids.should == "/some/pid/dir"
    end

    it "#warns the option parser (as help) on --help or -h and exits" do
      @config.should_receive(:warn).with(an_instance_of OptionParser)
      @config.should_receive(:exit).with(1)
      @config.from_command_line %w{--help}
    end

    it "sets integer options" do
      @config.from_command_line %w{--port 1337}
      @config.port.should == 1337
    end

    it "lazily initializes child configurations on first mention" do
      @config.from_command_line %w{--job make_lunch}
      @config.jobs.keys.should include 'make_lunch'
    end

    describe "on a job option" do
      it "initially configures the default job" do
        @config.from_command_line %w{--job-timeout 600}
        @config.jobs['default'].timeout.should == 600
      end

      it "configures whatever job was last declared with --job" do
        @config.from_command_line %w{--job make_lunch --job-timeout 600}
        @config.jobs['make_lunch'].timeout.should == 600
      end
    end

    describe "on an exchange option" do
      it "initially configures the default exchange" do
        @config.from_command_line %w{--exchange-type fanout}
        @config.exchanges['default'].type.should == "fanout"
      end

      it "configures whatever exchange was last declared with --exchange" do
        @config.from_command_line %w{--exchange clock --exchange-type fanout}
        @config.exchanges['clock'].type.should == "fanout"
      end
    end

    describe "on a queue option" do
      it "initially configures the default queue" do
        @config.from_command_line %w{--queue-type pop}
        @config.queues['default'].type.should == "pop"
      end

      it "configures whatever queue was last declared with --queue" do
        @config.from_command_line %w{--queue retry --queue-type pop}
        @config.queues['retry'].type.should == "pop"
      end
    end

    describe "on a bind option" do
      it "initially configures the default bind" do
        @config.from_command_line %w{--bind-key *}
        @config.binds['default'].key.should == "*"
      end

      it "configures whatever bind was last declared with --bind" do
        @config.from_command_line %w{--bind retry --bind-key *.retry.#}
        @config.binds['retry'].key.should == "*.retry.#"
      end
    end

    describe "on a worker option" do
      it "initially configures the default worker" do
        @config.from_command_line %w{--worker-count 2}
        @config.workers['default'].count.should == 2
      end

      it "configures whatever worker was last declared with --worker" do
        @config.from_command_line %w{--worker high_prio --worker-count 2}
        @config.workers['high_prio'].count.should == 2
      end
    end

    describe "on a funnel option" do
      it "initially configures the default funnel" do
        @config.from_command_line %w{--funnel-count 2}
        @config.funnels['default'].count.should == 2
      end

      it "configures whatever funnel was last declared with --funnel" do
        @config.from_command_line %w{--funnel high_prio --funnel-count 2}
        @config.funnels['high_prio'].count.should == 2
      end
    end
  end

  describe "#from_file" do
    before :each do
      @yaml = <<-YAML
      ---
      development:
        pids: tmp/my-pids
        host: amqp.example.com
        port: 5673
        virtual_host: /some/vhost
        username: myapp
        password: 1337haxx
        require_paths: [jobs]

        jobs:
          default:
            timeout: 300
            attempts: 10

          slow_job:
            timeout: 600
            exchanges: [slow_exchange]

        exchanges:
          default:
            type: topic
            name: myapp

          fanout_exchange:
            type: fanout
            name: my_fanout
            internal: true

          slow_exchange:
            name: my_slow_exchange

        queues:
          default:
            type: subscribe
            name: myapp
            inflight: 100

          retry_queue:
            type: pop
            name: my_retry

          high_priority:
            name: urgent

          tiny:
            name: to_aggregate

        binds:
          default:
            key: some_job.#

          high_prio:
            queue: high_priority
            key: high_priority.#

        workers:
          default:
            count: 3

          high_prio:
            count: 1
            queues: [high_priority]

        funnels:
          default:
            count: 1

          tiny:
            queues: [tiny]
      YAML

      YAML.stub!(:load_file).and_return(@parsed = YAML.load(@yaml))
    end

    it "reads YAML from the named file, relative to #working_directory" do
      @config.working_directory = '/here'
      YAML.should_receive(:load_file).with(
        '/here/config/jobbable.yml').and_return(@parsed)
      @config.from_file('config/jobbable.yml', 'development')
    end

    it "reads from the current directory if #working_directory is nil" do
      @config.working_directory = nil
      YAML.should_receive(:load_file).with(
        'config/jobbable.yml').and_return(@parsed)
      @config.from_file('config/jobbable.yml', 'development')
    end

    it "takes configuration from the passed environment" do
      @config.from_file('config/jobbable.yml', 'development')
      @config.pids.should == 'tmp/my-pids'
    end

    it "takes child-specific configuration" do
      @config.from_file('config/jobbable.yml', 'development')

      @config.jobs['default'].timeout.should == 300

      # This should be pulled from default, which must be set first
      @config.jobs['slow_job'].attempts.should == 10

      @config.jobs['slow_job'].exchanges.should == ['slow_exchange']
      @config.exchanges['fanout_exchange'].type.should == 'fanout'
      @config.queues['retry_queue'].type.should == 'pop'
      @config.queues['high_priority'].name.should == 'urgent'
      @config.binds['default'].key.should == 'some_job.#'
      @config.binds['high_prio'].key.should == 'high_priority.#'
      @config.workers['high_prio'].count.should == 1
      @config.workers['high_prio'].queues.should include 'high_priority'
      @config.funnels['tiny'].queues.should include 'tiny'
    end

    # Beware, this spec performs honest-to-god IO
    it "produces a valid configuration from the template config file" do
      # Where's the template?
      template = File.expand_path('../../lib/generators/jobbable/install/' \
        'templates/config/jobbable.yml.tt', __FILE__)

      # Fake out the locals needed by the ERB template
      context = OpenStruct.new(:app_name => 'my_app').send :binding

      # Render the erb and parse the resulting yaml, mock the #load_file call
      parsed = YAML.load(ERB.new(File.read(template)).result(context))
      YAML.stub!(:load_file).with('config/jobbable.yml').and_return(parsed)

      @config.from_file('config/jobbable.yml', 'development')
      lambda { @config.validate! }.should_not raise_error
    end
  end

  describe "#expand!" do
    before :each do
      @config.jobs['foo'].attempts = 2
      @config.jobs['bar'].attempts = 3
    end

    it "expands child configs whose names include slots" do
      @config.queues['{job}_queue'].name = "amqp_queue_for_{job}"

      @config.expand!

      @config.queues['foo_queue'].name.should == "amqp_queue_for_foo"
      @config.queues['bar_queue'].name.should == "amqp_queue_for_bar"
    end

    it "can expand exchange, bind, worker and funnel configs" do
      @config.exchanges['{job}_exchange'].name = "amqp_exchange_for_{job}"
      @config.binds['{job}_bind'].queue = "a_queue_for_{job}"
      @config.binds['{job}_bind'].exchange = "an_exchange_for_{job}"
      @config.workers['{job}_worker'].queues = ["a_queue_for_{job}"]
      @config.funnels['{job}_funnel'].queues = ["a_queue_for_{job}"]

      @config.expand!

      @config.exchanges['foo_exchange'].name.should == "amqp_exchange_for_foo"
      @config.binds['foo_bind'].queue.should == "a_queue_for_foo"
      @config.binds['foo_bind'].exchange.should == "an_exchange_for_foo"
      @config.workers['foo_worker'].queues.should == ["a_queue_for_foo"]
      @config.funnels['foo_funnel'].queues.should == ["a_queue_for_foo"]

      @config.exchanges['bar_exchange'].name.should == "amqp_exchange_for_bar"
      @config.binds['bar_bind'].queue.should == "a_queue_for_bar"
      @config.binds['bar_bind'].exchange.should == "an_exchange_for_bar"
      @config.workers['bar_worker'].queues.should == ["a_queue_for_bar"]
      @config.funnels['bar_funnel'].queues.should == ["a_queue_for_bar"]
    end

    it "doesn't overwrite existing child configs" do
      @config.queues['{job}_queue'].name = "amqp_queue_for_{job}"
      @config.queues['foo_queue'].name = "already_got_my_name"

      @config.expand!

      @config.queues['foo_queue'].name.should == "already_got_my_name"
      @config.queues['bar_queue'].name.should == "amqp_queue_for_bar"
    end

    it "expands {job} in job exchanges to the name of the job considered" do
      @config.jobs['foo'].exchanges = ["{job}_exchange"]
      @config.expand!
      @config.jobs['foo'].exchanges.should == %w{foo_exchange}
    end
  end

  describe "#valid?" do
    it "is true for the default configuration" do
      @config.should be_valid
      @config.errors.should be_empty
    end

    it "complains if child configurations are invalid" do
      @config.jobs['default'].timeout = nil
      @config.should_not be_valid
      @config.errors['timeout'].should_not be_empty
    end

    it "rejects exchanges named 'all'" do
      @config.exchanges['all'].type = 'fanout'
      @config.should_not be_valid
      @config.errors['exchanges'].should_not be_empty
    end

    it "rejects queues named 'all'" do
      @config.queues['all'].type = 'pop_star'
      @config.should_not be_valid
      @config.errors['queues'].should_not be_empty
    end

    it "requires bind queues to be queue names" do
      @config.binds['default'].queue = 'unknown_queue'
      @config.should_not be_valid
      @config.errors['queue'].should_not be_empty
    end

    it "allows the bind queue 'all'" do
      @config.binds['default'].queue = 'all'
      @config.should be_valid
      @config.errors['queue'].should be_empty
    end

    it "requires bind exchanges to be exchange names" do
      @config.binds['default'].exchange = 'unknown_exchange'
      @config.should_not be_valid
      @config.errors['exchange'].should_not be_empty
    end

    it "allows the bind exchange 'all'" do
      @config.binds['default'].exchange = 'all'
      @config.should be_valid
      @config.errors['exchange'].should be_empty
    end

    it "requires worker queues to be queue names" do
      @config.workers['default'].queues << 'unknown_queue'
      @config.should_not be_valid
      @config.errors['queues'].should_not be_empty
    end

    it "allows the worker queue 'all'" do
      @config.workers['default'].queues = ['all']
      @config.should be_valid
      @config.errors['queues'].should be_empty
    end

    it "requires funnel queues to be queue names" do
      @config.funnels['default'].queues << 'unknown_queue'
      @config.should_not be_valid
      @config.errors['queues'].should_not be_empty
    end

    it "allows the funnel queue 'all'" do
      @config.funnels['default'].queues = ['all']
      @config.should be_valid
      @config.errors['queues'].should be_empty
    end

    it "requires job exchanges to be exchange names" do
      @config.jobs['default'].exchanges << 'unknown_exchange'
      @config.should_not be_valid
      @config.errors['exchanges'].should_not be_empty
    end

    it "allows the job exchange 'all'" do
      @config.jobs['default'].exchanges = ['all']
      @config.should be_valid
      @config.errors['exchanges'].should be_empty
    end
  end

  describe "#configure!" do
    before :each do
      @config.stub!(:clear!)
      @config.stub!(:from_file)
      @config.stub!(:from_command_line)
      @config.stub!(:expand!)
      @config.stub!(:validate!)
    end

    it "reads, expands and validates config from an env, file and options" do
      @config.should_receive(:clear!).ordered
      @config.should_receive(:from_file).with(
        'my/jobbable.yml', 'development').ordered
      @config.should_receive(:from_command_line).with(%w{--port 1234}).ordered
      @config.should_receive(:expand!).ordered
      @config.should_receive(:validate!).ordered

      @config.configure!('development', 'my/jobbable.yml',
        %w{--port 1234}, '/right/here')

      @config.working_directory.should == '/right/here'
    end

    it "only reads the config file if it is not nil" do
      @config.should_not_receive(:from_file)
      @config.configure!('development', nil, %w{--port 1234}, '/right/here')
    end

    it "sets the debug flag before reading the file if in the options" do
      @config.should_receive(:debug=).with(true).ordered
      @config.should_receive(:from_file).with(
        'my/jobbable.yml', 'development').ordered

      @config.configure!('development', 'my/jobbable.yml',
        %w{--debug}, '/right/here')
    end
  end

  describe "#full_require_paths" do
    before :each do
      @config.require_paths = %w{jobs app/jobs lib/jobs}
    end

    it "is #require_paths, expanded relative to #working_directory" do
      @config.working_directory = '/here'
      @config.full_require_paths.should == \
        %w{/here/jobs /here/app/jobs /here/lib/jobs}
    end

    it "is #require_paths if #working_directory is not set" do
      @config.working_directory = nil
      @config.full_require_paths.should == @config.require_paths
    end
  end

  describe "#full_log_file" do
    before :each do
      @config.log_file = 'log/jobbable.log'
    end

    it "is #log_file, expanded relative to #working_directory" do
      @config.working_directory = '/here'
      @config.full_log_file.should == '/here/log/jobbable.log'
    end

    it "is #log_file if #working_directory is not set" do
      @config.working_directory = nil
      @config.full_log_file.should == @config.log_file
    end

    it "is #log_file when syslog, none or -" do
      @config.working_directory = '/here'

      @config.log_file = 'none'
      @config.full_log_file.should == 'none'

      @config.log_file = 'syslog'
      @config.full_log_file.should == 'syslog'

      @config.log_file = '-'
      @config.full_log_file.should == '-'
    end
  end

  describe "#pid_directory" do
    it "is #pids, expanded relative to #working_directory" do
      @config.working_directory = '/here'
      @config.pids = 'tmp/pids'
      @config.pid_directory.should == '/here/tmp/pids'
    end

    it "is #pids if #working_directory is not set" do
      @config.working_directory = nil
      @config.pid_directory.should == @config.pids
    end
  end

  describe "#clear!" do
    it "resets general options to their defaults" do
      @config.pids = 'some/odd/pid/dir'
      @config.clear!
      @config.pids.should == Jobbable::Configuration.defaults['pids']
    end

    it "drops all job configs except default" do
      @config.jobs['some_job'].timeout = 30
      @config.clear!
      @config.jobs.should_not have_key 'some_job'
    end

    it "resets the default job config to its default" do
      @config.jobs['default'].timeout = 30
      @config.clear!
      @config.jobs['default'].timeout.should == \
        Jobbable::Config::JobConfig.defaults['timeout']
    end

    it "drops all exchange configs except default" do
      @config.exchanges['some_exchange'].type = 'fanout'
      @config.clear!
      @config.exchanges.should_not have_key 'some_exchange'
    end

    it "resets the default exchange config to its default" do
      @config.exchanges['default'].type = 'fanout'
      @config.clear!
      @config.exchanges['default'].type.should == \
        Jobbable::Config::ExchangeConfig.defaults['type']
    end

    it "drops all queue configs except default" do
      @config.queues['some_queue'].type = 'pop_star'
      @config.clear!
      @config.queues.should_not have_key 'some_queue'
    end

    it "resets the default queue config to its default" do
      @config.queues['default'].type = 'pop_star'
      @config.clear!
      @config.queues['default'].type.should == \
        Jobbable::Config::QueueConfig.defaults['type']
    end

    it "drops all worker configs except default" do
      @config.workers['some_worker'].count = 3
      @config.clear!
      @config.workers.should_not have_key 'some_worker'
    end

    it "resets the default worker config to its default" do
      @config.workers['default'].count = 3
      @config.clear!
      @config.workers['default'].count.should == \
        Jobbable::Config::WorkerConfig.defaults['count']
    end

    it "drops all funnel configs except default" do
      @config.funnels['some_funnel'].count = 3
      @config.clear!
      @config.funnels.should_not have_key 'some_funnel'
    end

    it "resets the default funnel config to its default" do
      @config.funnels['default'].count = 3
      @config.clear!
      @config.funnels['default'].count.should == \
        Jobbable::Config::FunnelConfig.defaults['count']
    end
  end

  describe "#to_hash" do
    it "is a pure-hash copy" do
      @config.to_hash.should be_instance_of Hash
    end

    Jobbable::Configuration::CHILDREN.each do |child|
      it "converts contained #{child[:singular]} as well" do
        @config.to_hash[child[:plural]].should \
          be_all { |name, child| child.instance_of? Hash }
      end
    end
  end
end
