require 'spec_helper'
require 'jobbable/config_mixin'

describe Jobbable::ConfigMixin do
  before :each do
    Dir.stub!(:pwd).and_return("/here")

    @configured = Object.new.tap do |configured|
      class <<configured
        include Jobbable::ConfigMixin
      end
    end

    @config = Jobbable::Configuration.new
    @config.stub!(:configure!)

    @configured.config = @config
  end

  describe "#working_directory" do
    it "is an attribute" do
      @configured.working_directory = :the_directory
      @configured.working_directory.should == :the_directory
    end
  end

  describe "#environment" do
    it "lazy-initializes to 'development'" do
      @configured.environment = nil
      @configured.environment.should == "development"
    end
  end

  describe "#config_file" do
    before :each do
      @configured.working_directory = '/working/dir'
      @configured.config_file = nil
    end

    it "is config/jobbable.yml if it exists" do
      File.should_receive(:exist?).with(
        '/working/dir/config/jobbable.yml').and_return(true)
      @configured.config_file.should == '/working/dir/config/jobbable.yml'
    end

    it "is ./jobbable.yml if it exists and config/jobbable.yml doesn't" do
      File.should_receive(:exist?).with(
        '/working/dir/config/jobbable.yml').and_return(false)

      File.should_receive(:exist?).with(
        '/working/dir/jobbable.yml').and_return(true)

      @configured.config_file.should == '/working/dir/jobbable.yml'
    end

    it "is nil if none of the default locations exist" do
      File.stub!(:exist?).with(
        '/working/dir/config/jobbable.yml').and_return(false)

      File.should_receive(:exist?).with(
        '/working/dir/jobbable.yml').and_return(false)

      @configured.config_file.should be_nil
    end
  end

  describe "#argv" do
    it "lazy-initializes to an empty array" do
      @configured.argv = nil
      @configured.argv.should == []
    end
  end

  describe "#command_argv" do
    it "lazy-initializes to an empty array" do
      @configured.command_argv = nil
      @configured.command_argv.should == []
    end
  end

  describe "#configure!" do
    it "curries and saves Configuration#configure! in #command_argv" do
      @configured.should_receive(:environment).
        at_least(:once).and_return(:the_env)

      @configured.should_receive(:config_file).
        at_least(:once).and_return(:the_file)

      @configured.should_receive(:argv).
        at_least(:once).and_return([:the_argv])

      @configured.should_receive(:working_directory).
        at_least(:once).and_return(:the_directory)

      @config.should_receive(:configure!).with(:the_env, :the_file,
        [:the_argv], :the_directory).and_return(:the_command_argv)

      @configured.should_receive(:command_argv=).and_return(:the_command_argv)

      @configured.configure!
    end
  end

  describe "#config" do
    before :each do
      @configured.config = nil
      Jobbable::Configuration.stub!(:new).and_return(@config)
    end

    it "lazy-initializes a config" do
      Jobbable::Configuration.should_receive(:new).and_return(@config)
      @configured.config.should == @config
    end

    it "configures! the new instance" do
      @config.should_receive(:configure!).with(
        @configured.environment,
        @configured.config_file,
        @configured.argv,
        @configured.working_directory
      ).and_return(:command_argv)

      @configured.config

      @configured.command_argv.should == :command_argv
    end
  end

end
