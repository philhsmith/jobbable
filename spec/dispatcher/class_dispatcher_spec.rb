require 'spec_helper'
require 'jobbable/dispatcher/class_dispatcher'

describe Jobbable::Dispatcher::ClassDispatcher do
  before :each do
    @flight = mock("job in flight")

    @job_type = mock("The job class")
    @job_type.stub!(:new).and_return(@job = mock("AJob instance"))
    @job.stub!(:flight=)
    @job.stub!(:perform)

    @dispatcher = Jobbable::Dispatcher::ClassDispatcher.new
    @dispatcher.stub!(:job_type).with(@flight).and_return(@job_type)
  end

  it "is an abstract dispatcher" do
    @dispatcher.should be_a Jobbable::Dispatcher::Abstract
  end

  describe "#dispatch" do
    it "takes a flight and instantiates its job_type as a class" do
      @job_type.should_receive(:new).and_return(@job)
      @dispatcher.dispatch(@flight)
    end

    it "sets the flight on the job and calls #perform on it" do
      @job.should_receive(:flight=).with(@flight)
      @job.should_receive(:perform)
      @dispatcher.dispatch(@flight)
    end
  end
end
