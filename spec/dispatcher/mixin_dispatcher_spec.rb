require 'spec_helper'
require 'jobbable/dispatcher/mixin_dispatcher'

describe Jobbable::Dispatcher::MixinDispatcher do
  module JobType
  end

  before :each do
    @flight = mock("job in flight")
    @flight.stub!(:perform)

    @dispatcher = Jobbable::Dispatcher::MixinDispatcher.new
    @dispatcher.stub!(:job_type).with(@flight).and_return(JobType)
  end

  it "is an abstract dispatcher" do
    @dispatcher.should be_a Jobbable::Dispatcher::Abstract
  end

  describe "#dispatch" do
    it "mixes the job type into the flight" do
      @dispatcher.dispatch(@flight)
      @flight.should be_a JobType
    end

    it "calls flight.perform" do
      @flight.should_receive(:perform)
      @dispatcher.dispatch(@flight)
    end
  end
end
