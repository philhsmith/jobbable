require 'spec_helper'
require 'jobbable/dispatcher'

describe Jobbable::Dispatcher::Abstract do
  before :each do
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end
    Jobbable.stub!(:logger).and_return(@logger)

    class <<(@dispatcher = Object.new)
      include Jobbable::Dispatcher::Abstract
    end
  end

  describe "#constantize" do
    before :each do
      @dispatcher.stub!(:require)
      Object.stub!(:const_get).and_return(@job_type = mock("AJob"))
    end

    it "constantizes and returns the given name" do
      Object.should_receive(:const_get).with("AJob").and_return(@job_type)
      @dispatcher.constantize('a_job').should == @job_type
    end

    it "requires the underscored name and retries if the job wasn't found" do
      Object.should_receive(:const_get).with("AJob") do
        unless @called_before
          @called_before = true
          raise NameError.new("not found!") # the first call fails
        else
          @job_type                         # the rest succeed
        end
      end

      @dispatcher.should_receive(:require).with('a_job')

      @dispatcher.constantize('a_job').should == @job_type
    end
  end

  describe "#job_type" do
    it "is the constantized job name of the given flight" do
      @dispatcher.should_receive(:constantize).with('a_job').
        and_return(:the_job_type)

      flight = mock("the flight")
      flight.stub!(:metadata).and_return(:job => 'a_job')

      @dispatcher.job_type(flight).should == :the_job_type
    end
  end
end
