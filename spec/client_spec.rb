require 'spec_helper'
require 'jobbable/client'

describe Jobbable::Client do
  before :each do
    @config = Jobbable::Configuration.new

    @amqp = mock("ConfiguredAmqp")
    @amqp.stub!(:publish)
    @amqp.stub!(:bind_queues)

    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end
    Jobbable.stub!(:logger).and_return(@logger)

    # Yielding causes the block to run
    @amqp.stub!(:with_connection).and_yield

    @client = Jobbable::Client.new
    @client.config = @config
    @client.amqp = @amqp

    # Neuter some system calls
    Socket.stub!(:gethostname)
  end

  it "mixes in Config" do
    @client.should be_a Jobbable::ConfigMixin
  end

  it "mixes in Publisher" do
    @client.should be_a Jobbable::Amqp::PublisherMixin
  end

  describe "#logger" do
    it "is the global logger" do
      Jobbable.should_receive(:logger).and_return(@logger)
      @client.logger.should == @logger
    end
  end

end
