require 'spec_helper'
require 'jobbable/monkey/amqp/get_empty'

describe MQ::Header do
  before :each do
    @mq = mock("mq")
    @raw_header = mock("raw header")

    @empty_header = MQ::Header.new @mq, nil
    @normal_header = MQ::Header.new @mq, @raw_header
  end

  describe "#empty?" do
    it "holds if the header was constructed without a raw header" do
      @empty_header.should be_empty
      @normal_header.should_not be_empty
    end
  end
end
