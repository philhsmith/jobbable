require 'spec_helper'
require 'jobbable/monkey/core/exception'

describe Exception do
  describe "#raising_flight" do
    it "is an attribute" do
      exception = Exception.new("explosions!")
      exception.raising_flight = :a_flight
      exception.raising_flight.should == :a_flight
    end
  end
end
