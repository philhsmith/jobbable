require 'spec_helper'
require 'jobbable/monkey/core/proc'

module Explodey
  def explode
    raise StandardError.new("explode!")
  end

  def exploding_block
    lambda do
      explode
    end
  end
end

class Exploder
  attr_writer :flight

  include Explodey
end

describe Proc do
  before :each do
    @flight = mock("flight")
    @flight.stub!(:is_a?).with(Jobbable::Flight).and_return(true)
  end

  [:call, :[]].each do |method|
    describe "##{method}" do
      it "lazily captures the bound self if it is a Flight" do
        @flight.extend Explodey

        begin
          @flight.exploding_block.send method
        rescue StandardError => exception
          exception.message.should == "explode!"
          exception.raising_flight.should == @flight
        end
      end

      it "lazily captures @flight on the bound self if it has on" do
        exploder = Exploder.new
        exploder.flight = @flight

        begin
          exploder.exploding_block.send method
        rescue StandardError => exception
          exception.message.should == "explode!"
          exception.raising_flight.should == @flight
        end
      end
    end
  end
end
