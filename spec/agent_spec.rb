require 'spec_helper'
require 'jobbable/agent'

describe Jobbable::Agent do

  before :each do
    @config = Jobbable::Configuration.new.tap do |c|
      c.working_directory = '/here'
      c.pids = 'tmp/pids'
    end

    @agent_config = Jobbable::Config::AgentConfig.new

    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    Jobbable.stub!(:logger).and_return(@logger)

    @agent = Jobbable::Agent.new(@config, @agent_config)

    # Ignore some AMQP and EM
    EM.stub!(:run)
    EM.stub!(:stop_event_loop)
    EM.stub!(:add_timer)
    EM.stub!(:error_handler)
  end

  it "has a configuration" do
    @agent.config.should == @config
  end

  it "has a personal agent configuration" do
    @agent.agent_config.should == @agent_config
  end

  it "has a logger" do
    @agent.logger.should == @logger
  end

  describe ".subclasses" do
    it "is all loaded subclasses by underscored names" do
      class SeanConnery < Jobbable::Agent; end
      class RogerMoore < Jobbable::Agent; end

      Jobbable::Agent.subclasses['sean_connery'].should == SeanConnery
      Jobbable::Agent.subclasses['roger_moore'].should == RogerMoore
    end

    it "drops leading module names" do
      module Technically
        class GeorgeLazenby < Jobbable::Agent; end
      end

      Jobbable::Agent.subclasses['george_lazenby'].should == \
        Technically::GeorgeLazenby
    end

    after :all do
      Jobbable::Agent.subclasses.delete 'sean_connery'
      Jobbable::Agent.subclasses.delete 'roger_moore'
      Jobbable::Agent.subclasses.delete 'george_lazenby'
    end
  end

  describe "#beat" do
    it "is an accessor" do
      @agent.beat = 17
      @agent.beat.should == 17
    end

    it "lazy-initializes to zero" do
      @agent.beat = nil
      @agent.beat.should == 0

      @agent.beat += 1
      @agent.beat.should == 1
    end
  end

  describe "#heartbeat" do
    before :each do
      @agent.stub!(:start_heartbeat_timer)
    end

    it "bumps the beat counter" do
      @agent.beat = 7
      @agent.heartbeat
      @agent.beat.should == 8
    end

    it "calls #on_heartbeat if present" do
      @agent.should_receive(:respond_to?).with(:on_heartbeat).and_return(true)
      @agent.should_receive(:on_heartbeat)
      @agent.heartbeat
    end

    it "clears the old heartbeat timer" do
      @agent.heartbeat_timer = :the_old_timer
      @agent.heartbeat
      @agent.heartbeat_timer.should be_nil
    end

    it "starts the next heartbeat" do
      @agent.should_receive(:start_heartbeat_timer)
      @agent.heartbeat
    end
  end

  describe "#start_lifespan_timer" do
    it "lazily starts a timer firing after the configured seconds" do
      EM.should_receive(:add_timer).with(
        @agent.agent_config.lifespan).and_return(:timer)
      @agent.start_lifespan_timer
      @agent.lifespan_timer.should == :timer
    end

    it "calls #shutdown on expiration" do
      @agent.should_receive(:shutdown)

      # Yielding causes the block to run
      EM.should_receive(:add_timer).and_yield

      @agent.start_lifespan_timer
    end

    it "does nothing if the configured lifespan is 0" do
      @agent.agent_config.lifespan = 0
      EM.should_not_receive(:add_timer)
      @agent.start_lifespan_timer
    end
  end

  describe "#start_heartbeat_timer" do
    it "lazily starts a timer firing after the configured seconds" do
      EM.should_receive(:add_timer).with(
        @agent.agent_config.heartbeat).and_return(:timer)
      @agent.start_heartbeat_timer
      @agent.heartbeat_timer.should == :timer
    end

    it "calls #heartbeat on expiration" do
      @agent.should_receive(:heartbeat)

      # Yielding causes the block to run
      EM.should_receive(:add_timer).and_yield

      @agent.start_heartbeat_timer
    end
  end

  describe "#main" do
    before :each do
      @agent.stub!(:start)
      @agent.stub!(:respond_to?).with(:on_start).and_return(true)
      @agent.stub!(:on_start)
    end

    it "sets the eventmachine thread pool size to config.threads" do
      @agent.agent_config.threads = 1337 # some non-default value
      @agent.main
      EM.threadpool_size.should == @agent.agent_config.threads
    end

    it "sets the eventmachine timer maximum to config.timers" do
      @agent.agent_config.timers = 1337 # some non-default value
      @agent.main
      EM.get_max_timers.should == @agent.agent_config.timers
    end

    it "logs eventmachine errors" do
      an_exception = StandardError.new("explosions")

      # Yielding causes the block to run
      EM.should_receive(:error_handler).and_yield(an_exception)
      @logger.should_receive(:error).with(an_exception)

      @agent.main
    end

    it "starts eventmachine" do
      EM.should_receive(:run)
      @agent.main
    end

    describe "in EM.run" do
      before :each do
        @connection = mock "amqp connection"

        # Yielding causes the block to run
        EM.stub!(:run).and_yield
      end

      it "calls #on_start if present" do
        @agent.should_receive(:respond_to?).with(:on_start).and_return(true)
        @agent.should_receive(:on_start)
        @agent.main
      end

      it "starts the first heartbeat" do
        @agent.should_receive(:start_heartbeat_timer)
        @agent.main
      end

      it "starts the lifespan timer" do
        @agent.should_receive(:start_lifespan_timer)
        @agent.main
      end
    end
  end

  describe "#shutdown" do
    it "stops eventmachine" do
      # Yielding causes the block to run
      EM.should_receive(:stop_event_loop)
      @agent.shutdown
    end
  end

  describe "#guard" do
    before :each do
      @client_code = mock("client code")
      @client_code.stub!(:called)
    end

    it "takes and calls a block, returning its value" do
      @client_code.should_receive(:called).and_return(:value)
      @agent.guard { @client_code.called }.should == :value
    end

    [StandardError, ScriptError].each do |exception_type|
      it "traps #{exception_type}s and logs them, returning nil" do
        exception = exception_type.new("explosions!")
        @client_code.should_receive(:called).and_raise(exception)
        @logger.should_receive(:error).with(exception)
        @agent.guard { @client_code.called }
      end
    end

    it "takes an optional message to log before the exception" do
      @client_code.stub!(:called).and_raise(StandardError.new("explosions!"))
      @logger.should_receive(:error).with("message!")
      @agent.guard("message!") { @client_code.called }
    end
  end

end
