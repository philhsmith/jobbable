require 'spec_helper'
require 'jobbable/facade'

describe Jobbable do
  it "has a parent singleton" do
    Jobbable.parent = :a_parent
    Jobbable.parent.should == :a_parent
  end

  it "has a worker singleton" do
    Jobbable.worker = :a_worker
    Jobbable.worker.should == :a_worker
  end

  after :each do
    Jobbable.parent = nil
    Jobbable.worker = nil
  end

  describe "#client" do
    it "can be set" do
      Jobbable.client = :a_client
      Jobbable.client.should == :a_client
    end

    it "is the #worker by default" do
      Jobbable.worker = mock("the worker")
      Jobbable.client = nil
      Jobbable.client.should == Jobbable.worker
    end
  end

  describe "#logger" do
    it "lazy-initializes to the null logger" do
      Jobbable.logger = nil
      Jobbable.logger.should == Jobbable::NULL_LOGGER
    end
  end

  describe "#logger=" do
    it "closes the previous logger if present" do
      old_logger = mock("old logger")
      old_logger.should_receive(:close)
      Jobbable.instance_variable_set("@logger", old_logger)

      Jobbable.logger = :new_logger
    end

    it "doesn't try to close nil" do
      Jobbable.instance_variable_set("@logger", nil)
      Jobbable.logger = :new_logger
    end

    it "assigns the new one" do
      Jobbable.logger = :new_logger
      Jobbable.logger.should == :new_logger
    end
  end

  describe "delegation" do
    before :each do
      Jobbable.parent = mock("the parent")
      Jobbable.client = mock("the client")
    end

    it "sends #config to #parent" do
      Jobbable.parent.should_receive(:config).and_return(:the_config)
      Jobbable.config.should == :the_config
    end

    it "sends #environment to #parent" do
      Jobbable.parent.should_receive(:environment).and_return("stagduction")
      Jobbable.environment.should == "stagduction"
    end

    it "sends #publish to #client" do
      Jobbable.client.should_receive(:publish).with(:job => :a_job)
      Jobbable.publish(:job => :a_job)
    end

    it "sends #queue_status to #client" do
      client_code = mock("client code")
      client_code.should_receive(:called)

      # Yielding causes the block to run
      Jobbable.client.should_receive(:queue_status).with("queue", {}).
        and_yield.and_return(:status)

      Jobbable.queue_status("queue") { client_code.called }.should == :status
    end
  end
end
