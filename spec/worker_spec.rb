require 'spec_helper'
require 'jobbable/worker'

describe Jobbable::Worker do

  before :each do
    @config = Jobbable::Configuration.new.tap do |c|
      c.working_directory = '/here'
      c.pids = 'tmp/pids'
      c.require_paths = %w{jobs app/jobs lib/jobs}
      c.workers['worker'].count = 2
    end

    @worker_config = @config.workers['worker']
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    Jobbable.stub!(:logger).and_return(@logger)

    @amqp = mock("ConfiguredAmqp").tap do |amqp|
      amqp.stub! :subscribe
      amqp.stub! :stop

      # Yielding causes the block to run
      amqp.stub!(:with_connection).and_yield
    end

    @dispatcher = mock("dispatcher").tap do |dispatcher|
      dispatcher.stub!(:dispatch)
    end

    @worker = Jobbable::Worker.new(@config, @worker_config).tap do |worker|
      worker.stub!(:require)
      worker.amqp = @amqp
      worker.dispatcher = @dispatcher
    end

    # Ignore some AMQP and EM
    EM.stub!(:run)
    EM.stub!(:stop_event_loop)
    EM.stub!(:error_handler)
  end

  it "is an agent" do
    @worker.should be_a Jobbable::Agent
  end

  it "mixes in subscriber" do
    @worker.should be_a Jobbable::Amqp::SubscriberMixin
  end

  it "mixes in publisher" do
    @worker.should be_a Jobbable::Amqp::PublisherMixin
  end

  describe "#dispatcher" do
    it "lazy-initializes to a MixinDispatcher" do
      @worker.dispatcher = nil
      @worker.dispatcher.should be_a Jobbable::Dispatcher::MixinDispatcher
    end
  end

  describe "#append_require_paths" do
    it "adds the configured paths to $:" do
      @worker.append_require_paths
      @config.full_require_paths.each { |path| $:.should include path }
    end
  end

  describe "#autorequire" do
    it "requires the file named in config.autorequire" do
      @config.autorequire = "job_environment"
      @worker.should_receive(:require).with("job_environment")
      @worker.autorequire
    end

    it "does nothing if config.autorequire is nil" do
      @config.autorequire = nil
      @worker.should_not_receive(:require)
      @worker.autorequire
    end
  end

  describe "#on_heartbeat" do
    it "takes work from pop queues every heartbeat" do
      @worker.stub!(:pop_queues).and_return(%w{a_pop_queue another_pop_queue})

      @worker.should_receive(:pop).with('a_pop_queue')
      @worker.should_receive(:pop).with('another_pop_queue')

      @worker.on_heartbeat
    end

    it "logs the current counters every heartbeat" do
      @logger.should_receive(:info).with(@worker.counters)
      @worker.on_heartbeat
    end
  end

  describe "#shutdown" do
    it "stops amqp" do
      # Yielding causes the block to run
      @worker.should_receive(:stop).and_yield
      @worker.shutdown
    end
  end

  describe "#on_start" do
    before :each do
      @amqp.stub!(:start)
    end

    it "sets the Jobbable.worker singleton to itself" do
      @worker.on_start
      Jobbable.worker.should == @worker
    end

    it "sends eventmachine errors to #rescue" do
      an_exception = StandardError.new("explosions")

      # Yielding causes the block to run
      EM.should_receive(:error_handler).and_yield(an_exception)
      @worker.should_receive(:rescue).with(an_exception)

      @worker.on_start
    end

    it "creates a new amqp connection" do
      @amqp.should_receive(:start)
      @worker.on_start
    end

    describe "after the connection is established" do
      before :each do
        # Yielding causes the block to run
        @amqp.stub!(:start).and_yield
      end

      it "appends the configured include paths" do
        @worker.should_receive(:append_require_paths)
        @worker.on_start
      end

      it "requires the autorequired file" do
        @worker.should_receive(:autorequire)
        @worker.on_start
      end

      %w{crash abort fail}.each do |special_exchange|
        it "ensures #{special_exchange} is bound if present" do
          @worker.config.exchanges[special_exchange]
          @amqp.should_receive(:ensure_bound).with(special_exchange)
          @worker.on_start
        end
      end

      it "subscribes to queues, calling perform when messages arrive" do
        @worker_config.queues = %w{foo bar}

        # Yielding causes the block to run
        @worker.should_receive(:subscribe).with(
          %w{foo bar}).and_yield(:message)

        @worker.should_receive(:perform).with(:message)

        @worker.on_start
      end
    end
  end

  describe "#perform" do
    before :each do
      @flight = mock("flight")
      @flight.stub!(:extend)
      @flight.stub!(:start_timeout)

      @exception = StandardError.new("your head asplode")
    end

    it "mixes Flight into the passed message" do
      @flight.should_receive(:extend).with(Jobbable::Flight)
      @worker.perform @flight
    end

    it "starts the flight's timeout" do
      @flight.should_receive(:start_timeout)
      @worker.perform @flight
    end

    it "notifies observers" do
      @worker.should_receive(:notify).with(@flight, :perform)
      @worker.perform @flight
    end

    it "guards against exceptions with #rescue" do
      # Yielding causes the block to run
      @worker.should_receive(:rescue).with(@flight).and_yield
      @worker.perform @flight
    end

    it "passes the flight to the dispatcher" do
      @worker.dispatcher.should_receive(:dispatch).with(@flight)
      @worker.perform @flight
    end
  end

  describe "#exception_handlers" do
    it "lazy-initializes to an empty array" do
      @worker.exception_handlers = nil
      @worker.exception_handlers.should == []
    end
  end

  describe "#exception_handlers_for" do
    before :each do
      @flight = mock("flight")
      @flight.stub!(:respond_to?).with(:exception_handlers).and_return(true)

      @worker_handlers = [:worker, :exception_handlers]
      @flight_handlers = [:flight_specific, :handlers]
      @worker.exception_handlers = @worker_handlers
    end

    it "takes a flight and returns exception handlers for it" do
      @flight.should_receive(:exception_handlers).and_return(@flight_handlers)
      @worker.exception_handlers_for(@flight).should == \
        @flight_handlers + @worker_handlers
    end

    it "is the worker's unless the flight responds to #exception_handlers" do
      @flight.should_receive(:respond_to?).with(
        :exception_handlers).and_return(false)

      @worker.exception_handlers_for(@flight).should == @worker_handlers
    end

    it "is the worker's if the flight#exception_handlers explodes" do
      @flight.should_receive(
        :exception_handlers).and_raise(StandardError.new("explosions!"))

      @worker.exception_handlers_for(@flight).should == @worker_handlers
    end

    it "is the worker's if the flight#exception_handlers is not an Array" do
      @flight.should_receive(
        :exception_handlers).and_return("client code issues!")

      @worker.exception_handlers_for(@flight).should == @worker_handlers
    end
  end

  describe "#rescue" do
    before :each do
      @handlers = [mock("one handler"), mock("another handler")]
      @handlers.each { |handler| handler.stub!(:rescue).and_return(false) }
      @flight = mock("the flight")
      @exception = StandardError.new("explosions!")

      @worker.stub!(:exception_handlers_for).and_return(@handlers)
    end

    it "calls #rescue on the exception handlers for the job" do
      @worker.should_receive(
        :exception_handlers_for).with(@flight).and_return(@handlers)

      @handlers.first.should_receive(:rescue).with(
        @exception, @flight).and_return(true)

      @worker.rescue(@exception, @flight)
    end

    it "tries each exception handler in turn until one returns true" do
      @handlers.first.stub!(:rescue).and_return(false)

      @handlers[1].should_receive(:rescue).with(
        @exception, @flight).and_return(true)

      @worker.rescue(@exception, @flight)
    end

    it "crashing the flight if none return true" do
      @handlers.each { |handler| handler.stub!(:rescue).and_return(false) }
      @flight.should_receive(:crash).with(@exception)
      @worker.rescue(@exception, @flight)
    end

    it "can omit the flight, taking it from the exception" do
      @exception.raising_flight = @flight
      @flight.should_receive(:crash).with(@exception)
      @worker.rescue(@exception)
    end

    it "logs exceptions that seem to have no associated flight" do
      @exception.raising_flight = nil
      @logger.should_receive(:error).with(@exception)
      @worker.rescue(@exception)
    end

    it "returns the exception" do
      @worker.rescue(@exception).should == @exception
    end

    it "can run a block within a guard for StandardErrors and ScriptErrors" do
      client_code = mock("client code")
      client_code.should_receive(:called).and_raise(@exception)

      # In the case that no associated flight is found..
      @logger.should_receive(:error).with(@exception)

      @worker.rescue { client_code.called }
    end

    it "whines if called without arguments or block" do
      lambda { @worker.rescue }.should raise_error(ArgumentError)
    end
  end

  describe "#observers" do
    it "lazy-initializes to an empty array" do
      @worker.observers = nil
      @worker.observers.should == []
    end
  end

  describe "#observers_for" do
    before :each do
      @flight = mock("flight")
      @flight.stub!(:respond_to?).with(:observers).and_return(true)
      @worker_observers = [:worker, :observers]
      @flight_observers = [:flight_specific, :observers]
      @worker.observers = @worker_observers
    end

    it "takes a flight and returns observers for it" do
      @flight.should_receive(:observers).and_return(@flight_observers)
      @worker.observers_for(@flight).should == \
        @flight_observers + @worker_observers
    end

    it "is the worker's if the flight does not respond to #observers" do
      @flight.should_receive(:respond_to?).with(:observers).and_return(false)
      @worker.observers_for(@flight).should == @worker_observers
    end

    it "is the worker's if the flight#observers explodes" do
      @flight.should_receive(
        :observers).and_raise(StandardError.new("explosions!"))
      @worker.observers_for(@flight).should == @worker_observers
    end

    it "is the worker's if the flight#observers is not an Array" do
      @flight.should_receive(:observers).and_return("client code issues!")
      @worker.observers_for(@flight).should == @worker_observers
    end
  end

  describe "#notify" do
    before :each do
      @observers = [mock("one observer"), mock("another observer")]
      @observers.each { |observer| observer.stub!(:on_complete) }
      @flight = mock("the flight")

      @worker.stub!(:observers_for).and_return(@observers)
    end

    it "notifies observers who respond to the callback" do
      @observers.first.should_receive(:respond_to?).with(
        :on_complete).and_return(false)

      @observers.last.should_receive(:respond_to?).with(
        :on_complete).and_return(true)

      @observers.last.should_receive(:on_complete).with(@flight)

      @worker.notify(@flight, :complete)
    end

    it "guards observer invocations" do
      # Yielding causes the block to run
      @worker.should_receive(:guard).twice.and_yield
      @worker.notify(@flight, :complete)
    end
  end

end
