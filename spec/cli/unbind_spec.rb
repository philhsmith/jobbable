require 'spec_helper'
require 'jobbable/cli/unbind'

describe Jobbable::Cli::Unbind do

  before :each do
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    @command_line = mock("command line")
    @config = Jobbable::Configuration.new
    @command_line.stub!(:config).and_return(@config)
    @command_line.stub!(:logger).and_return(@logger)

    @unbind = Jobbable::Cli::Unbind.new(@command_line)
  end

  it "is a command" do
    @unbind.should be_a Jobbable::Cli::Command
  end

  it "mixes in ConfiguredAmqp" do
    @unbind.should be_a Jobbable::Amqp::ConfiguredAmqpMixin
  end

  describe "#perform" do
    before :each do
      @amqp = mock("amqp")
      Jobbable::Amqp::ConfiguredAmqp.stub!(:new).and_return(@amqp)
      @amqp.stub!(:start)
      @amqp.stub!(:unbind)
      @amqp.stub!(:stop)

      @command_line.stub!(:command_argv).and_return(
        %w{--exchange an_exchange --queue a_queue})
    end

    it "connects to AMQP and disconnects when it is done" do
      Jobbable::Amqp::ConfiguredAmqp.should_receive(:new).with(
        @config).and_return(@amqp)
      @amqp.should_receive(:start)
      @amqp.should_receive(:stop)
      @unbind.perform
    end

    it "takes --exchange and --queue" do
      @amqp.should_receive(:unbind).with(
        'an_exchange', 'a_queue', instance_of(Hash))
      @command_line.stub!(:command_argv).and_return \
        %w{--exchange an_exchange --queue a_queue}
      @unbind.perform
    end

    it "whines if --exchange is missing" do
      @command_line.stub!(:command_argv).and_return %w{--queue a_queue}
      lambda { @unbind.perform }.should raise_error
    end

    it "whines if --queue is missing" do
      @command_line.stub!(:command_argv).and_return %w{--exchange an_exchange}
      lambda { @unbind.perform }.should raise_error
    end

    it "can take --key" do
      @amqp.should_receive(:unbind).with(
        'an_exchange', 'a_queue', hash_including(:key => 'key'))
      @command_line.stub!(:command_argv).and_return \
        %w{--exchange an_exchange --queue a_queue --key key}
      @unbind.perform
    end
  end

end
