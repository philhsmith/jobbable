require 'spec_helper'
require 'jobbable/cli/publish'

describe Jobbable::Cli::Publish do

  before :each do
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    @command_line = mock("command line")
    @config = Jobbable::Configuration.new
    @command_line.stub!(:config).and_return(@config)
    @command_line.stub!(:logger).and_return(@logger)

    @publish = Jobbable::Cli::Publish.new(@command_line)

    # Some I/O and Jobbable::JSON
    # Yielding causes the block to run
    File.stub!(:open).and_yield(@io = mock("io"))
    @io.stub!(:each).and_yield(:json)
    STDIN.stub!(:each).and_yield(:json_from_stdin)

    Jobbable::JSON.stub!(:decode).and_return('foo' => 'bar')
  end

  it "is a command" do
    @publish.should be_a Jobbable::Cli::Command
  end

  it "mixes in Publisher" do
    @publish.should be_a Jobbable::Amqp::PublisherMixin
  end

  describe "#parse_command_argv" do
    it "can take --key" do
      @command_line.stub!(:command_argv).and_return %w{--job a_job --key key}
      @publish.parse_command_argv['key'].should == 'key'
    end

    it "can take --message" do
      @command_line.stub!(
        :command_argv).and_return %w{--job a_job --message some_json}
      @publish.parse_command_argv['message'].should == 'some_json'
    end

    it "can take --file" do
      @command_line.stub!(
        :command_argv).and_return %w{--job a_job --file some/message.json}
      @publish.parse_command_argv['file'].should == 'some/message.json'
    end

    it "can take --count" do
      @command_line.stub!(:command_argv).and_return %w{--job a_job --count 3}
      @publish.parse_command_argv['count'].should == 3
    end

    it "can take --template" do
      @command_line.stub!(
        :command_argv).and_return %w{--job a_job --template template}
      @publish.parse_command_argv['template'].should == 'template'
    end

    it "can take --state" do
      @command_line.stub!(
        :command_argv).and_return %w{--job a_job --state state}
      @publish.parse_command_argv['state'].should == 'state'
    end

    it "can take --attempt" do
      @command_line.stub!(
        :command_argv).and_return %w{--job a_job --attempt 3}
      @publish.parse_command_argv['attempt'].should == '3'
    end

    it "can take --priority" do
      @command_line.stub!(
        :command_argv).and_return %w{--job a_job --priority 1}
      @publish.parse_command_argv['priority'].should == '1'
    end

    it "can take --host" do
      @command_line.stub!(
        :command_argv).and_return %w{--job a_job --host example.com}
      @publish.parse_command_argv['host'].should == 'example.com'
    end

    it "can take --type" do
      @command_line.stub!(
        :command_argv).and_return %w{--job a_job --type text/plain}
      @publish.parse_command_argv['content_type'].should == 'text/plain'
    end

    it "can take --exchange" do
      @command_line.stub!(
        :command_argv).and_return %w{--job a_job --exchange an_exchange}
      @publish.parse_command_argv['exchanges'].should == %w{an_exchange}
    end

    it "can take --exchange many times" do
      @command_line.stub!(:command_argv).and_return \
        %w{--job a_job --exchange an_exchange --exchange another}
      @publish.parse_command_argv['exchanges'].should == \
        %w{an_exchange another}
    end

    it "can take --tag" do
      @command_line.stub!(:command_argv).and_return %w{--job a_job --tag foo}
      @publish.parse_command_argv['tags'].should == %w{foo}
    end

    it "can take --tag many times" do
      @command_line.stub!(
        :command_argv).and_return %w{--job a_job --tag foo --tag bar}
      @publish.parse_command_argv['tags'].should == %w{foo bar}
    end
  end

  describe "#perform" do
    before :each do
      @publish.stub!(:start)
      @publish.stub!(:publish)
      @publish.stub!(:stop)

      @publish.stub!(:parse_command_argv).and_return('some' => 'options')
    end

    it "publishes the named job" do
      @publish.should_receive(
        :parse_command_argv).and_return('some' => 'options')

      @publish.should_receive(:start).ordered
      @publish.should_receive(:publish).with('some' => 'options').ordered
      @publish.should_receive(:stop).ordered

      @publish.perform
    end

    it "whines if both --file and --count are passed" do
      @publish.should_receive(:parse_command_argv).and_return(
        'file' => 'some/message.json', 'count' => 3)
      lambda { @publish.perform }.should raise_error ArgumentError
    end

    it "can pull messages from a json file of option hashes" do
      @publish.should_receive(
        :parse_command_argv).and_return('file' => 'some.json')
      File.should_receive(:open).with("some.json", "r").and_yield(@io)
      Jobbable::JSON.should_receive(:decode).
        with(:json).and_return('a' => 'option')
      @publish.should_receive(:publish).with('a' => 'option')

      @publish.perform
    end

    it "can publish a message several times" do
      @publish.should_receive(
        :parse_command_argv).and_return('count' => 3, 'a' => 'option')
      @publish.should_receive(:publish).with('a' => 'option').exactly(3).times
      @publish.perform
    end

    it "can pull messages to publish (as json) from stdin" do
      @publish.should_receive(:parse_command_argv).and_return('file' => '-')
      Jobbable::JSON.should_receive(:decode).
        with(:json_from_stdin).and_return('some' => 'options')
      @publish.should_receive(:publish).with('some' => 'options')

      @publish.perform
    end

    it "merges in other command line options when publishing from json" do
      @publish.should_receive(:parse_command_argv).and_return('file' => '-',
        'exchange' => 'overriding_exchange')

      Jobbable::JSON.stub!(:decode).and_return('message' => "a_message",
        'exchange' => 'original_exchange')

      @publish.should_receive(:publish).with(hash_including(
        'message' => "a_message", 'exchange' => 'overriding_exchange'))

      @publish.perform
    end
  end

end
