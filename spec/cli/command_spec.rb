require 'spec_helper'
require 'jobbable/cli/command'

describe Jobbable::Cli::Command do

  before :each do
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    @command_line = mock("command line")
    @config = Jobbable::Configuration.new
    @command_line.stub!(:config).and_return(@config)
    @command_line.stub!(:logger).and_return(@logger)
    @command_line.stub!(:command_argv).and_return(@command_argv = [])

    @command = Jobbable::Cli::Command.new(@command_line)
    @command.stub!(:puts)

    FileUtils.stub!(:mv)
    Process.stub!(:kill)
  end

  it "mixes in logger" do
    @command.should be_a Jobbable::LoggerMixin
  end

  describe ".summary" do
    it "sets and returns its argument" do
      Jobbable::Cli::Command.summary(
        "run a jobbable command").should == "run a jobbable command"
      Jobbable::Cli::Command.summary(
        "another jobbable command").should == "another jobbable command"
    end
  end

  describe ".summaries" do
    it "is a hash of downcased command type names to their summaries" do
      class Foo < Jobbable::Cli::Command
        summary "foo's summary"
      end

      class Bar < Jobbable::Cli::Command
        summary "bar's summary"
      end

      Jobbable::Cli::Command.summaries['foo'].should == "foo's summary"
      Jobbable::Cli::Command.summaries['bar'].should == "bar's summary"
    end
  end

  describe ".verb" do
    it "is the downcased command type name" do
      class Foo < Jobbable::Cli::Command; end
      Foo.verb.should == 'foo'
    end
  end

  describe ".verbs" do
    it "is the sorted keys of .summaries" do
      Jobbable::Cli::Command.verbs.should == \
        Jobbable::Cli::Command.summaries.keys.sort
    end
  end

  describe "#command_line" do
    it "is the one it was constructed with" do
      @command.command_line.should == @command_line
    end
  end

  describe "#config" do
    it "delegates to #command_line" do
      @command_line.should_receive(:config).and_return(@config)
      @command.config.should == @config
    end
  end

  describe "#environment" do
    it "delegates to #command_line" do
      @command_line.should_receive(:environment).and_return(:the_environment)
      @command.environment.should == :the_environment
    end
  end

  describe "#config_file" do
    it "delegates to #command_line" do
      @command_line.should_receive(:config_file).and_return(:the_config_file)
      @command.config_file.should == :the_config_file
    end
  end

  describe "#argv" do
    it "delegates to #command_line" do
      @command_line.should_receive(:argv).and_return(:the_argv)
      @command.argv.should == :the_argv
    end
  end

  describe "#command_argv" do
    it "delegates to #command_line" do
      @command_line.should_receive(
        :command_argv).and_return(:the_command_argv)
      @command.command_argv.should == :the_command_argv
    end
  end

  describe "#logger" do
    it "delegates to #command_line" do
      @command_line.should_receive(:logger).and_return(:the_logger)
      @command.logger.should == :the_logger
    end
  end

  describe "#build_command_option_parser" do
    it "takes an OptionParser, but does nothing to it" do
      @command.build_command_option_parser(mock("option parser"))
    end

    it "is a symbol noting that the subclass did not override the method" do
      @command.build_command_option_parser(nil).should == :not_modified
    end
  end

  describe "#command_option_parser" do
    before :each do
      @parser = OptionParser.new
      OptionParser.stub!(:new).and_return(@parser)
      @parser.stub!(:banner=)
      @parser.stub!(:separator)
      @parser.stub!(:on_tail)
    end

    it "is an OptionParser" do
      @command.command_option_parser.should == @parser
    end

    it "has an #options hash for accumulating values" do
      @command.command_option_parser.options.should == {}
    end

    it "passes the new parser to #build_command_option_parser" do
      @command.should_receive(:build_command_option_parser).with(@parser)
      @command.command_option_parser
    end

    it "stubs a banner if #build_command_option_parser is :not_modified" do
      @command.should_receive(
        :build_command_option_parser).and_return(:not_modified)
      @parser.should_receive(:banner=).with("Usage: jobbable command " \
        "[environment] [path/to/jobbable.yml] [general options]")
      @command.command_option_parser
    end

    it "uses a fuller banner otherwise" do
      @command.should_receive(:build_command_option_parser).and_return(nil)
      @parser.should_receive(:banner=).with(
        "Usage: jobbable command [environment] [path/to/jobbable.yml] " \
        "[general options] -- [command options]")
      @command.command_option_parser
    end
  end

  describe "#parse_command_argv" do
    before :each do
      @parser = mock("parser")
      @command.stub!(:command_option_parser).and_return(@parser)
      @parser.stub!(:parse)
      @parser.stub!(:options)
    end

    it "parses command_argv with command_option_parser, returning options" do
      @command.should_receive(:command_option_parser).and_return(@parser)
      @parser.should_receive(:parse).with(@command_argv)
      @parser.should_receive(:options).and_return(:the_options)
      @command.parse_command_argv.should == :the_options
    end

    it "assumes --help if nothing was passed" do
      @command_argv.clear
      @command.parse_command_argv
      @command_argv.should include "--help"
    end
  end

  describe "#parent_pid" do
    it "reads the parent pid from file" do
      @command.should_receive(:read_pid).with("jobbable.pid").and_return(1000)
      @command.parent_pid.should == 1000
    end
  end

  describe "#old_pidfile?" do
    it "is a flag" do
      @command.old_pidfile = :flag
      @command.old_pidfile?.should == :flag
    end
  end

  describe "#write_parent_pid" do
    it "delegates to #write_pid" do
      Process.should_receive(:pid).and_return(1000)
      @command.should_receive(:write_pid).with("jobbable.pid", 1000)
      @command.write_parent_pid
    end
  end

  describe "#age_parent_pid" do
    before :each do
      @command.stub!(:read_pid).and_return(2000)
      @command.stub!(:rename_pid)
      @command.stub!(:remove_pid)
    end

    it "renames the current parent pid to the old one" do
      @command.should_receive(:rename_pid).with(
        "jobbable.pid", "jobbable-old.pid")
      @command.age_parent_pid
    end

    it "kills and removes the old pid before retrying if one is found" do
      @command.should_receive(:rename_pid).with("jobbable.pid",
        "jobbable-old.pid").and_raise(Jobbable::CantWritePidError)
      @command.should_receive(:read_pid).with(
        "jobbable-old.pid").and_return(2000)
      Process.should_receive(:kill).with("KILL", 2000)
      @command.should_receive(:remove_pid).with("jobbable-old.pid")
      @command.age_parent_pid
    end

    it "tolerates stale, old pid files" do
      @command.should_receive(:rename_pid).with("jobbable.pid",
        "jobbable-old.pid").and_raise(Jobbable::CantWritePidError)
      Process.should_receive(:kill).and_raise(Errno::ESRCH)
      @command.age_parent_pid
    end
  end

  describe "#remove_parent_pid" do
    it "delegates to #remove_pid" do
      @command.should_receive(:remove_pid).with("jobbable.pid")
      @command.remove_parent_pid
    end

    it "removes the old pid file if it is being used" do
      @command.should_receive(:old_pidfile?).and_return(true)
      @command.should_receive(:remove_pid).with("jobbable-old.pid")
      @command.remove_parent_pid
    end
  end

  describe "#check_pid_directory" do
    it "whines if the pids directory doesn't exist or isn't writable" do
      File.should_receive(:writable?).with(
        @config.pid_directory).and_return(false)
      lambda do
        @command.check_pid_directory
      end.should raise_error Jobbable::CantWritePidError
    end
  end

  describe "#check_pid_file" do
    it "whines if it would overwrite an existing pid file" do
      expanded = File.expand_path("my_pid_file.pid", @config.pid_directory)
      File.should_receive(:exist?).with(expanded).and_return(true)
      lambda do
        @command.check_pid_file("my_pid_file.pid")
      end.should raise_error(Jobbable::CantWritePidError)
    end

    it "returns the expanded filename otherwise" do
      expanded = File.expand_path("my_pid_file.pid", @config.pid_directory)
      File.should_receive(:exist?).with(expanded).and_return(false)
      @command.check_pid_file("my_pid_file.pid").should == expanded
    end
  end

  describe "#write_pid" do
    before :each do
      File.stub!(:writable?).and_return(true)
      File.stub!(:exist?).and_return(false)
    end

    it "writes the given pid to the given filename in the pids directory" do
      expanded = File.expand_path("my_pid_file.pid", @config.pid_directory)
      File.should_receive(:open).with(expanded, 'w').and_yield(
        mock('open pid file').tap do |file|
          file.should_receive(:<<).with(1000)
        end
      )

      @command.write_pid("my_pid_file.pid", 1000)
    end

    it "whines if it would overwrite an existing pid file" do
      expanded = File.expand_path("my_pid_file.pid", @config.pid_directory)
      File.should_receive(:exist?).with(expanded).and_return(true)
      lambda do
        @command.write_pid("my_pid_file.pid", 1000)
      end.should raise_error(Jobbable::CantWritePidError)
    end
  end

  describe "#rename_pid" do
    before :each do
      File.stub!(:writable?).and_return(true)
      File.stub!(:exist?).and_return(false)
    end

    it "renames the given pid in the pids directory" do
      original = File.expand_path("my_pid_file.pid", @config.pid_directory)
      renamed = File.expand_path("his_pid_file.pid", @config.pid_directory)
      FileUtils.should_receive(:mv).with(original, renamed)
      @command.rename_pid("my_pid_file.pid", "his_pid_file.pid")
    end

    it "whines if it would overwrite an existing pid file" do
      renamed = File.expand_path("his_pid_file.pid", @config.pid_directory)
      File.should_receive(:exist?).with(renamed).and_return(true)
      lambda do
        @command.rename_pid("my_pid_file.pid", "his_pid_file.pid")
      end.should raise_error(Jobbable::CantWritePidError)
    end
  end

  describe "#read_pid" do
    it "reads content of the given pid file in the pids directory" do
      File.should_receive(:read).with(File.expand_path(
        "my_pid_file.pid", @config.pid_directory)).and_return("1000")
      @command.read_pid("my_pid_file.pid").should == 1000
    end
  end

  describe "#remove_pid" do
    it "removes the given pid file from the pids directory" do
      File.should_receive(:delete).with(
        File.expand_path("my_pid_file.pid", @config.pid_directory))
      @command.remove_pid("my_pid_file.pid")
    end
  end

  describe "#daemonize!" do
    it "does some OS-level voodoo" do
      @command.should_receive(:fork).twice.and_return(nil)
      Process.should_receive(:setsid)
      Dir.should_receive(:chdir).with("/")
      File.should_receive(:umask).with(0000)
      STDIN.should_receive(:reopen).with('/dev/null')
      STDOUT.should_receive(:reopen).with("/dev/null", "a")
      STDERR.should_receive(:reopen).with(STDOUT)

      @command.daemonize!
    end

    it "does nothing if --no-daemon is in effect" do
      @config.daemon = false

      @command.should_not_receive(:fork)
      Process.should_not_receive(:setsid)
      Dir.should_not_receive(:chdir).with("/")
      File.should_not_receive(:umask)
      STDIN.should_not_receive(:reopen)
      STDOUT.should_not_receive(:reopen)
      STDERR.should_not_receive(:reopen)

      @command.daemonize!
    end
  end
end
