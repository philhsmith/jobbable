require 'spec_helper'
require 'jobbable/cli/kill'

describe Jobbable::Cli::Kill do

  before :each do
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    @command_line = mock("command line")
    @config = Jobbable::Configuration.new
    @command_line.stub!(:config).and_return(@config)
    @command_line.stub!(:logger).and_return(@logger)

    @kill = Jobbable::Cli::Kill.new(@command_line)
  end

  it "is a command" do
    @kill.should be_a Jobbable::Cli::Command
  end

  describe "#perform" do
    before :each do
      @kill.stub!(:parent_pid).and_return(1000)
      @kill.stub!(:remove_parent_pid)
    end

    it "sends a SIGKILL to the process identified by #pid" do
      @kill.stub!(:parent_pid).and_return(1000)
      Process.should_receive(:kill).with("KILL", 1000)
      @kill.perform
    end

    it "removes the parent pid file" do
      @kill.should_receive(:remove_parent_pid)
      @kill.perform
    end

    it "removes stale pid files" do
      Process.should_receive(:kill).with("KILL", 1000).and_raise(Errno::ESRCH)
      @kill.should_receive(:remove_parent_pid)
      @kill.perform
    end

    it "does nothing if no pid file was found" do
      @kill.should_receive(:parent_pid).and_raise(Errno::ENOENT)
      @kill.perform
    end
  end

end
