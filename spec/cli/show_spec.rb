require 'spec_helper'
require 'jobbable/cli/show'

describe Jobbable::Cli::Show do

  before :each do
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    @command_line = mock("command line")
    @config = Jobbable::Configuration.new
    @command_line.stub!(:config).and_return(@config)
    @command_line.stub!(:logger).and_return(@logger)

    @show = Jobbable::Cli::Show.new(@command_line)
  end

  it "is a command" do
    @show.should be_a Jobbable::Cli::Command
  end

  describe "#perform" do
    it "shows the effective configuration" do
      @config.should_receive(:to_hash).and_return(:plain_hash_config)
      YAML.should_receive(:dump).with(
        :plain_hash_config).and_return(:effective_config_yaml)
      @logger.should_receive(:info).with(:effective_config_yaml)

      @show.perform
    end
  end

end
