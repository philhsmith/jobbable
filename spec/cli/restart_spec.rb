require 'spec_helper'
require 'jobbable/cli/restart'

describe Jobbable::Cli::Restart do

  before :each do
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    @command_line = mock("command line")
    @config = Jobbable::Configuration.new
    @command_line.stub!(:config).and_return(@config)
    @command_line.stub!(:logger).and_return(@logger)

    @restart = Jobbable::Cli::Restart.new(@command_line)
  end

  it "is a command" do
    @restart.should be_a Jobbable::Cli::Command
  end

  describe "#perform" do
    before :each do
      @restart.stub!(:read_pid).and_return(1000)
      @restart.stub!(:age_parent_pid)
      @restart.stub!(:remove_pid)
      @restart.stub!(:start)

      @start = mock("start")
      Jobbable::Cli::Start.stub!(:new).with(@command_line).and_return(@start)
      @start.stub!(:perform)
    end

    it "sends a SIGUSR1 to the old parent process" do
      @restart.should_receive(:read_pid).with(
        "jobbable-old.pid").and_return(1000)
      Process.should_receive(:kill).with("USR1", 1000)
      @restart.perform
    end

    it "ages the existing parent pid file by moving it to the old pid file" do
      @restart.should_receive(:age_parent_pid)
      @restart.perform
    end

    it "starts" do
      Jobbable::Cli::Start.should_receive(:new).with(
        @command_line).and_return(@start)
      @start.should_receive(:perform)
      @restart.perform
    end

    it "removes stale pid files before starting" do
      Process.should_receive(:kill).with("USR1", 1000).and_raise(Errno::ESRCH)
      @restart.should_receive(:remove_pid).with("jobbable-old.pid")
      @restart.perform
    end
  end

end
