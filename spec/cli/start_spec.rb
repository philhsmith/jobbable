require 'spec_helper'
require 'jobbable/cli/start'

describe Jobbable::Cli::Start do

  before :each do
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    @command_line = mock("command line")
    @config = Jobbable::Configuration.new
    @command_line.stub!(:config).and_return(@config)
    @command_line.stub!(:logger).and_return(@logger)

    @start = Jobbable::Cli::Start.new(@command_line)
  end

  it "is a command" do
    @start.should be_a Jobbable::Cli::Command
  end

  describe "#perform" do
    before :each do
      @parent = mock("parent")
      Jobbable::Parent.stub!(:new).with(@command_line).and_return(@parent)
      @parent.stub!(:check_pid_directory)
      @parent.stub!(:check_pid_file)
      @parent.stub!(:open_logger)
      @parent.stub!(:start)
    end

    it "creates a Parent and #starts it" do
      Jobbable::Parent.should_receive(:new).with(
        @command_line).and_return(@parent)
      @parent.should_receive(:start)

      @start.perform
    end

    it "checks the pid directory" do
      @parent.should_receive(:check_pid_directory)
      @start.perform
    end

    it "checks the pid file" do
      @parent.should_receive(:check_pid_file).with("jobbable.pid")
      @start.perform
    end

    it "opens the parents logger" do
      @parent.should_receive(:open_logger)
      @start.perform
    end
  end

end
