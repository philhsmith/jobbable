require 'spec_helper'
require 'jobbable/cli/command_line'

describe Jobbable::Cli::CommandLine do
  before :each do
    Dir.stub!(:pwd).and_return("/here")

    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    @config = Jobbable::Configuration.new

    @command_line = Jobbable::Cli::CommandLine.new
    @command_line.config = @config
    @command_line.stub!(:logger).and_return(@logger)

    @command = mock("the command")
    @command.stub!(:open_logger)
    @command.stub!(:perform)
    Jobbable::Cli::Start.stub!(:new).with(@command_line).and_return(@command)
  end

  it "mixes in Config" do
    @command_line.should be_a Jobbable::ConfigMixin
  end

  describe "#verb" do
    it "is an attribute" do
      @command_line.verb = :the_verb
      @command_line.verb.should == :the_verb
    end
  end

  describe "#logger" do
    it "is the global logger" do
      @command_line.unstub(:logger)
      Jobbable.should_receive(:logger).and_return(:the_logger)
      @command_line.logger.should == :the_logger
    end
  end

  describe "#main" do
    before :each do
      @command_line.stub!(:configure!)
    end

    it "takes an argv and sets verb, environment, config_file and argv" do
      @command_line.main %w{start prod my/jobbable.yml --job-timeout 1234}

      @command_line.verb.should == "start"
      @command_line.environment.should == "prod"
      @command_line.config_file.should == "my/jobbable.yml"
      @command_line.argv.should == %w{--job-timeout 1234}
    end

    it "captures the original working directory" do
      Dir.should_receive(:pwd).and_return('/here')
      @command_line.main %w{start}
      @command_line.working_directory.should == '/here'
    end

    it "may omit the environment and assume development" do
      @command_line.main %w{start}
      @command_line.environment.should == "development"
    end

    it "may omit the config_file and assume config/jobbable.yml" do
      File.stub!(:exist?).and_return(true)
      @command_line.main %w{start}
      @command_line.config_file.should == "/here/config/jobbable.yml"
    end

    it "may omit extra command line options" do
      @command_line.main %w{start}
      @command_line.argv.should == []
    end

    it "assumes --help if nothing is passed" do
      @command_line.main %w{}
      @command_line.argv.should include "--help"
    end

    it "assumes --help if an illegal verb was passed" do
      @command_line.main %w{make_sandwich}
      @command_line.argv.should include '--help'
    end

    KNOWN_VERBS = Dir[
      File.expand_path("../../../lib/jobbable/cli/*.rb", __FILE__)
    ].collect { |f| File.basename(f)[0...-3] } - %w{command command_line}

    KNOWN_VERBS.each do |verb|
      it "calls #perform on a new #{verb.capitalize} on #{verb}" do
        eval("Jobbable::Cli::#{verb.capitalize}").should_receive(:new).with(
          @command_line).and_return(@command)
        @command.stub!(:open_logger)
        @command.should_receive(:perform)
        @command_line.main [verb]
      end
    end

    it "loads configuration" do
      @command_line.should_receive(:config)
      @command_line.main %w{start}
    end

    it "logs and dies on errors thrown by verbs" do
      exception = StandardError.new("explosions!")
      @command.stub!(:perform).and_raise(exception)
      @command_line.logger.should_receive(:fatal).with(exception)
      @command_line.should_receive(:exit).with(1)
      lambda { @command_line.main %w{start} }.should_not raise_error
    end
  end
end
