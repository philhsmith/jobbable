require 'spec_helper'
require 'jobbable/cli/stop'

describe Jobbable::Cli::Stop do

  before :each do
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    @command_line = mock("command line")
    @config = Jobbable::Configuration.new
    @command_line.stub!(:config).and_return(@config)
    @command_line.stub!(:logger).and_return(@logger)

    @stop = Jobbable::Cli::Stop.new(@command_line)
  end

  it "is a command" do
    @stop.should be_a Jobbable::Cli::Command
  end

  describe "#perform" do
    before :each do
      @stop.stub!(:parent_pid).and_return(1000)
    end

    it "sends a SIGTERM to the process identified by #pid" do
      @stop.should_receive(:parent_pid).and_return(1000)
      Process.should_receive(:kill).with("TERM", 1000)
      @stop.perform
    end

    it "removes stale pid files" do
      Process.should_receive(:kill).with("TERM", 1000).and_raise(Errno::ESRCH)
      @stop.should_receive(:remove_parent_pid)
      @stop.perform
    end

    it "does nothing if no pid file was found" do
      @stop.should_receive(:parent_pid).and_raise(Errno::ENOENT)
      @stop.perform
    end
  end

end
