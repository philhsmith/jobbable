require 'spec_helper'
require 'jobbable/cli/pop'

describe Jobbable::Cli::Pop do

  before :each do
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    @command_line = mock("command line")
    @config = Jobbable::Configuration.new
    @command_line.stub!(:config).and_return(@config)
    @command_line.stub!(:logger).and_return(@logger)

    @pop = Jobbable::Cli::Pop.new(@command_line)

    # Stub out some EM
    EM.stub!(:run).and_yield
    EM.stub!(:stop_event_loop)

    # and IO
    File.stub!(:open).and_yield(@io = mock("io"))
    @io.stub!(:<<)
    STDOUT.stub!(:<<)

    # and Jobbable::JSON
    Jobbable::JSON.stub!(:encode).and_return(:json)

    # And some subscriber mixin
    @pop.stub!(:subscribe)
    @pop.stub!(:unsubscribe)
    @pop.stub!(:stop)
  end

  it "is a command" do
    @pop.should be_a Jobbable::Cli::Command
  end

  it "is a subscriber" do
    @pop.should be_a Jobbable::Amqp::SubscriberMixin
  end

  describe "#parse_command_argv" do
    it "can take --queue" do
      @command_line.stub!(:command_argv).and_return %w{--queue a_queue}
      @pop.parse_command_argv[:queues].should == %w{a_queue}
    end

    it "can take --queue many times" do
      @command_line.stub!(
        :command_argv).and_return %w{--queue a_queue --queue another_queue}
      @pop.parse_command_argv[:queues].should == %w{a_queue another_queue}
    end

    it "can take --file" do
      @command_line.stub!(
        :command_argv).and_return %w{--file path/to/messages}
      @pop.parse_command_argv[:file].should == 'path/to/messages'
    end

    it "uses - as the default file" do
      @command_line.stub!(:command_argv).and_return %w{--queue a_queue}
      @pop.parse_command_argv[:file].should == '-'
    end

    it "can take --count" do
      @command_line.stub!(:command_argv).and_return %w{--count 7}
      @pop.parse_command_argv[:count].should == 7
    end

    it "uses 0 as the default count" do
      @command_line.stub!(:command_argv).and_return %w{--queue a_queue}
      @pop.parse_command_argv[:count].should == 0
    end

    it "can take --timeout" do
      @command_line.stub!(:command_argv).and_return %w{--timeout 5}
      @pop.parse_command_argv[:timeout].should == 5
    end

    it "uses 2 as the default timeout" do
      @command_line.stub!(:command_argv).and_return %w{--queue a_queue}
      @pop.parse_command_argv[:timeout].should == 2
    end
  end

  describe "#count" do
    it "is an accessor" do
      @pop.count = 14
      @pop.count.should == 14
    end
  end

  describe "#on_delivery_timeout" do
    it "shuts down" do
      @pop.should_receive(:shutdown)
      @pop.on_delivery_timeout
    end
  end

  describe "#shutdown" do
    it "stop amqp, and then eventmachine" do
      # Yielding causes the block to run
      @pop.should_receive(:stop).and_yield
      EM.should_receive(:stop_event_loop)
      @pop.shutdown
    end
  end

  describe "#pop_messages" do
    before :each do
      @queues = %w{a_queue}

      @message = mock("message").tap do |message|
        message.stub!(:header).and_return(mock("header").tap do |header|
          header.stub!(:routing_key).and_return("a_key", "another_key")
          header.stub!(:exchange).and_return(
            "an_exchange", "another_exchange")
        end)
        message.stub!(:body).and_return("a_message", "another_message")
        message.stub!(:ack)
      end

      # Yielding causes the block to run
      @pop.stub!(:subscribe).and_yield(@message)
    end

    it "starts eventmachine" do
      EM.should_receive(:run)
      @pop.pop_messages @queues, @io
    end

    it "subscribes to the queues" do
      @pop.should_receive(:subscribe).with(@queues)
      @pop.pop_messages @queues, @io
    end

    it "writes each message's routing key and body as a hash" do
      # Yielding causes the block to run
      @pop.stub!(:subscribe).and_yield(@message)

      Jobbable::JSON.should_receive(:encode).with({
        :key => "a_key",
        :exchange => "an_exchange",
        :message => "a_message"
      }).and_return(:the_json)

      @io.should_receive(:<<).with(:the_json).ordered
      @io.should_receive(:<<).with("\n").ordered

      @pop.pop_messages @queues, @io
    end

    it "acks messages after writing them" do
      Jobbable::JSON.should_receive(:encode).ordered

      @message.should_receive(:ack).ordered

      @pop.pop_messages @queues, @io
    end

    it "shuts down once count jobs or more have been started" do
      @pop.counters[:started] = 5
      @pop.count = 5

      @pop.should_receive(:shutdown)

      @pop.pop_messages @queues, @io
    end
  end

  describe "#perform" do
    before :each do
      @pop.stub!(:parse_command_argv).and_return(
        :queues => %w{a_queue},
        :file => "-",
        :count => 0,
        :timeout => 2
      )
    end

    it "parses the command line options" do
      @pop.should_receive(:parse_command_argv).and_return(
        :queues => %w{a_queue})
      @pop.perform
    end

    it "sets the count" do
      @pop.stub!(:parse_command_argv).and_return(
        :queues => %w{a_queue},
        :count => 6
      )
      @pop.perform
      @pop.count.should == 6
    end

    it "sets the delivery timeout" do
      @pop.stub!(:parse_command_argv).and_return(
        :queues => %w{a_queue},
        :timeout => 4
      )
      @pop.perform
      @pop.delivery_timeout.should == 4
    end

    it "opens the output file" do
      @pop.stub!(:parse_command_argv).and_return(
        :queues => %w{a_queue},
        :file => "a/file.json"
      )

      File.should_receive(:open).with("a/file.json", "w")
      @pop.perform
    end

    it "uses STDOUT if the output file is -" do
      @pop.stub!(:parse_command_argv).and_return(
        :queues => %w{a_queue},
        :file => "-"
      )

      File.should_not_receive(:open)
      @pop.perform
    end

    it "logs to STDERR if the output file is -" do
      @pop.stub!(:parse_command_argv).and_return(
        :queues => %w{a_queue},
        :file => "-"
      )

      @pop.perform
      Jobbable.logger.should == Jobbable::STDERR_LOGGER
    end

    it "hands the queues and output IO to #pop_messages" do
      @pop.stub!(:parse_command_argv).and_return(
        :queues => %w{a_queue another_queue},
        :file => "-"
      )

      @pop.should_receive(:pop_messages).with(
        %w{a_queue another_queue}, STDOUT)
      @pop.perform
    end

    it "whines if there are no queues to subscribe to" do
      @pop.stub!(:parse_command_argv).and_return(:queues => [])
      lambda { @pop.perform }.should raise_error ArgumentError
    end
  end

end
