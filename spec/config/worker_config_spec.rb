require 'spec_helper'

require 'jobbable/config/worker_config'

describe Jobbable::Config::WorkerConfig do
  before :each do
    @worker_config = Jobbable::Config::WorkerConfig.new
  end

  it "is an AgentConfig" do
    @worker_config.should be_a Jobbable::Config::AgentConfig
  end

  {
    :queues => %w{default}
  }.each do |option, default_value|

    it "includes #{option} with default value #{default_value.inspect}" do
      @worker_config.send(option).should == default_value
      @worker_config.send :"#{option}=", :new_value
      @worker_config.send(option).should == :new_value
    end

  end
end
