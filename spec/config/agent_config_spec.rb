require 'spec_helper'

require 'jobbable/config/agent_config'

describe Jobbable::Config::AgentConfig do
  before :each do
    @agent_config = Jobbable::Config::AgentConfig.new
  end

  {
    :count      => 1,
    :threads    => 7,
    :timers     => 100000,
    :heartbeat  => 10,
    :lifespan   => 900,
    :afterlife  => 120
  }.each do |option, default_value|

    it "includes #{option} with default value #{default_value.inspect}" do
      @agent_config.send(option).should == default_value
      @agent_config.send :"#{option}=", :new_value
      @agent_config.send(option).should == :new_value
    end

  end
end
