require 'spec_helper'
require 'jobbable/config/slot_mixin'
require 'jobbable/configuration'

describe Jobbable::Config::SlotMixin do
  before :each do
    @mixer = Object.new.tap do |mixer|
      class <<mixer
        include Jobbable::Config::SlotMixin
      end
    end
  end
  
  describe "#slot_names" do
    it "extracts any known dynamic slots names from the given string" do
      @mixer.slot_names("{job}_bar_{state}_quux").should == %w{job state}
    end

    it "reports each slot at most once" do
      @mixer.slot_names("{job}_bar_{job}_quux").should == %w{job}
    end
  end

  describe "#slot_pattern" do
    it "is a regex that captures dynamic slots in the given string" do
      pattern = @mixer.slot_pattern("foo_{job}_bar")
      pattern.should be_a Regexp
      "foo_my_job_bar".match(pattern)[1].should == "my_job"
    end

    it "escapes regex-reactive characters in the string" do
      @mixer.slot_pattern(".*{job}.*").should match ".*a_job.*"
      @mixer.slot_pattern(".*{job}.*").should_not match "anything at all"
    end

    it "captures non-negative integers for attempt" do
      @mixer.slot_pattern("{attempt}").should match "0"
      @mixer.slot_pattern("{attempt}").should match "17"
      @mixer.slot_pattern("{attempt}").should_not match "sleven"
    end

    it "captures only legal states for state" do
      Jobbable::Config::SlotMixin::STATES.each do |state|
        @mixer.slot_pattern("{state}").should match state
      end
      @mixer.slot_pattern("{state}").should_not match "notastate"
    end

    it "captures integers 0 to 9 for priority" do
      @mixer.slot_pattern("{priority}").should_not match "-1"
      (0..9).each do |i|
        @mixer.slot_pattern("{priority}").should match i.to_s
      end
      @mixer.slot_pattern("{priority}").should_not match "10"
    end

    it "matches dot-less strings for host" do
      @mixer.slot_pattern("{host}").should match "a_host*"
      @mixer.slot_pattern("{host}").should_not match "a.host"
    end

    it "matches anything at all for tags, including periods" do
      @mixer.slot_pattern("{tags}").should match "a_tag"
      @mixer.slot_pattern("{tags}").should match "a.tag"
    end
  end

  describe "#slot_values" do
    it "takes a template and a string, returning a hash of slot values" do
      actual = @mixer.slot_values("{job}.{state}", "some_job.new")
      actual[:job].should == "some_job"
      actual[:state].should == "new"
    end

    it "is nil for strings that do not match the template" do
      @mixer.slot_values(
        "{job}.dogsnot.{state}", "some_job.not-dogsnot.new").should be_nil
    end

    it "supplies default values for unreferenced dynamic slots" do
      @mixer.slot_values("", "").should == {
        :job      => 'default',
        :attempt  => 0,
        :state    => 'new',
        :priority => 0,
        :host     => 'any',
        :tags     => []
      }
    end

    it "rejects negative attempts" do
      @mixer.slot_values("{attempt}", "-1").should be_nil
    end

    it "rejects non-integer attempts" do
      @mixer.slot_values("{attempt}", "foo").should be_nil
    end

    it "translates underscores in host back to dots" do
      @mixer.slot_values(
        "{host}", "example_com")[:host].should == 'example.com'
    end

    it "decodes tags" do
      @mixer.slot_values(
        "{tags}", "bar.baz.foo")[:tags].should == %w{bar baz foo}
    end
  end

  describe "#metadata_for" do
    before :each do
      @config = Jobbable::Configuration.new.tap do |config|
        config.jobs["a_job"].keys = [
          "{job}.{attempt}",
          "foo.{job}.{attempt}"
        ]

        config.jobs["another_job"].keys = [
          "{job}.bar.{attempt}"
        ]

        config.jobs["default"].keys = [
          "baz.{job}.{attempt}"
        ]
      end
    end

    it "returns slot values for the first matching template" do
      @mixer.metadata_for(@config, "a_job.1").should == {
        :job      => 'a_job',
        :state    => 'new',
        :attempt  => 1,
        :priority => 0,
        :host     => 'any',
        :tags     => []
      }
    end

    it "tolerates different positions of the job slot" do
      @mixer.metadata_for(@config, "foo.a_job.1").should == {
        :job      => 'a_job',
        :state    => 'new',
        :attempt  => 1,
        :priority => 0,
        :host     => 'any',
        :tags     => []
      }
    end

    it "is nil if no template matches" do
      @mixer.metadata_for(@config, "unmatched").should be_nil
    end

    it "does not match jobs to templates they aren't configured with" do
      @mixer.metadata_for(@config, "foo.another_job.1").should be_nil
    end

    it "can match a default job template" do
      @mixer.metadata_for(@config, "baz.a_job.1").should == {
        :job      => 'a_job',
        :state    => 'new',
        :attempt  => 1,
        :priority => 0,
        :host     => 'any',
        :tags     => []
      }
    end

    it "can take a :job option to restrict the considered templates" do
      @mixer.metadata_for(
        @config, "a_job.1", :job => 'another_job').should be_nil
    end

    it "does not consider default templates when :job is passed" do
      @mixer.metadata_for(
        @config, "baz.a_job.1", :job => 'a_job').should be_nil
    end

    it "interprets :job => nil as if :job was not passed" do
      @mixer.metadata_for(
        @config, "a_job.1", :job => nil).should_not be_nil
    end
  end

  describe "#expand_slots_in_string" do
    it "substituting a hash of values into a string template" do
      @mixer.expand_slots_in_string("{job}_{state}",
        'job' => 'my_job', 'state' => 'new').should == 'my_job_new'
    end
  end

  describe "#expand_slots" do
    it "delegates to #expand_slots_in_string for strings" do
      @mixer.expand_slots("{job}", 'job' => 'my_job').should == \
        @mixer.expand_slots_in_string("{job}", 'job' => 'my_job')
    end

    it "recurses on arrays" do
      @mixer.expand_slots(["{job}", "{job}"],
        'job' => 'my_job').should == %w{my_job my_job}
    end

    it "recurses on hash keys and values" do
      @mixer.expand_slots({ "{job}" => "{job}" },
        'job' => 'my_job').should == { 'my_job' => 'my_job' }
    end

    it "passes other types through" do
      @mixer.expand_slots(:"{job}", 'job' => 'my_job').should == :"{job}"
    end
  end

  describe "#each_slot_expansion" do
    it "is #each on a new Slots constructed with the given values" do
      slots = mock("slots")
      Jobbable::Config::SlotMixin::Slots.should_receive(:new).
        with(:slots, :jobs, :attempts).and_return(slots)

      slots.should_receive(:each).and_yield(:combo)

      client_code = mock("client code")
      client_code.should_receive(:called).with(:combo)

      @mixer.each_slot_expansion(:slots, :jobs, :attempts) do |combo|
        client_code.called(combo)
      end
    end
  end

  describe "#slots" do
    it "is a new Slots" do
      Jobbable::Config::SlotMixin::Slots.should_receive(:new).
        with(:slots, :jobs, :attempts).and_return(slots = mock("slots"))

      @mixer.slots(:slots, :jobs, :attempts).should == slots
    end
  end
end

describe Jobbable::Config::SlotMixin::Slots do
  before :each do
    @slots = Jobbable::Config::SlotMixin::Slots.new(
      %w{state priority}, %w{my_job}, 3)
  end

  it "is an Enumerable" do
    @slots.should be_a Enumerable
  end

  it "has assumed job names" do
    @slots.jobs.should == %w{my_job}
  end

  it "has an assumed number of attempts" do
    @slots.attempts.should == 3
  end

  it "has slot names" do
    @slots.slots.should == %w{state priority}
  end

  it "whines if constructed with slot names other than those in SLOTS" do
    lambda do
      Jobbable::Config::SlotMixin::Slots.new(%w{foo bar}, %w{my_job}, 3)
    end.should raise_error ArgumentError
  end

  it "whines if constructed with the tags slot" do
    lambda do
      Jobbable::Config::SlotMixin::Slots.new(%w{tags}, %w{my_job}, 3)
    end.should raise_error ArgumentError
  end

  describe ".hostname" do
    before :each do
      Jobbable::Config::SlotMixin::Slots.hostname = nil
    end

    it "is Socket.gethostname" do
      Socket.should_receive(:gethostname).and_return('some_host')
      Jobbable::Config::SlotMixin::Slots.hostname.should == 'some_host'
    end

    it "memoizes" do
      Socket.should_receive(:gethostname).once.and_return('some_host')

      Jobbable::Config::SlotMixin::Slots.hostname
      Jobbable::Config::SlotMixin::Slots.hostname
    end
  end

  describe "#hostname" do
    it "is .hostname" do
      @slots.hostname.should == Jobbable::Config::SlotMixin::Slots.hostname
    end
  end

  describe "#attempts_for" do
    it "is the number of attempts to assume for the named job" do
      @slots.attempts_for(@slots.jobs.first).should == @slots.attempts
    end

    describe "when constructed with a attempts hash" do
      before :each do
        @slots = Jobbable::Config::SlotMixin::Slots.new(
          %w{job}, %w{my_job his_job}, "my_job" => 2, "his_job" => 4)
      end

      it "indexes into the hash" do
        @slots.attempts_for('my_job').should == 2
        @slots.attempts_for('his_job').should == 4
      end
    end
  end

  describe "#each" do
    before :each do
      Socket.stub!(:gethostname).and_return('some_host')
    end

    # Use #each instead of #to_a in testing since it is easier to look at
    it "yields hashs of slot name value pairs" do
      @slots.to_a.should == [
        { 'state' => "new",   'priority' => 0 },
        { 'state' => "new",   'priority' => 1 },
        { 'state' => "new",   'priority' => 2 },
        { 'state' => "new",   'priority' => 3 },
        { 'state' => "new",   'priority' => 4 },
        { 'state' => "new",   'priority' => 5 },
        { 'state' => "new",   'priority' => 6 },
        { 'state' => "new",   'priority' => 7 },
        { 'state' => "new",   'priority' => 8 },
        { 'state' => "new",   'priority' => 9 },
        { 'state' => "retry", 'priority' => 0 },
        { 'state' => "retry", 'priority' => 1 },
        { 'state' => "retry", 'priority' => 2 },
        { 'state' => "retry", 'priority' => 3 },
        { 'state' => "retry", 'priority' => 4 },
        { 'state' => "retry", 'priority' => 5 },
        { 'state' => "retry", 'priority' => 6 },
        { 'state' => "retry", 'priority' => 7 },
        { 'state' => "retry", 'priority' => 8 },
        { 'state' => "retry", 'priority' => 9 },
        { 'state' => "fail",  'priority' => 0 },
        { 'state' => "fail",  'priority' => 1 },
        { 'state' => "fail",  'priority' => 2 },
        { 'state' => "fail",  'priority' => 3 },
        { 'state' => "fail",  'priority' => 4 },
        { 'state' => "fail",  'priority' => 5 },
        { 'state' => "fail",  'priority' => 6 },
        { 'state' => "fail",  'priority' => 7 },
        { 'state' => "fail",  'priority' => 8 },
        { 'state' => "fail",  'priority' => 9 },
        { 'state' => "abort", 'priority' => 0 },
        { 'state' => "abort", 'priority' => 1 },
        { 'state' => "abort", 'priority' => 2 },
        { 'state' => "abort", 'priority' => 3 },
        { 'state' => "abort", 'priority' => 4 },
        { 'state' => "abort", 'priority' => 5 },
        { 'state' => "abort", 'priority' => 6 },
        { 'state' => "abort", 'priority' => 7 },
        { 'state' => "abort", 'priority' => 8 },
        { 'state' => "abort", 'priority' => 9 },
        { 'state' => "crash", 'priority' => 0 },
        { 'state' => "crash", 'priority' => 1 },
        { 'state' => "crash", 'priority' => 2 },
        { 'state' => "crash", 'priority' => 3 },
        { 'state' => "crash", 'priority' => 4 },
        { 'state' => "crash", 'priority' => 5 },
        { 'state' => "crash", 'priority' => 6 },
        { 'state' => "crash", 'priority' => 7 },
        { 'state' => "crash", 'priority' => 8 },
        { 'state' => "crash", 'priority' => 9 }
      ]
    end

    it "can include the job names it was constructed with" do
      Jobbable::Config::SlotMixin::Slots.new(
        %w{job state}, %w{my_job your_job}, 3).to_a.should == [

        { 'job' => "my_job",    'state' => "new"   },
        { 'job' => "my_job",    'state' => "retry" },
        { 'job' => "my_job",    'state' => "fail"  },
        { 'job' => "my_job",    'state' => "abort" },
        { 'job' => "my_job",    'state' => "crash" },
        { 'job' => "your_job",  'state' => "new"   },
        { 'job' => "your_job",  'state' => "retry" },
        { 'job' => "your_job",  'state' => "fail"  },
        { 'job' => "your_job",  'state' => "abort" },
        { 'job' => "your_job",  'state' => "crash" }
      ]
    end

    it "can include attempts" do
      Jobbable::Config::SlotMixin::Slots.new(
        %w{attempt}, %w{my_job}, 3).to_a.should == [

        { 'attempt' => 0 },
        { 'attempt' => 1 },
        { 'attempt' => 2 }
      ]
    end

    it "includes only legal combinations of states and attempts" do
      Jobbable::Config::SlotMixin::Slots.new(
        %w{state attempt}, %w{my_job}, 3).to_a.should == [

        # Notice new only appears on attempt 0
        { 'state' => "new",   'attempt' => 0 },

        # ..and retry appears on attempt 1 and above
        { 'state' => "retry", 'attempt' => 1 },
        { 'state' => "retry", 'attempt' => 2 },

        # The others appear in all combinations
        { 'state' => "fail",  'attempt' => 0 },
        { 'state' => "fail",  'attempt' => 1 },
        { 'state' => "fail",  'attempt' => 2 },
        { 'state' => "abort", 'attempt' => 0 },
        { 'state' => "abort", 'attempt' => 1 },
        { 'state' => "abort", 'attempt' => 2 },
        { 'state' => "crash", 'attempt' => 0 },
        { 'state' => "crash", 'attempt' => 1 },
        { 'state' => "crash", 'attempt' => 2 }
      ]
    end

    describe "when constructed with a attempts hash" do
      it "includes per-job attempts" do
        Jobbable::Config::SlotMixin::Slots.new(%w{job attempt},
          %w{my_job his_job}, 'my_job' => 2, 'his_job' => 3).to_a.should == [

          { 'job' => "my_job",  'attempt' => 0 },
          { 'job' => "my_job",  'attempt' => 1 },
          { 'job' => "his_job", 'attempt' => 0 },
          { 'job' => "his_job", 'attempt' => 1 },
          { 'job' => "his_job", 'attempt' => 2 }
        ]
      end

      it "uses the 'default' job attempt if not iterating over job" do
        Jobbable::Config::SlotMixin::Slots.new(
          %w{attempt}, %w{my_job his_job}, 'default' => 4, 'my_job' => 2,
          'his_job' => 3).to_a.should == [

          { 'attempt' => 0 },
          { 'attempt' => 1 },
          { 'attempt' => 2 },
          { 'attempt' => 3 }
        ]
      end
    end

    it "can include the transformed current host name and 'any'" do
      Jobbable::Config::SlotMixin::Slots.hostname = nil
      Socket.stub!(:gethostname).and_return("the.host.name")

      Jobbable::Config::SlotMixin::Slots.new(
        %w{host}, %w{my_job}, 3).to_a.should == [

        { 'host' => "any" },
        { 'host' => "the_host_name" }
      ]
    end
  end
end
