require 'spec_helper'
require 'jobbable/config/config_hash'

describe Jobbable::Config::ConfigHash do
  class ExampleHash < Jobbable::Config::ConfigHash
    config_property :attr,                      :default => 'default_value',
      :description => "some english", :type => String,  :optional => false

    config_property :integer_property,          :default => 3,
      :description => "a number",     :type => Integer

    config_property :boolean_property,          :default => true,
      :description => "a flag",       :type => :boolean

    config_property :things,                    :default => [],
      :description => "a list",       :type => Array

    config_property :descriptionless_property,  :default => 'derp'
    config_property :typeless_property,         :default => 'a lerp'
    config_property :optional_property,         :optional => true
  end

  before :each do
    @example_hash = ExampleHash.new
  end

  it "starts attributes at the default value" do
    @example_hash.attr.should == 'default_value'
    @example_hash['attr'].should == 'default_value'
  end

  it "freezes freezeable defaults" do
    ExampleHash.defaults['attr'].should be_frozen
    ExampleHash.defaults['things'].should be_frozen
  end

  it "has setters for attibutes" do
    @example_hash.attr = :new_default
    @example_hash.attr.should == :new_default
  end

  it "has descriptions" do
    @example_hash.descriptions['attr'].should == "some english"
  end

  it "knows optional attributes" do
    @example_hash.optional['optional_property'].should be_true
  end

  it "requires attributes by default" do
    @example_hash.optional['attr'].should be_false
  end

  it "has types" do
    @example_hash.types['integer_property'].should == Integer
  end

  it "uses String as the default type" do
    @example_hash.types['typeless_property'].should == String
  end

  describe "#declare_simple_options" do
    before :each do
      @opts = OptionParser.new
    end

    it "declares options on an OptionParser that set values on the block" do
      @example_hash.declare_simple_options(@opts) { @example_hash }
      @opts.parse %w{--attr foo}
      @example_hash.attr.should == "foo"
    end

    it "skips options without descriptions" do
      @example_hash.declare_simple_options(@opts) { @example_hash }
      lambda do
        @opts.parse %w{--descriptionless-property foo}
      end.should raise_error OptionParser::InvalidOption
    end

    it "mentions the description and default in the help message" do
      @example_hash.declare_simple_options(@opts) { @example_hash }
      @opts.to_s.should include @example_hash.descriptions['attr']
      @opts.to_s.should include @example_hash.defaults['attr']
    end

    it "declares Integers to the option parser" do
      @example_hash.declare_simple_options(@opts) { @example_hash }
      @opts.parse %w{--integer-property 7}
      @example_hash.integer_property.should == 7 # and not "7"
    end

    it "can apply a prefix to options" do
      @example_hash.declare_simple_options(@opts, :prefix => 'my') do
        @example_hash
      end

      @opts.parse %w{--my-attr foo}
      @example_hash.attr.should == "foo"
    end

    it "declares booleans with a pair of options" do
      @example_hash.declare_simple_options(@opts) { @example_hash }

      @opts.parse %w{--boolean-property}
      @example_hash.boolean_property.should be_true

      @opts.parse %w{--no-boolean-property}
      @example_hash.boolean_property.should be_false
    end

    it "declares arrays with a triple of options" do
      @example_hash.declare_simple_options(@opts) { @example_hash }

      @opts.parse %w{--add-thing foo}
      @opts.parse %w{--add-thing bar}
      @opts.parse %w{--add-thing baz}
      @example_hash.things.should == %w{foo bar baz}

      @opts.parse %w{--remove-thing bar}
      @example_hash.things.should == %w{foo baz}

      @opts.parse %w{--clear-things}
      @example_hash.things.should == %w{}
    end
  end

  describe "#defaults!" do
    it "merges in the defaults" do
      @example_hash.attr = 'foo'
      @example_hash.defaults!
      @example_hash.attr.should == 'default_value'
    end

    it "dups frozen default values" do
      @example_hash.defaults!
      @example_hash.things.should_not be_frozen
    end
  end

  describe "#valid? (and #errors)" do
    it "is true for valid values" do
      @example_hash.should be_valid
      @example_hash.errors.should be_empty
    end

    it "complains if required properties are nil" do
      @example_hash.attr = nil
      @example_hash.should_not be_valid
      @example_hash.errors['attr'].should_not be_empty
    end

    it "allows optional properties to be nil" do
      @example_hash.optional_property = nil
      @example_hash.should be_valid
      @example_hash.errors.should be_empty
    end

    it "complains if integer properties are negative" do
      @example_hash.integer_property = -1
      @example_hash.should_not be_valid
      @example_hash.errors['integer_property'].should_not be_empty
    end
  end

  describe "#validate!" do
    it "raises an error if not valid" do
      @example_hash.should_receive(:valid?).and_return(false)
      @example_hash.should_receive(:errors).and_return(
        'timeout' => ['is waay too short'])

      lambda do
        @example_hash.validate!
      end.should raise_error(ArgumentError, "timeout is waay too short")
    end
  end

  describe "#to_hash" do
    it "returns itself as a pure Hash instance" do
      @example_hash['foo'] = 'bar'
      @example_hash.to_hash.should be_instance_of Hash
      @example_hash.to_hash['foo'].should == 'bar'
    end
  end

  describe ".inherited" do
    class ExampleChildHash < ExampleHash
    end

    %w{defaults types descriptions optional}.each do |metadata|
      it "copies #{metadata} to the child's #{metadata}" do
        ExampleChildHash.send(metadata).should == ExampleHash.send(metadata)
        ExampleChildHash.send(metadata).object_id.should_not == \
          ExampleHash.send(metadata).object_id
      end
    end
  end
end
