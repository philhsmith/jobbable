require 'spec_helper'

require 'jobbable/config/exchange_config'

describe Jobbable::Config::ExchangeConfig do
  before :each do
    @exchange_config = Jobbable::Config::ExchangeConfig.new
  end

  {
    :type         => 'topic',
    :name         => 'jobbable',
    :passive      => false,
    :durable      => false,
    :auto_delete  => false,
    :internal     => false,
    :wait         => false,
    :declare      => true
  }.each do |option, default_value|

    it "includes #{option} with default value #{default_value.inspect}" do
      @exchange_config.send(option).should == default_value
      @exchange_config.send :"#{option}=", :new_value
      @exchange_config.send(option).should == :new_value
    end

  end

  describe "#valid?" do
    it "requires exchange type to be one of 'topic', 'direct', or 'fanout'" do
      @exchange_config.type = 'fanboy'
      @exchange_config.should_not be_valid
      @exchange_config.errors['type'].should_not be_empty
    end
  end
end
