require 'spec_helper'

require 'jobbable/config/funnel_config'

describe Jobbable::Config::FunnelConfig do
  before :each do
    @funnel_config = Jobbable::Config::FunnelConfig.new
  end

  it "is an AgentConfig" do
    @funnel_config.should be_a Jobbable::Config::AgentConfig
  end

  {
    :count => 0,
    :queues => %w{default},
    :exchanges => %w{default},
    :key => "default.0.new.0.any._",
    :max => 20
  }.each do |option, default_value|

    it "includes #{option} with default value #{default_value.inspect}" do
      @funnel_config.send(option).should == default_value
      @funnel_config.send :"#{option}=", :new_value
      @funnel_config.send(option).should == :new_value
    end

  end
end
