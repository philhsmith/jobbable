require 'spec_helper'

require 'jobbable/config/job_config'

describe Jobbable::Config::JobConfig do
  before :each do
    @job_config = Jobbable::Config::JobConfig.new
  end

  {
    :timeout    => 180,
    :attempts   => 5,
    :exchanges  => ['default'],
    :keys       => ["{job}.{attempt}.{state}.{priority}.{host}.{tags}"]
  }.each do |option, default_value|

    it "includes #{option} with default value #{default_value.inspect}" do
      @job_config.send(option).should == default_value
      @job_config.send :"#{option}=", :new_value
      @job_config.send(option).should == :new_value
    end

  end

  it "mixes in slots" do
    @job_config.should be_a Jobbable::Config::SlotMixin
  end

  describe "#valid?" do
    it "requires job key templates to conform to topic matching syntax" do
      @job_config.keys = ['mixes#and*without.periods']
      @job_config.should_not be_valid
      @job_config.errors['keys'].should_not be_empty
    end

    it "requires job key templates to start with {job}" do
      @job_config.keys = ['{attempt}.{job}']
      @job_config.should_not be_valid
      @job_config.errors['keys'].should_not be_empty
    end

    it "requires job key templates to include {attempt}" do
      @job_config.keys = ['{job}.but.no.attempt']
      @job_config.should_not be_valid
      @job_config.errors['keys'].should_not be_empty
    end

    it "forbids job key templates from including unknown dynamic slots" do
      @job_config.keys = ['{job}.{attempt}.{voodoo}']
      @job_config.should_not be_valid
      @job_config.errors['keys'].should_not be_empty
    end
  end
end
