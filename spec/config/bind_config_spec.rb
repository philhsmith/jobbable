require 'spec_helper'

require 'jobbable/config/bind_config'

describe Jobbable::Config::BindConfig do
  before :each do
    @bind_config = Jobbable::Config::BindConfig.new
  end

  {
    :exchange => 'default',
    :queue    => 'default',
    :key      => '#',
    :wait     => false
  }.each do |option, default_value|

    it "includes #{option} with default value #{default_value.inspect}" do
      @bind_config.send(option).should == default_value
      @bind_config.send :"#{option}=", :new_value
      @bind_config.send(option).should == :new_value
    end

  end

  it "mixes in slots" do
    @bind_config.should be_a Jobbable::Config::SlotMixin
  end

  describe "#valid?" do
    it "requires bind keys to conform to topic matching syntax" do
      @bind_config.key = 'mixes#and*without.periods'
      @bind_config.should_not be_valid
      @bind_config.errors['key'].should_not be_empty
    end
  end
end
