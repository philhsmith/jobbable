require 'spec_helper'

require 'jobbable/config/queue_config'

describe Jobbable::Config::QueueConfig do
  before :each do
    @queue_config = Jobbable::Config::QueueConfig.new
  end

  {
    :type         => 'subscribe',
    :name         => 'jobbable',
    :inflight     => 10,
    :burst        => 1,
    :passive      => false,
    :durable      => false,
    :exclusive    => false,
    :auto_delete  => false,
    :wait         => false
  }.each do |option, default_value|

    it "includes #{option} with default value #{default_value.inspect}" do
      @queue_config.send(option).should == default_value
      @queue_config.send :"#{option}=", :new_value
      @queue_config.send(option).should == :new_value
    end

  end

  describe "#valid?" do
    it "requires queue type to be one of 'subscribe', or 'pop'" do
      @queue_config.type = 'pop_star'
      @queue_config.should_not be_valid
      @queue_config.errors['type'].should_not be_empty
    end
  end
end
