require 'spec_helper'
require 'jobbable/amqp/configured_amqp'

describe Jobbable::Amqp::ConfiguredAmqp do

  before :each do
    @config = Jobbable::Configuration.new
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end
    Jobbable.stub!(:logger).and_return(@logger)

    @thread = mock "em thread"

    # Ignore some EM and AMQP calls
    Thread.stub!(:new)
    EM.stub!(:run)
    EM.stub!(:stop_event_loop)
    EM.stub!(:schedule).and_yield
    EM.stub!(:reactor_running?).and_return(true)
    EM.stub!(:reactor_thread?).and_return(false)
    EM.stub!(:threadpool).and_return(nil)
    AMQP.stub!(:connect).and_return(@connection = mock("amqp connection"))
    MQ.stub!(:new).and_return(@channel = mock("MQ"))
    MQ.stub!(:error)

    # Some shmancy mocking on the MQ channel
    @channel.stub!(:prefetch)

    queue_mocks = {}
    @channel.stub!(:queue).and_return do |name, options|
      queue_mocks[name] ||= mock("queue #{name}").tap do |q|
        q.stub! :bind
        q.stub! :subscribe
        q.stub!(:subscribed?).and_return(false)
        q.stub! :pop
      end
    end

    exchange_mocks = {}
    [:topic, :fanout, :direct].each do |factory_method|
      @channel.stub!(factory_method).and_return do |name, options|
        exchange_mocks[name] ||= mock(
          "#{factory_method} exchange #{name}").tap do |e|
            e.stub!(:name).and_return(name)
        end
      end
    end

    @client_code = mock "client code"

    @configured_amqp = Jobbable::Amqp::ConfiguredAmqp.new(@config)
    @configured_amqp.em_thread = @thread
    @configured_amqp.connection = @connection

    # Mock out some thread synchronization primitives
    @condition_variable = mock("condition variable")
    @configured_amqp.stub!(:new_cond).and_return(@condition_variable)
    @condition_variable.stub!(:wait)
    @condition_variable.stub!(:signal)

    # Yielding causes the block to run
    @configured_amqp.stub!(:synchronize).and_yield
  end

  it "has a configuration" do
    @configured_amqp.config.should == @config
  end

  describe "#logger" do
    it "is the global logger" do
      Jobbable.should_receive(:logger).and_return(@logger)
      @configured_amqp.logger.should == @logger
    end
  end

  describe "#reconnect_delay" do
    it "is an attribute" do
      @configured_amqp.reconnect_delay = 7
      @configured_amqp.reconnect_delay.should == 7
    end

    it "lazy-initializes to 1" do
      @configured_amqp.reconnect_delay = nil
      @configured_amqp.reconnect_delay.should == 1
    end
  end

  describe "#relax_reconnect_delay" do
    before :each do
      @configured_amqp.stub!(:start_relax_reconnect_delay_timer)
    end

    it "halves the reconnect delay" do
      @configured_amqp.reconnect_delay = 4
      @configured_amqp.relax_reconnect_delay
      @configured_amqp.reconnect_delay.should == 2
    end

    it "does not set the reconnect delay less than 1" do
      @configured_amqp.reconnect_delay = 1.5
      @configured_amqp.relax_reconnect_delay
      @configured_amqp.reconnect_delay.should == 1
    end

    it "clears the old relaxation timer" do
      @configured_amqp.relax_reconnect_delay_timer = :the_old_timer
      @configured_amqp.relax_reconnect_delay
      @configured_amqp.relax_reconnect_delay_timer.should be_nil
    end

    it "restarts the relaxing timer if the new delay greater than 1" do
      # The next relax call will set the delay above the minimum
      @configured_amqp.reconnect_delay = 10
      @configured_amqp.should_receive(:start_relax_reconnect_delay_timer)
      @configured_amqp.relax_reconnect_delay
    end
  end

  describe "#start_relax_reconnect_delay_timer" do
    before :each do
      @configured_amqp.reconnect_delay = 8
      @timer = mock("back-off timer")
    end

    it "lazily starts a one-shot timer firing after 31 seconds" do
      EM.should_receive(:add_timer).with(31).and_return(@timer)
      @configured_amqp.start_relax_reconnect_delay_timer
      @configured_amqp.relax_reconnect_delay_timer.should == @timer
    end

    it "calls #relax_reconnect_delay when the timer expires" do
      # Yielding causes the block to run
      EM.should_receive(:add_timer).and_yield
      @configured_amqp.should_receive(:relax_reconnect_delay)

      @configured_amqp.start_relax_reconnect_delay_timer
    end
  end

  describe "#on_disconnect" do
    before :each do
      @configured_amqp.stub!(:start_relax_reconnect_delay_timer)
    end

    it "doubles and returns the reconnect delay" do
      @configured_amqp.reconnect_delay = 2

      @configured_amqp.on_disconnect.should == 4
      @configured_amqp.reconnect_delay.should == 4
    end

    it "starts the backoff timer" do
      @configured_amqp.should_receive(:start_relax_reconnect_delay_timer)
      @configured_amqp.on_disconnect
    end
  end

  describe "#connection" do
    it "is an attribute" do
      @configured_amqp.connection = :a_connection
      @configured_amqp.connection.should == :a_connection
    end
  end

  describe "#connection=" do
    it "clears all channels" do
      @configured_amqp.channels[:some_channel] = :channel_object
      @configured_amqp.connection = :something_else
      @configured_amqp.channels.should be_empty
    end

    it "forgets bound exchanges" do
      @configured_amqp.bound_exchanges[:some_exchange] = true
      @configured_amqp.connection = :something_else
      @configured_amqp.bound_exchanges.should be_empty
    end
  end

  describe "#connected?" do
    it "holds if #connection is not nil" do
      @configured_amqp.connection = :not_nil
      @configured_amqp.should be_connected

      @configured_amqp.connection = nil
      @configured_amqp.should_not be_connected
    end
  end

  describe "#start" do
    before :each do
      @configured_amqp.connection = nil
      @connection = mock("the new connection")
      # Yielding causes the block to run
      Thread.stub!(:new).and_yield
      EM.stub!(:run).and_yield
      @connection.stub!(:callback).and_yield(@connection)
      AMQP.stub!(:connect).and_return(@connection)
      EM.stub!(:reactor_running?).and_return(false)
    end

    it "runs eventmachine in a new thread" do
      thread = mock("EM thread")

      # Yielding causes the block to run
      Thread.should_receive(:new).and_yield.and_return(thread)
      EM.should_receive(:run)

      @configured_amqp.start
      @configured_amqp.em_thread.should == thread
    end

    it "clears the connection once eventmachine exits" do
      # Otherwise setting the connection makes #start a no-op.
      @configured_amqp.stub!(:connected?).and_return(false)

      @configured_amqp.connection = :a_connection
      @configured_amqp.start
      @configured_amqp.connection.should be_nil
    end

    it "does not run EM if it is already running" do
      EM.stub!(:reactor_running?).and_return(true)

      Thread.should_not_receive(:new)
      EM.should_not_receive(:run)

      @configured_amqp.start
    end

    it "passes the given callback along to the connection" do
      @client_code.should_receive(:called)
      @configured_amqp.start { @client_code.called }
    end

    it "schedules the passed block (if any) if EM is already running" do
      EM.stub!(:reactor_running?).and_return(true)

      # Yielding causes the block to run
      EM.should_receive(:schedule).and_yield

      @client_code.should_receive(:called)
      @configured_amqp.start { @client_code.called }
    end

    it "connects to AMQP (in EM) with the configured options" do
      # Avoid having the EM thread clearing the connection when it exits.
      EM.stub!(:reactor_running?).and_return(true)

      AMQP.should_receive(:connect).with(hash_including(
        :host => @config.host,
        :port => @config.port,
        :vhost => @config.virtual_host
      )).and_return(@connection)

      # Yielding causes the block to run
      @connection.should_receive(:callback).and_yield(@connection)

      @configured_amqp.start
      @configured_amqp.connection.should == @connection
    end

    it "logs AMQP errors" do
      # AMQP reports strings, not exceptions it seems.
      # Yielding causes the block to run
      MQ.should_receive(:error).and_yield("explosions!")

      @logger.should_receive(:error).with("explosions!")

      @configured_amqp.start
    end

    it "logs EM errors if starting EM" do
      exception = StandardError.new("explosions!")

      EM.should_receive(:run)

      # Yielding causes the block to run
      EM.should_receive(:error_handler).and_yield(exception)

      @logger.should_receive(:error).with(exception)

      @configured_amqp.start
    end

    it "does nothing if already connected" do
      @configured_amqp.connection = @connection
      AMQP.should_not_receive(:connect)
      @configured_amqp.start
    end

    it "still calls the block if present when already connected" do
      @configured_amqp.connection = @connection
      AMQP.should_not_receive(:connect)
      @client_code.should_receive(:called)
      @configured_amqp.start { @client_code.called }
    end

    {
      :user     => 'username',
      :pass     => 'password',
      :timeout  => 'connection_timeout',
      :logging  => 'amqp_logging'
    }.each do |connect_param, config_property|
      it "passes config.#{config_property} as #{connect_param} if present" do
        @config[config_property] = :config_value
        AMQP.should_receive(:connect).with(
          hash_including connect_param => :config_value)
        @configured_amqp.start
      end
    end

    it "calls #on_disconnect as the :reconnect policy" do
      @configured_amqp.should_receive(:on_disconnect).and_return(:what_to_do)

      AMQP.should_receive(:connect) do |options|
        options.should have_key :reconnect
        options[:reconnect].call.should == :what_to_do
      end

      @configured_amqp.start
    end
  end

  describe "#stop" do
    before :each do
      # Yielding causes the block to run
      @connection.stub!(:close).and_yield

      @configured_amqp.connection = @connection
      @configured_amqp.em_thread = @thread
      @configured_amqp.em_stopped = @condition_variable
    end

    it "closes the connection, passing in the given block" do
      # Yielding causes the block to run
      @connection.should_receive(:close).and_yield
      @client_code.should_receive(:called)
      @configured_amqp.stop { @client_code.called }
    end

    it "calls the block if no connection exists" do
      @configured_amqp.connection = nil
      @client_code.should_receive(:called)
      @configured_amqp.stop { @client_code.called }
    end

    it "does nothing if no connection exists and no block was passed" do
      @configured_amqp.connection = nil
      @configured_amqp.stop
    end

    it "clears the connection once closed" do
      @configured_amqp.stop
      @configured_amqp.connection.should be_nil
    end

    it "stops EM and clears its thread if it was started by #start" do
      EM.should_receive(:stop_event_loop)
      @configured_amqp.stop
      @configured_amqp.em_thread.should be_nil
    end

    it "doesn't stop eventmachine if it was not started by #start" do
      @configured_amqp.em_thread = nil

      EM.should_not_receive(:stop_event_loop)
      @configured_amqp.stop
    end
  end

  describe "#with_connection" do
    it "passes the given block to #start if not connected" do
      @configured_amqp.connection = nil

      # Yielding causes the block to run
      @configured_amqp.should_receive(:start).and_yield

      @client_code.should_receive(:called)
      @configured_amqp.with_connection { @client_code.called }
    end

    it "calls the block immediately if already connected" do
      @configured_amqp.connection = :a_connection
      @configured_amqp.should_not_receive(:start)

      @client_code.should_receive(:called)
      @configured_amqp.with_connection { @client_code.called }
    end
  end

  describe "#channels" do
    it "lazy-initializes to a hash" do
      @configured_amqp.channels = nil
      @configured_amqp.channels.should == {}
    end
  end

  describe "#channel" do
    before :each do
      @configured_amqp.connection = :the_connection
      MQ.stub!(:new).with(:the_connection).and_return(:some_channel)
    end

    it "is an AMQP channel" do
      MQ.should_receive(:new).with(:the_connection).and_return(:the_channel)
      @configured_amqp.channel('default').should == :the_channel
    end

    it "lazy-initializes new channels" do
      MQ.should_receive(:new).once.and_return(:the_channel)
      @configured_amqp.channel('default').should == :the_channel
      @configured_amqp.channel('default').should == :the_channel
    end

    it "keeps separate channels for each passed name" do
      MQ.should_receive(:new).and_return(:the_channel, :another_channel)
      @configured_amqp.channel('default').should == :the_channel
      @configured_amqp.channel('another_channel').should == :another_channel
    end

    it "assumes the name 'default' by default" do
      @configured_amqp.channel.should == @configured_amqp.channel('default')
    end

    it "whines if no connection is present" do
      @configured_amqp.connection = nil
      MQ.should_not_receive(:new)
      lambda do
        @configured_amqp.channel
      end.should raise_error(Jobbable::NotConnectedError)
    end
  end

  describe "#exchange" do
    before :each do
      @config.exchanges['an_exchange'].type = "topic"
      @config.exchanges['an_exchange'].name = "my_app"
    end

    it "takes an exchange name and produces the appropriate object" do
      @channel.should_receive(:topic).with(
        "my_app", instance_of(Hash)).and_return(:the_exchange_proxy)
      @configured_amqp.exchange('an_exchange').should == :the_exchange_proxy
    end

    %w{fanout direct}.each do |exchange_type|
      it "creates a #{exchange_type} exchange if asked" do
        @config.exchanges['an_exchange'].type = exchange_type

        @channel.should_receive(exchange_type.to_sym).with(
          "my_app", instance_of(Hash)).and_return(:the_exchange_proxy)
        @configured_amqp.exchange('an_exchange').should == :the_exchange_proxy
      end
    end

    %w{passive durable auto_delete internal}.each do |config_property|
      it "passes along the #{config_property} configuration property" do
        value = @config.exchanges['an_exchange'][config_property]
        @channel.should_receive(:topic).with(
          "my_app", hash_including(config_property.to_sym => value))
        @configured_amqp.exchange('an_exchange')
      end
    end

    {
      'wait'    => :nowait,
      'declare' => :no_declare
    }.each do |config_property, protocol_parameter|
      it "passes the negated #{config_property} as #{protocol_parameter}" do
        negated_value = !@config.exchanges['an_exchange'][config_property]
        @channel.should_receive(:topic).with(
          "my_app", hash_including(protocol_parameter => negated_value))
        @configured_amqp.exchange('an_exchange')
      end
    end
  end

  describe "#queue_status" do
    before :each do
      @config.queues['a_queue'].name = "my_amqp_queue"
      @queue = mock("the queue")
      @queue.stub!(:status)
      @channel.stub!(:queue).and_return(@queue)
      EM::Timer.stub!(:new).and_return(@timer = mock("timer"))
      @timer.stub!(:cancel)
    end

    it "delegates the block to #status on the passive queue proxy" do
      @channel.should_receive(:queue).with("my_amqp_queue", :passive => true).
        and_return(@queue)

      # Yielding causes the block to run
      @queue.should_receive(:status).and_yield(:message_count)

      message_count = nil
      @configured_amqp.queue_status('a_queue') do |c|
        # Unlike reality, this spec may expect the block to run immediately
        message_count = c
      end

      message_count.should == :message_count
    end

    it "whines if called without a block on the reactor thread" do
      EM.should_receive(:reactor_thread?).and_return(true)

      lambda do
        @configured_amqp.queue_status('a_queue')
      end.should raise_error(Jobbable::DeadlockError)
    end

    it "blocks and returns the message count if called without a block" do
      # Yielding causes the block to run
      @queue.should_receive(:status).and_yield(:message_count)

      @configured_amqp.queue_status('a_queue').should == :message_count
    end

    it "sets a 5 second timeout in case the status never comes back" do
      EM::Timer.should_receive(:new).with(5).and_yield.and_return(@timer)
      @queue.should_receive(:status) # and not yield

      @condition_variable.should_receive(:signal)

      @configured_amqp.queue_status('a_queue').should be_nil
    end

    it "can take a :timeout option instead" do
      EM::Timer.should_receive(:new).with(:sleven).and_return(@timer)
      @configured_amqp.queue_status('a_queue', :timeout => :sleven)
    end

    it "cancels the timer once control returns to the calling thread" do
      @timer.should_receive(:cancel)

      # Yielding causes the block to run
      @queue.should_receive(:status).and_yield(:message_count)

      @configured_amqp.queue_status('a_queue')
    end
  end

  describe "#queue" do
    before :each do
      @config.queues['a_queue'].name = "my_app"
    end

    it "takes a queue name and produces the appropriate object" do
      @channel.should_receive(:queue).with(
        "my_app", instance_of(Hash)).and_return(:the_queue_proxy)
      @configured_amqp.queue('a_queue').should == :the_queue_proxy
    end

    %w{passive durable exclusive auto_delete}.each do |config_property|
      it "passes along the #{config_property} configuration property" do
        value = @config.queues['a_queue'][config_property]
        @channel.should_receive(:queue).with(
          "my_app", hash_including(config_property.to_sym => value))
        @configured_amqp.queue('a_queue')
      end
    end

    it "passes along the negated wait configuration property as nowait" do
      negated_value = !@config.queues['a_queue']['wait']
      @channel.should_receive(:queue).with(
        "my_app", hash_including(:nowait => negated_value))
      @configured_amqp.queue('a_queue')
    end
  end

  describe "#bind" do
    before :each do
      @config.exchanges['an_exchange'].name = 'my_exchange'
      @config.queues['a_queue'].name = 'my_queue'

      @a_queue      = @configured_amqp.queue('a_queue')
      @an_exchange  = @configured_amqp.exchange('an_exchange')
    end

    it "binds the named queue to the named exchange with the given key" do
      @a_queue.should_receive(:bind).with(
        'my_exchange', hash_including(:key => 'a_bind.key'))
      @configured_amqp.bind "an_exchange", "a_queue", :key => 'a_bind.key'
    end

    it "passes along the negated wait option as nowait" do
      @a_queue.should_receive(:bind).with(
        'my_exchange', hash_including(:nowait => true))
      @configured_amqp.bind "an_exchange", "a_queue", :wait => false
    end
  end

  describe "#unbind" do
    before :each do
      @config.exchanges['an_exchange'].name = 'my_exchange'
      @config.queues['a_queue'].name = 'my_queue'

      @a_queue      = @configured_amqp.queue('a_queue')
      @an_exchange  = @configured_amqp.exchange('an_exchange')
    end

    it "binds the named queue to the named exchange with the given key" do
      @a_queue.should_receive(:unbind).with(
        'my_exchange', hash_including(:key => 'a_bind.key'))
      @configured_amqp.unbind "an_exchange", "a_queue", :key => 'a_bind.key'
    end

    it "passes along the negated wait option as nowait" do
      @a_queue.should_receive(:unbind).with(
        'my_exchange', hash_including(:nowait => true))
      @configured_amqp.unbind "an_exchange", "a_queue", :wait => false
    end
  end

  describe "#bind_queues" do
    before :each do
      @config.exchanges['an_exchange'].name = 'my_exchange'
      @config.queues['a_queue'].name = 'my_queue'
      @config.queues['another_queue'].name = 'my_other_queue'

      @config.binds['a_bind'].tap do |bind|
        bind.exchange = 'an_exchange'
        bind.queue = 'a_queue'
        bind.key = 'a_bind.key'
      end

      @config.binds['all_to_an_exchange'].tap do |bind|
        bind.exchange = 'an_exchange'
        bind.queue = 'all'
        bind.key = 'all_to_an_exchange.key'
      end

      @config.binds['a_queue_to_all'].tap do |bind|
        bind.exchange = 'all'
        bind.queue = 'a_queue'
        bind.key = 'a_queue_to_all.key'
      end

      @config.binds['another_bind'].tap do |bind|
        bind.exchange = 'an_exchange'
        bind.queue = 'a_queue'
        bind.key = 'another_bind.key'
      end

      @a_queue          = @configured_amqp.queue('a_queue')
      @another_queue    = @configured_amqp.queue('another_queue')
      @an_exchange      = @configured_amqp.exchange('an_exchange')

      @configured_amqp.stub!(:bind)
    end

    it "binds configured queues to the named exchange" do
      @configured_amqp.should_receive(:bind).with(
        'an_exchange', 'a_queue', hash_including(:key => 'a_bind.key'))
      @configured_amqp.bind_queues('an_exchange')
    end

    it "passes along the wait configuration property" do
      @configured_amqp.should_receive(:bind).with('an_exchange', 'a_queue',
        hash_including(:wait => @config.binds['a_bind']['wait']))
      @configured_amqp.bind_queues('an_exchange')
    end

    it "interprets the 'all' queue as all declared queues" do
      @configured_amqp.should_receive(:bind).with('an_exchange', 'a_queue',
        hash_including(:key => 'all_to_an_exchange.key'))
      @configured_amqp.should_receive(:bind).with('an_exchange',
        'another_queue', hash_including(:key => 'all_to_an_exchange.key'))

      @configured_amqp.bind_queues('an_exchange')
    end

    it "interprets the 'all' exchange as all declared exchanges" do
      @configured_amqp.should_receive(:bind).with('an_exchange', 'a_queue',
        hash_including(:key => 'a_queue_to_all.key'))
      @configured_amqp.bind_queues('an_exchange')
    end

    it "can bind the same queue to the same exchange with multiple keys" do
      @configured_amqp.should_receive(:bind).with('an_exchange', 'a_queue',
        hash_including(:key => 'a_bind.key'))
      @configured_amqp.should_receive(:bind).with('an_exchange', 'a_queue',
        hash_including(:key => 'another_bind.key'))

      @configured_amqp.bind_queues('an_exchange')
    end
  end

  describe "#bound_exchanges" do
    it "lazy-initializes to an empty hash" do
      @configured_amqp.bound_exchanges = nil
      @configured_amqp.bound_exchanges.should == {}
    end
  end

  describe "#ensure_bound" do
    it "binds configured queues to the passed exchange" do
      @configured_amqp.should_receive(:bind_queues).with('an_exchange')
      @configured_amqp.should_receive(:bind_queues).with('another_exchange')

      @configured_amqp.ensure_bound 'an_exchange'
      @configured_amqp.ensure_bound 'another_exchange'
    end

    it "only binds any given exchange on the first call" do
      @configured_amqp.should_receive(:bind_queues).once
      @configured_amqp.ensure_bound 'an_exchange'
      @configured_amqp.ensure_bound 'an_exchange'
    end
  end

  describe "#subscribe" do
    before :each do
      @config.queues['a_queue'].name = 'my_queue'
      @config.queues['a_pop_queue'].type = 'pop'
      @config.queues['a_pop_queue'].name = 'retry_queue'

      @a_queue              = @configured_amqp.queue('a_queue')
      @a_pop_queue  = @configured_amqp.queue('a_pop_queue')
    end

    it "subscribes to the named queue" do
      # Yielding causes the block to run
      @a_queue.should_receive(:subscribe).with(
        :ack => true, :nowait => false).and_yield
      @client_code.should_receive(:message)
      @configured_amqp.subscribe('a_queue') { @client_code.message }
    end

    it "sets the queue's inflight property as the channel's prefetch" do
      @channel.should_receive(:prefetch).with(
        @config.queues['a_queue'].inflight)
      @configured_amqp.subscribe('a_queue')
    end

    it "first unsubscribes already-subscribed queues" do
      @a_queue.should_receive(:subscribed?).at_least(:once).and_return(true)
      @a_queue.should_receive(:unsubscribe)
      @configured_amqp.subscribe('a_queue')
    end

    it '"subscribes" to the named synchronous pop queue' do
      # Yielding causes the block to run
      @a_pop_queue.should_receive(:pop).with(
        :ack => true, :nowait => false).and_yield
      @client_code.should_receive(:message)
      @configured_amqp.subscribe('a_pop_queue') { @client_code.message }
    end
  end

  describe "#unsubscribe" do
    before :each do
      @config.queues['a_queue'].name = 'my_queue'
      @a_queue = @configured_amqp.queue('a_queue')

      @a_queue.stub!(:subscribed?).and_return(true)
    end

    it "unsubscribes from subscribed queues" do
      @a_queue.should_receive(:unsubscribe)
      @configured_amqp.unsubscribe('a_queue')
    end

    it "does nothing for unsubscribed queues" do
      @a_queue.should_receive(:subscribed?).and_return(false)
      @a_queue.should_not_receive(:unsubscribe)
      @configured_amqp.unsubscribe('a_queue')
    end

    it "does nothing for pop queues" do
      @config.queues['a_queue'].type = 'pop'
      @a_queue.should_not_receive(:unsubscribe)
      @configured_amqp.unsubscribe('a_queue')
    end
  end

  describe "#publish" do
    before :each do
      @configured_amqp.stub!(:exchange).with(
        'an_exchange').and_return(@exchange = mock('the exchange'))
      @configured_amqp.stub!(:ensure_bound)
      @exchange.stub!(:publish)
    end

    it "publishes the given message to the named exchange" do
      @configured_amqp.should_receive(:exchange).with(
        'an_exchange').and_return(@exchange)
      @exchange.should_receive(:publish).with('message', :some => :options)
      @configured_amqp.publish('an_exchange', 'message', :some => :options)
    end

    it "ensures the named exchange is bound" do
      @configured_amqp.should_receive(:ensure_bound).with('an_exchange')
      @configured_amqp.publish('an_exchange', 'message', :some => :options)
    end

    it "does not ensure the exchange is bound if :bind => false is passed" do
      @configured_amqp.should_not_receive(:ensure_bound)
      @configured_amqp.publish('an_exchange', 'message', :bind => false)
    end
  end

end
