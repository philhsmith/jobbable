require 'spec_helper'
require 'jobbable/amqp/publisher_mixin'

describe Jobbable::Amqp::PublisherMixin do

  before :each do
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    @config = Jobbable::Configuration.new.tap do |config|
    end

    @amqp = mock("ConfiguredAmqp")
    @amqp.stub!(:publish)
    @amqp.stub!(:bind_queues)

    # Yielding causes the block to run
    @amqp.stub!(:with_connection).and_yield

    @publisher = Object.new.tap do |publisher|
      class <<publisher
        include Jobbable::Amqp::PublisherMixin
      end
    end

    @publisher.stub!(:logger).and_return(@logger)
    @publisher.stub!(:config).and_return(@config)
    @publisher.stub!(:amqp).and_return(@amqp)

    # Neuter some system calls
    @publisher.stub!(:hostname)

    # and JSON
    Jobbable::JSON.stub!(:encode).and_return("{}")
  end

  it "mixes in ConfiguredAmqp" do
    @publisher.should be_a Jobbable::Amqp::ConfiguredAmqpMixin
  end

  it "mixes in slots" do
    @publisher.should be_a Jobbable::Config::SlotMixin
  end

  describe "#routing_key" do
    before :each do
      @config.jobs['a_job'].keys =
        ["{job}.{state}.{attempt}.{priority}.{host}.{tags}"]
    end

    it "takes a job name and returns a routing key" do
      @publisher.routing_key('a_job').should == "a_job.new.0.0.any._"
    end

    it "substitutes dynamic values into {braces} in the job's key template" do
      @config.jobs['a_job'].keys = ["{state}.{job}.{priority}"]
      @publisher.routing_key('a_job').should == "new.a_job.0"
    end

    it "can take a template passed in options" do
      @publisher.routing_key('a_job',
        :template => "{state}.{job}.{priority}").should == "new.a_job.0"
    end

    it "can take a precomputed key to return in options" do
      @publisher.routing_key('a_job',
        :key => "some.random.key").should == "some.random.key"
    end

    it "binds {job} to the job name" do
      @publisher.routing_key('a_job', :template => "{job}").should == "a_job"
    end

    it "binds {state} to job states, defaulting to new" do
      @config.jobs['a_job'].keys = ["{state}"]

      %w{new retry fail abort crash}.each do |state|
        @publisher.routing_key('a_job', :state => state).should == state
      end

      @publisher.routing_key('a_job').should == "new"

      lambda do
        @publisher.routing_key('a_job', :state => "loco")
      end.should raise_error ArgumentError
    end

    it "binds {attempt} to the passed non-negative integer, or 0" do
      @config.jobs['a_job'].keys = ["{attempt}"]

      @publisher.routing_key('a_job', :attempt => 4).should == "4"
      @publisher.routing_key('a_job').should == "0"

      lambda do
        @publisher.routing_key('a_job', :attempt => :foo)
      end.should raise_error ArgumentError

      lambda do
        @publisher.routing_key('a_job', :attempt => -1)
      end.should raise_error ArgumentError
    end

    it "requires the new state to use attempt 0" do
      @config.jobs['a_job'].keys = ["{state}.{attempt}"]

      lambda do
        @publisher.routing_key('a_job', :state => :new, :attempt => 1)
      end.should raise_error ArgumentError
    end

    it "requires the retry state to use a non-0 attempt" do
      @config.jobs['a_job'].keys = ["{state}.{attempt}"]

      lambda do
        @publisher.routing_key('a_job', :state => :retry, :attempt => 0)
      end.should raise_error ArgumentError
    end

    it "binds {priority} to the passed integer from 0 to 9, or 0" do
      @config.jobs['a_job'].keys = ["{priority}"]

      (0..9).each do |priority|
        @publisher.routing_key('a_job',
          :priority => priority).should == priority.to_s
      end

      @publisher.routing_key('a_job').should == "0"

      lambda do
        @publisher.routing_key('a_job', :priority => '¡urgente!')
      end.should raise_error ArgumentError
    end

    it "accepts string attempts and priorities" do
      @publisher.routing_key('a_job', :template => "{attempt}.{priority}",
        :attempt => "1", :priority => "2").should == "1.2"
    end

    it "binds {host} to the passed star- and pound-less host, or 'any'" do
      @config.jobs['a_job'].keys = ["{host}"]
      @publisher.routing_key('a_job', :host => 'him').should == "him"
      @publisher.routing_key('a_job').should == "any"

      lambda do
        @publisher.routing_key('a_job', :host => 'that*guy')
      end.should raise_error ArgumentError

      lambda do
        @publisher.routing_key('a_job', :host => 'that#guy')
      end.should raise_error ArgumentError
    end

    it "binds {host} to #hostname if :this is specified" do
      @config.jobs['a_job'].keys = ["{host}"]
      @publisher.should_receive(:hostname).and_return('my_server')
      @publisher.routing_key('a_job', :host => :this).should == "my_server"
    end

    it "replaces dots in {host} with underscores" do
      @config.jobs['a_job'].keys = ["{host}"]

      @publisher.should_receive(:hostname).and_return('example.com')
      @publisher.routing_key('a_job', :host => :this).should == "example_com"

      @publisher.routing_key('a_job',
        :host => 'jobs.example.com').should == "jobs_example_com"
    end

    it "binds {tags} to the sorted list of [.*#]-less tags, or one _" do
      @config.jobs['a_job'].keys = ["{tags}"]

      @publisher.routing_key(
        'a_job', :tags => %w{foo bar}).should == "bar.foo"
      @publisher.routing_key('a_job', :tags => %w{}).should == "_"
      @publisher.routing_key('a_job').should == "_"

      lambda do
        @publisher.routing_key('a_job', :tags => %w{fo.o})
      end.should raise_error ArgumentError

      lambda do
        @publisher.routing_key('a_job', :tags => %w{fo*o})
      end.should raise_error ArgumentError

      lambda do
        @publisher.routing_key('a_job', :tags => %w{fo#o})
      end.should raise_error ArgumentError
    end

    it "can modify a passed-in key" do
      @config.jobs['a_job'].keys = ["{job}.{state}.{attempt}"]
      @publisher.routing_key('a_job', :key => "a_job.retry.1",
        :state => 'abort').should == "a_job.abort.1"
    end

    it "can infer the job from the passed-in key if needed" do
      @config.jobs['a_job'].keys = ["{job}.{state}.{attempt}"]
      @publisher.routing_key(nil, :key => "a_job.retry.1",
        :state => 'abort').should == "a_job.abort.1"
    end

    it "returns unparsable, passed-in keys" do
      @config.jobs['a_job'].keys = ["{job}.{state}.{attempt}"]
      @publisher.routing_key('a_job', :key => "man.who.knows",
        :state => 'abort').should == "man.who.knows"
    end
  end

  describe "#exchanges_for_job" do
    before :each do
      @config.exchanges['default'].name = 'default_exchange'
      @config.exchanges['an_exchange'].name = 'an_exchange'
      @config.exchanges['another'].name = 'another_exchange'

      @config.jobs['a_job'].exchanges = %w{default an_exchange}
    end

    it "takes a job and gives a list of the job's exchange names" do
      @publisher.exchanges_for_job('a_job').should == %w{default an_exchange}
    end

    it "can take a :exchange override" do
      @publisher.exchanges_for_job('a_job',
        :exchange => 'another').should == %w{another}
    end

    it "can take an :exchanges override" do
      @publisher.exchanges_for_job('a_job',
        :exchanges => %w{default another}).should == %w{default another}
    end

    it "can even take both if you really want to" do
      @publisher.exchanges_for_job('a_job',
        :exchange => 'an_exchange', :exchanges => %w{default another}
      ).should == %w{an_exchange default another}
    end

    it "expands the 'all' exchange to all configured exchanges" do
      @publisher.exchanges_for_job('a_job', :exchange => 'all'
        ).sort.should == %w{default an_exchange another}.sort
    end
  end

  describe "#publish" do
    before :each do
      @config.exchanges['default'].name = 'default_exchange'
      @config.exchanges['an_exchange'].name = 'an_exchange'
      @config.exchanges['another'].name = 'another_exchange'

      @config.jobs['a_job'].exchanges = %w{default an_exchange}
    end

    it "publishes to the job's #exchanges_for_job" do
      @publisher.should_receive(:exchanges_for_job).with(
        'a_job', {}).and_return(%w{another})

      @publisher.amqp.should_receive(:publish).with(
        'another', "{}", instance_of(Hash))

      @publisher.publish :job => 'a_job'
    end

    it "whines if :job is not passed" do
      lambda { @publisher.publish }.should raise_error ArgumentError
    end

    it "guards with amqp.with_connection" do
      @publisher.amqp.should_receive(:publish)

      # Yielding causes the block to run
      @amqp.should_receive(:with_connection).and_yield

      @publisher.publish :job => 'a_job'
    end

    it "tolerates string options" do
      lambda do
        @publisher.publish 'job' => 'a_job'
      end.should_not raise_error ArgumentError
    end

    it "whines if string and symbol versions of the same option is passed" do
      lambda do
        @publisher.publish :job => 'a_job', 'job' => 'another_job'
      end.should raise_error ArgumentError
    end

    it "doesn't whine if both string and symbol copies have the same value" do
      lambda do
        @publisher.publish :job => 'a_job', 'job' => 'a_job'
      end.should_not raise_error ArgumentError
    end

    it "includes #routing_key as :key" do
      @config.jobs['a_job'].keys = ["{job}.{attempt}"]
      @publisher.amqp.should_receive(:publish).with(
        'default', "{}", hash_including(:key => "a_job.0"))
      @publisher.publish :job => 'a_job'
    end

    it "passes along the priority, if any, as :priority" do
      @publisher.amqp.should_receive(:publish).with(
        'default', "{}", hash_including(:priority => 3))
      @publisher.publish :job => 'a_job', :priority => '3'
    end

    it "passes along :content_type, if any" do
      @publisher.amqp.should_receive(:publish).with(
        'default', "{}", hash_including(:content_type => 'application/thing'))
      @publisher.publish :job => 'a_job', :content_type => 'application/thing'
    end

    it "defaults :content_type to application/json" do
      @publisher.amqp.should_receive(:publish).with(
        'default', "{}", hash_including(:content_type => 'application/json'))
      @publisher.publish :job => 'a_job'
    end

    it "serializes remaining options as json, included as the message" do
      Jobbable::JSON.should_receive(:encode).with(
        :foo => :bar, :baz => :quux).and_return("the message")
      @publisher.amqp.should_receive(:publish).with(
        'default', "the message", instance_of(Hash))
      @publisher.publish :job => 'a_job', :exchange => 'default',
        :foo => :bar, :baz => :quux
    end

    it "can just take an overriding message as :message" do
      Jobbable::JSON.should_not_receive(:encode)

      @publisher.amqp.should_receive(:publish).with(
        'default', "overriding message", instance_of(Hash))

      @publisher.publish :job => 'a_job', :exchange => 'default',
        :message => "overriding message"
    end

    it "can merge excess options with an explicitly passed-in message" do
      Jobbable::JSON.should_receive(:decode).
        with("incoming message").and_return("foo" => "bar", "baz" => "quux")
      Jobbable::JSON.should_receive(:encode).
        with(:foo => "derp", :baz => "quux").and_return("the json")

      @publisher.amqp.should_receive(:publish).with(
        "default", "the json", instance_of(Hash))

      @publisher.publish :job => 'a_job', :exchange => 'default',
        :message => "incoming message", "foo" => "derp"
    end

    it "publishes the passed-in message if it does not decode to a hash" do
      Jobbable::JSON.should_receive(:decode).
        with("incoming message").and_return(%w{foo bar})

      Jobbable::JSON.should_not_receive(:encode)

      @publisher.amqp.should_receive(:publish).with(
        "default", "incoming message", instance_of(Hash))

      @publisher.publish :job => 'a_job', :exchange => 'default',
        :message => "incoming message", "foo" => "derp"
    end

    class NativeParseError < StandardError
      include Jobbable::JSON::ParseError
    end

    it "publishes the passed-in message if it does not decode at all" do
      Jobbable::JSON.should_receive(:decode).
        with("incoming message").and_raise(NativeParseError)
      Jobbable::JSON.should_not_receive(:encode)

      @publisher.amqp.should_receive(:publish).with(
        "default", "incoming message", instance_of(Hash))

      @publisher.publish :job => 'a_job', :exchange => 'default',
        :message => "incoming message", "foo" => "derp"
    end

    it "can be called with only :key and :exchange" do
      @publisher.amqp.should_receive(:publish).with(
        'default', "{}", hash_including(:key => "key"))

      @publisher.publish :key => 'key', :exchange => 'default'
    end

    it "can be called with only :key and :exchanges" do
      @publisher.amqp.should_receive(:publish).with(
        'default', "{}", hash_including(:key => "key"))

      @publisher.amqp.should_receive(:publish).with(
        'another', "{}", hash_including(:key => "key"))

      @publisher.publish :key => 'key', :exchanges => %w{default another}
    end

    it "can use a logger passed with :logger" do
      another_logger = mock("another logger")

      @logger.should_not_receive(:info)
      another_logger.should_receive(:info)

      @publisher.publish :job => 'a_job', :logger => another_logger
    end
  end

end
