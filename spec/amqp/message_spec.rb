require 'spec_helper'
require 'jobbable/amqp/message'

describe Jobbable::Amqp::Message do
  before :each do
    @queue = :a_queue

    @header = mock("AMQP header")
    @header.stub!(:routing_key).and_return('a_job.0')
    @header.stub!(:ack)

    @body = mock("AMQP body")

    @subscriber = mock("subscriber")

    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end
    @subscriber.stub!(:logger).and_return(@logger)

    @config = Jobbable::Configuration.new
    @subscriber.stub!(:config).and_return(@config)

    @counters = Hash.new { |h, k| h[k] = 0 }
    @counters.stub!(:bump)
    @subscriber.stub!(:counters).and_return(@counters)

    @message = Jobbable::Amqp::Message.new(
      @subscriber, @queue, @header, @body)
  end

  describe "#subscriber" do
    it "is the subscriber the message was constructed with" do
      @message.subscriber.should == @subscriber
    end
  end

  describe "#queue" do
    it "is the queue name the message was constructed with" do
      @message.queue.should == @queue
    end
  end

  describe "#header" do
    it "is the header the message was constructed with" do
      @message.header.should == @header
    end
  end

  describe "#body" do
    it "is the body the message was constructed with" do
      @message.body.should == @body
    end
  end

  describe "#decoded_body" do
    it "is the json-decoded body" do
      Jobbable::JSON.should_receive(:decode).with(@body).and_return(:decoded)
      @message.decoded_body.should == :decoded
    end

    class NativeParseError < StandardError
      include Jobbable::JSON::ParseError
    end

    it "logs the message body before re-raising if it isn't parsable" do
      Jobbable::JSON.should_receive(:decode).and_raise(NativeParseError)
      @logger.should_receive(:error).with(Regexp.new @body.inspect)

      lambda do
        @message.decoded_body
      end.should raise_error Jobbable::JSON::ParseError
    end
  end

  describe "#decoded?" do
    it "holds if (and only if) #decoded_body has already been called" do
      Jobbable::JSON.stub!(:decode).and_return(:decoded)

      @message.should_not be_decoded
      @message.decoded_body
      @message.should be_decoded
    end
  end

  describe "#acked?" do
    it "can be taken as a flag on construction" do
      Jobbable::Amqp::Message.new(
        @subscriber, @queue, @header, @body, false).should_not be_acked
    end

    it "is true by default" do
      @message.should be_acked
    end
  end

  describe "#id" do
    it "is the routing key with the message #object_id" do
      @message.id.should == "#{@header.routing_key}:#{@message.object_id}"
    end
  end

  describe "#config" do
    it "is the subscriber's config" do
      @subscriber.should_receive(:config).and_return(@config)
      @message.config.should == @config
    end
  end

  describe "#original_logger" do
    it "is the subscriber's logger" do
      @subscriber.should_receive(:logger).and_return(@logger)
      @message.original_logger.should == @logger
    end
  end

  describe "#counters" do
    it "is the subscriber's counters" do
      @subscriber.should_receive(:counters).and_return(@counters)
      @message.counters.should == @counters
    end
  end

  describe "#logger" do
    before :each do
      @message.logger = nil
    end

    it "lazy-initializes to a prefix logger wrapping the original one" do
      @message.logger.should be_a Jobbable::PrefixLogger
      @message.logger.wrapped.should == @logger
    end

    it "uses the message id as the prefix" do
      @message.logger.prefix.call.should == @message.id.to_s
    end
  end

  describe "#on_ack" do
    it "exists" do
      @message.on_ack
    end
  end

  describe "#ack" do
    it "bumps the acked counter" do
      @counters.should_receive(:bump).with(:acked)
      @message.ack
    end

    it "acks the header" do
      @header.should_receive(:ack)
      @message.ack
    end

    it "does nothing if the message is not #acked?" do
      @message.should_receive(:acked?).and_return(false)
      @header.should_not_receive(:ack)
      @message.ack
    end

    it "calls #on_ack" do
      @message.should_receive(:on_ack)
      @message.ack
    end
  end

  describe "#remaining_count" do
    it "is the remaining message count for pop messages" do
      @config.queues[@message.queue].type = 'pop'
      @header.should_receive(:message_count).and_return(:remaining_count)
      @message.remaining_count.should == :remaining_count
    end

    it "is nil for subscribe messages" do
      @config.queues[@message.queue].type = 'subscribe'
      @header.should_not_receive(:message_count)
      @message.remaining_count.should be_nil
    end
  end

end
