require 'spec_helper'
require 'jobbable/amqp/configured_amqp_mixin'

describe Jobbable::Amqp::ConfiguredAmqpMixin do

  before :each do
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    @client_code = mock("client code")

    @config = Jobbable::Configuration.new.tap do |config|
    end

    @mixer = Object.new.tap do |mixer|
      class <<mixer
        include Jobbable::Amqp::ConfiguredAmqpMixin
      end
    end

    @amqp = mock("ConfiguredAmqp").tap do |amqp|
      amqp.stub! :subscribe
      amqp.stub! :stop

      # Yielding causes the block to run
      amqp.stub!(:with_connection).and_yield
    end

    Jobbable::Amqp::ConfiguredAmqp.stub!(:new).and_return(@amqp)

    @mixer.stub!(:logger).and_return(@logger)
    @mixer.stub!(:config).and_return(@config)
  end

  describe "#amqp" do
    it "lazy-initializes to a ConfiguredAmqp based on the configuration" do
      Jobbable::Amqp::ConfiguredAmqp.should_receive(:new).with(
        @config).and_return(:a_configured_amqp)

      @mixer.amqp = nil
      @mixer.amqp.should == :a_configured_amqp
    end
  end

  describe "#start" do
    it "delegates to amqp.start" do
      @client_code.should_receive(:called)

      # Yielding causes the block to run
      @amqp.should_receive(:start).and_yield

      @mixer.start { @client_code.called }
    end
  end

  describe "#stop" do
    it "delegates to amqp.stop" do
      @client_code.should_receive(:called)

      # Yielding causes the block to run
      @amqp.should_receive(:stop).and_yield

      @mixer.stop { @client_code.called }
    end
  end

  describe "#queue_status" do
    it "delegates to amqp.queue_status" do
      @client_code.should_receive(:called)

      # Yielding causes the block to run
      @amqp.should_receive(:queue_status).with("queue", {}).
        and_yield.and_return(:status)

      @mixer.queue_status("queue") { @client_code.called }.should == :status
    end

    it "guards with amqp.with_connection" do
      @mixer.amqp.should_receive(:queue_status)

      # Yielding causes the block to run
      @amqp.should_receive(:with_connection).and_yield

      @mixer.queue_status "queue"
    end
  end

end
