require 'spec_helper'
require 'jobbable/amqp/subscriber_mixin'

describe Jobbable::Amqp::SubscriberMixin do

  before :each do
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    @config = Jobbable::Configuration.new

    @amqp = mock("ConfiguredAmqp").tap do |amqp|
      amqp.stub! :subscribe
      amqp.stub! :stop

      # Yielding causes the block to run
      amqp.stub!(:with_connection).and_yield
    end

    # Stub out some EM
    EM::Timer.stub!(:new)

    @subscriber = Object.new.tap do |subscriber|
      class <<subscriber
        include Jobbable::Amqp::SubscriberMixin
      end
    end

    @subscriber.stub!(:logger).and_return(@logger)
    @subscriber.stub!(:config).and_return(@config)
    @subscriber.stub!(:amqp).and_return(@amqp)
  end

  it "mixes in ConfiguredAmqp" do
    @subscriber.should be_a Jobbable::Amqp::ConfiguredAmqpMixin
  end

  describe "#remaining_pop_messages" do
    it "lazily initializes to a Hash" do
      @subscriber.remaining_pop_messages.should be_a Hash
    end
  end

  describe "#pop" do
    before :each do
      @config.queues['a_pop_queue'].type = 'pop'
      @config.queues['a_pop_queue'].burst = 3

      @amqp.stub!(:queue).and_return(@queue = mock("the pop queue"))
    end

    it "pops the named queue a number of times equal to its burst" do
      @amqp.should_receive(:queue).at_least(:once).with(
        'a_pop_queue').and_return(@queue)
      @queue.should_receive(:pop).exactly(3).times
      @subscriber.pop('a_pop_queue')
    end

    it "pops the remaining messages in the queue, if less than the burst" do
      @subscriber.remaining_pop_messages['a_pop_queue'] = 2
      @queue.should_receive(:pop).exactly(2).times
      @subscriber.pop('a_pop_queue')
    end

    it "always pops at least 1 message" do
      @subscriber.remaining_pop_messages['a_pop_queue'] = 0
      @queue.should_receive(:pop).once
      @subscriber.pop('a_pop_queue')
    end
  end

  describe "#pop_queues" do
    it "lazy-initializes to an empty array" do
      @subscriber.pop_queues = nil
      @subscriber.pop_queues.should == []
    end
  end

  describe "#delivery_timeout" do
    it "is an accessor" do
      @subscriber.delivery_timeout = 7
      @subscriber.delivery_timeout.should == 7
    end

    it "is 10 seconds by default" do
      @subscriber.delivery_timeout = nil
      @subscriber.delivery_timeout.should == 10
    end
  end

  describe "#delivery_timer" do
    it "is an accessor" do
      @subscriber.delivery_timer = :a_delivery_timer
      @subscriber.delivery_timer.should == :a_delivery_timer
    end
  end

  describe "#restart_delivery_timeout" do
    before :each do
      @subscriber.stub!(:respond_to?).with(
        :on_delivery_timeout).and_return(true)
      @subscriber.delivery_timeout = 17
    end

    it "starts the delivery timer for the timeout and saves it" do
      EM::Timer.should_receive(:new).with(
        @subscriber.delivery_timeout).and_return(:the_timer)
      @subscriber.restart_delivery_timeout
      @subscriber.delivery_timer.should == :the_timer
    end

    it "cancels the previous delivery timer, if any" do
      @subscriber.delivery_timer = mock("old timer")
      @subscriber.delivery_timer.should_receive(:cancel)
      @subscriber.restart_delivery_timeout
    end

    it "doesn't start the delivery timer if timeout isn't positive" do
      @subscriber.delivery_timeout = 0
      EM::Timer.should_not_receive(:new)
      @subscriber.restart_delivery_timeout
    end

    it "starts the timer only if the mixer responds to on_delivery_timeout" do
      @subscriber.should_receive(:respond_to?).with(
        :on_delivery_timeout).and_return(false)
      EM::Timer.should_not_receive(:new)
      @subscriber.restart_delivery_timeout
    end

    it "calls #on_delivery_timeout on timeout" do
      # Yielding causes the block to run
      EM::Timer.stub!(:new).and_yield

      @subscriber.should_receive(:on_delivery_timeout)
      @subscriber.restart_delivery_timeout
    end
  end

  describe "#subscribe" do
    before :each do
      @header = mock("amqp header")
      @header.stub!(:empty?).and_return(false)
    end

    it "ensures an amqp connection is present" do
      @amqp.should_receive(:with_connection)
      @subscriber.subscribe(%w{foo}) { |q, h, b| }
    end

    it "subscribes to the given queues, passing messages to the block" do
      # Yielding causes the block to run
      @amqp.should_receive(:subscribe).with('foo').and_yield(@header, :body)
      Jobbable::Amqp::Message.should_receive(:new).with(
        @subscriber, 'foo', @header, :body).and_return(:message_from_foo)

      @amqp.should_receive(:subscribe).with('bar').and_yield(@header, :body)
      Jobbable::Amqp::Message.should_receive(:new).with(
        @subscriber, 'bar', @header, :body).and_return(:message_from_bar)

      client_code = mock("client code")
      client_code.should_receive(:called).with(:message_from_foo)
      client_code.should_receive(:called).with(:message_from_bar)

      @subscriber.subscribe(%w{foo bar}) do |message|
        client_code.called message
      end
    end

    it "starts the delivery timeout before subscribing" do
      @subscriber.should_receive(:restart_delivery_timeout).ordered
      @amqp.should_receive(:subscribe).with('foo').ordered

      @subscriber.subscribe(%w{foo}) { |message| }
    end

    it "restarts the delivery timeout after each message arrives" do
      @subscriber.should_receive(:restart_delivery_timeout).twice
      @amqp.should_receive(:subscribe).with('foo').and_yield @header, :body

      @subscriber.subscribe(%w{foo}) { |message| }
    end

    it "subscribes to all declared queues if passed 'all'" do
      %w{foo bar baz}.each do |queue|
        @config.queues[queue].name = "my_#{queue}_queue"
      end

      @amqp.should_receive(:subscribe).with('foo')
      @amqp.should_receive(:subscribe).with('bar')
      @amqp.should_receive(:subscribe).with('baz')

      @subscriber.subscribe(%w{foo all}) { |q, h, b| }
    end

    it "notes the queues that have type 'pop'" do
      @subscriber.pop_queues = %w{quux}

      @config.queues['foo'].type = 'pop'
      @config.queues['bar'].type = 'subscribe'
      @config.queues['baz'].type = 'pop'

      @subscriber.subscribe(%w{all}) { |q, h, b| }
      @subscriber.pop_queues.should == %w{quux baz foo}
    end

    it "records the remaining message counts from pop queues" do
      @config.queues['foo'].type = 'pop'
      @header.should_receive(:message_count).and_return(2501)

      # Yielding causes the block to run
      @amqp.should_receive(:subscribe).and_yield(@header, nil)

      @subscriber.subscribe(%w{foo}) { |q, h, b| }
      @subscriber.remaining_pop_messages['foo'].should == 2501
    end

    it "ignores messages with empty headers" do
      @header.should_receive(:empty?).and_return(true)

      # Yielding causes the block to run
      @amqp.should_receive(:subscribe).and_yield(@header, nil)

      @subscriber.subscribe(%w{foo}) do |q, h, b|
        raise "should not be called"
      end
      @subscriber.remaining_pop_messages['foo'].should == 0
    end

    it "ignores messages with nil headers" do
      # Yielding causes the block to run
      @amqp.should_receive(:subscribe).and_yield(nil, nil)

      @subscriber.subscribe(%w{foo}) do |q, h, b|
        raise "should not be called"
      end
      @subscriber.remaining_pop_messages['foo'].should == 0
    end

    it "bumps the started counter on new messages" do
      # Yielding causes the block to run
      @amqp.should_receive(:subscribe).and_yield(@header, :body)
      @subscriber.counters.should_receive(:bump).with(:started)
      @subscriber.subscribe(%w{foo}) { |q, h, b| }
    end
  end

  describe "#unsubscribe" do
    before :each do
      @amqp.stub!(:unsubscribe)
    end

    it "unsubscribes from the named queues" do
      @amqp.should_receive(:unsubscribe).with('foo')
      @amqp.should_receive(:unsubscribe).with('bar')

      @subscriber.unsubscribe %w{foo bar}
    end

    it "drops named pop queues" do
      @subscriber.pop_queues = %w{foo baz}
      @subscriber.unsubscribe %w{foo bar}
      @subscriber.pop_queues.should == %w{baz}
    end

    it "unsubscribes from all declared queues if passed 'all'" do
      %w{foo bar baz}.each do |queue|
        @config.queues[queue].name = "my_#{queue}_queue"
      end

      @amqp.should_receive(:unsubscribe).with('foo')
      @amqp.should_receive(:unsubscribe).with('bar')
      @amqp.should_receive(:unsubscribe).with('baz')

      @subscriber.unsubscribe(%w{foo all})
    end
  end

  describe "#counters" do
    before :each do
      # Yielding causes the block to run
      EM.stub!(:schedule).and_yield
    end

      it "is a hash that lazy-initializes keys to 0" do
      @subscriber.counters[:foo].should == 0
    end

    describe "#bump" do
      it "increments the named counter on the reactor" do
        # Yielding causes the block to run
        EM.should_receive(:schedule).and_yield

        @subscriber.counters[:foo].should == 0
        @subscriber.counters.bump(:foo)
        @subscriber.counters[:foo].should == 1
      end
    end

    describe "#to_s" do
      it "formats the counters nicely" do
        11.times { @subscriber.counters.bump(:started) }
        @subscriber.counters.bump(:acked)
        @subscriber.counters.to_s.should == "acked     1 started  11"
      end
    end
  end

end
