require 'spec_helper'
require 'jobbable/funnel'

describe Jobbable::Funnel do

  before :each do
    @config = Jobbable::Configuration.new.tap do |c|
      c.working_directory = '/here'
      c.pids = 'tmp/pids'

      c.funnels['funnel'].count = 2
      c.funnels['funnel'].max = 20

      c.queues["a_queue"].type = 'subscribe'
      c.queues["a_queue"].inflight = 10

      c.queues["pop_queue"].type = 'pop'
    end

    @funnel_config = @config.funnels['funnel']
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end

    Jobbable.stub!(:logger).and_return(@logger)

    @amqp = mock("ConfiguredAmqp").tap do |amqp|
      amqp.stub! :subscribe
      amqp.stub! :stop

      # Yielding causes the block to run
      amqp.stub!(:with_connection).and_yield
    end

    @funnel = Jobbable::Funnel.new(@config, @funnel_config).tap do |funnel|
      funnel.amqp = @amqp
    end

    # Ignore some AMQP and EM
    EM.stub!(:run)
    EM.stub!(:stop_event_loop)
    EM.stub!(:schedule)
  end

  it "is an agent" do
    @funnel.should be_a Jobbable::Agent
  end

  it "mixes in subscriber" do
    @funnel.should be_a Jobbable::Amqp::SubscriberMixin
  end

  it "mixes in publisher" do
    @funnel.should be_a Jobbable::Amqp::PublisherMixin
  end

  describe "#on_start" do
    before :each do
      @amqp.stub!(:start)
    end

    it "creates a new amqp connection" do
      @amqp.should_receive(:start)
      @funnel.on_start
    end

    describe "after the connection is established" do
      before :each do
        # Yielding causes the block to run
        @amqp.stub!(:start).and_yield
      end

      it "subscribes to queues, calling perform when messages arrive" do
        @funnel_config.queues = %w{foo bar}

        # Yielding causes the block to run
        @funnel.should_receive(:subscribe).with(
          %w{foo bar}).and_yield(:message)

        @funnel.should_receive(:perform).with(:message)

        @funnel.on_start
      end
    end
  end

  describe "#pending_messages" do
    it "lazily initializes to an empty Array" do
      @funnel.pending_messages.should == []
    end
  end

  describe "#republish" do
    before :each do
      @pending = (1..3).collect do |i|
        mock("pending message #{i}").tap do |message|
          # Each new pending message mock gets a header with key and exchange
          message.stub!(:header).and_return(mock("header #{i}").tap do |h|
            h.stub!(:routing_key).and_return("routing key #{i}")
            h.stub!(:exchange).and_return("exchange #{i}")
          end)

          # ..a body
          message.stub!(:body).and_return("body #{i}")

          # ..and a json-decoded body
          message.stub!(:decoded_body).and_return({"decoded" => "body #{i}"})

          message.stub!(:ack)
        end
      end

      # Add some pending messages to the funnel
      @funnel.pending_messages.clear
      @funnel.pending_messages.concat @pending
    end

    it "publishes pending messages together on the configured exchanges" do
      @funnel.should_receive(:publish).with(
        :key => @funnel_config.key,
        :exchanges => @funnel_config.exchanges,
        :original_keys => @pending.collect { |m| m.header.routing_key },
        :original_exchanges => @pending.collect { |m| m.header.exchange },
        :messages => @pending.collect(&:decoded_body)
      )

      @funnel.republish
    end

    class NativeParseError < StandardError
      include Jobbable::JSON::ParseError
    end

    it "includes undecodable message bodies" do
      @pending[1].should_receive(:decoded_body).and_raise(NativeParseError)

      @funnel.should_receive(:publish).with(hash_including(
        :messages => [
          { "decoded" => "body 1" },
          "body 2",
          { "decoded" => "body 3" }
        ]
      ))

      @funnel.republish
    end

    it "acks and clears the pending messages and counts after publishing" do
      @funnel.message_counts['a_queue'] = 1

      # Publish *first*
      @funnel.should_receive(:publish).ordered

      # Now ack the messages
      @pending.each do |message|
        message.should_receive(:ack).ordered
      end

      @funnel.republish

      @funnel.pending_messages.should be_empty
      @funnel.message_counts.should be_empty
    end

    it "does not publish if there are no pending messages" do
      @funnel.pending_messages.clear
      @funnel.should_not_receive(:publish)
      @funnel.republish
    end
  end

  describe "#on_delivery_timeout" do
    it "republishes" do
      @funnel.should_receive(:republish)
      @funnel.on_delivery_timeout
    end
  end

  describe "#on_heartbeat" do
    it "takes work from pop queues every heartbeat" do
      @funnel.stub!(:pop_queues).and_return(%w{a_pop_queue another_pop_queue})

      @funnel.should_receive(:pop).with('a_pop_queue')
      @funnel.should_receive(:pop).with('another_pop_queue')

      @funnel.on_heartbeat
    end

    it "logs the current counters every heartbeat" do
      @logger.should_receive(:info).with(@funnel.counters)
      @funnel.on_heartbeat
    end
  end

  describe "#message_counts" do
    it "is a Hash" do
      @funnel.message_counts.should be_a Hash
    end

    it "initializes unknown keys to 0" do
      @funnel.message_counts.clear
      @funnel.message_counts[:foo].should == 0
    end
  end

  describe "#bump" do
    before :each do
      @message = mock("message")
      @message.stub!(:queue).and_return("a_queue")

      @pop_message = mock("message")
      @pop_message.stub!(:queue).and_return("pop_queue")
      @pop_message.stub!(:remaining_count).and_return(10)
    end

    it "takes a message and bumps its queue's counter in #message_counts" do
      prior = @funnel.message_counts["a_queue"]
      @funnel.bump @message
      (@funnel.message_counts["a_queue"] - prior).should == 1
    end

    it "also adds the message to #pending_messages" do
      @funnel.bump @message
      @funnel.pending_messages.last.should == @message
    end

    it "holds for subscribe queues whose count is >= their inflight limit" do
      @funnel.message_counts["a_queue"] =
        @config.queues["a_queue"].inflight - 1

      @funnel.bump(@message).should be_true
    end

    it "holds for pop queues whose remaining message count is 0" do
      @pop_message.should_receive(:remaining_count).and_return(0)
      @funnel.bump(@pop_message).should be_true
    end

    it "holds if the total message counts is >= the funnel's max" do
      @funnel.message_counts.clear
      @funnel.message_counts['pop_queue'] = @config.funnels['funnel'].max - 1

      @funnel.bump(@pop_message).should be_true
    end

    it "is false otherwise" do
      @funnel.message_counts.clear
      @funnel.bump(@message).should be_false
    end
  end

  describe "#perform" do
    before :each do
      @header = mock("AMQP message header")
      @body = mock("AMQP message body")

      Jobbable::Amqp::Message.stub!(:new).
        and_return(@message = mock("message"))
    end

    it "acts on the reactor thread" do
      EM.should_receive(:schedule)
      @funnel.perform @message
    end

    describe "on the reactor thread" do
      before :each do
        EM.stub!(:schedule).and_yield
      end

      it "bumps the message" do
        @funnel.should_receive(:bump).with(@message)
        @funnel.perform @message
      end

      it "republishes if bump says so" do
        @funnel.stub!(:bump).and_return(true)
        @funnel.should_receive(:republish)

        @funnel.perform @message
      end

      it "does not republish otherwise" do
        @funnel.stub!(:bump).and_return(false)
        @funnel.should_not_receive(:republish)

        @funnel.perform @message
      end
    end
  end

end
