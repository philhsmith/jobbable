require 'spec_helper'
require 'jobbable/flight'

describe Jobbable::Flight do
  before :each do
    @queue = :a_queue

    @header = mock("AMQP header")
    @header.stub!(:routing_key).and_return('a_job.0')
    @header.stub!(:ack)

    @worker = mock("worker")

    @config = Jobbable::Configuration.new
    @config.jobs['a_job'].keys = ["{job}.{attempt}"]
    @config.jobs['a_job'].timeout = 5
    @worker.stub!(:config).and_return(@config)
    @worker.stub!(:notify)
    @worker.stub!(:publish)

    @counters = Hash.new { |h, k| h[k] = 0 }
    @counters.stub!(:bump)
    @worker.stub!(:counters).and_return(@counters)

    @body = mock("AMQP body")
    @logger = mock("logger").tap do |logger|
      %w{debug info warn error fatal}.each { |s| logger.stub!(s) }
    end
    @worker.stub!(:logger).and_return(@logger)

    @flight = Jobbable::Amqp::Message.new @worker, @queue, @header, @body
    @flight.extend Jobbable::Flight

    # Stub some external calls
    Jobbable::JSON.stub!(:decode).and_return('foo' => 'bar')
    EM::Timer.stub!(:new).and_return(@timer = mock("timer"))
    @timer.stub!(:cancel)
    EM.stub!(:defer)
    EM.stub!(:schedule)
  end

  it "is a message" do
    @flight.should be_a Jobbable::Amqp::Message
  end

  it "mixes in slot" do
    @flight.should be_a Jobbable::Config::SlotMixin
  end

  it "mixes in exception formatter" do
    @flight.should be_a Jobbable::ExceptionFormatter
  end

  describe "#worker" do
    it "is an alias for #subscriber" do
      @flight.worker.should == @flight.subscriber
    end
  end

  describe "#metadata" do
    it "lazy-initializes to #metadata_for(config) with the message's key" do
      @header.stub!(:routing_key).and_return("a_job.rest_of_key")

      @flight.should_receive(:metadata_for).
        with(@config, "a_job.rest_of_key").
        and_return(:slot_values)

      @flight.metadata.should == :slot_values
    end
  end

  describe "#message" do
    it "is the #decoded_body" do
      @flight.should_receive(:decoded_body).and_return(:decoded)
      @flight.message.should == :decoded
    end
  end

  describe "#notify" do
    it "delegates to Worker#notify with itself" do
      @worker.should_receive(:notify).with(@flight, :event)
      @flight.notify(:event)
    end
  end

  describe "#on_ack" do
    it "cancels and clears the timer, if present" do
      @flight.timer = @timer
      @timer.should_receive(:cancel)
      @flight.on_ack
      @flight.timer.should be_nil
    end

    it "notifies observers" do
      @flight.should_receive(:notify).with(:ack)
      @flight.on_ack
    end
  end

  describe "#params" do
    it "is the hash needed to republish this job" do
      @flight.params.should == @flight.metadata.merge(@flight.message)
    end
  end

  describe "#publish" do
    before :each do
      @flight.stub!(:params).and_return(:foo => :bar, :baz => :quux)
      @flight.stub!(:decoded?).and_return(true)
      @flight.stub!(:logger).and_return(@logger)
    end

    it "merges its options into #params before passing off to the client" do
      @flight.should_receive(:params).and_return(:foo => :bar, :baz => :quux)
      @worker.should_receive(:publish).with(
        hash_including :foo => :bar, :baz => :xyzzy)
      @flight.publish :baz => :xyzzy
    end

    it "avoids calling #params if the flight is not yet #decoded?" do
      @flight.should_receive(:decoded?).and_return(false)

      @flight.should_not_receive(:params)
      @flight.should_receive(:body).and_return("the body")
      @flight.should_receive(:metadata).
        and_return(:foo => :bar, :baz => :quux)

      @worker.should_receive(:publish).with(
        hash_including :foo => :bar, :baz => :xyzzy, :message => "the body")
      @flight.publish :baz => :xyzzy
    end

    it "uses the flight's logger" do
      @flight.should_receive(:logger).and_return(@logger)
      @worker.should_receive(:publish).with(hash_including :logger => @logger)
      @flight.publish
    end

    it "resets to state new and attempt 0 if neither are specified" do
      @flight.stub!(:params).and_return(:state => 'retry', :attempt => 3)
      @worker.should_receive(:publish).with(
        hash_including :state => 'new', :attempt => 0)
      @flight.publish
    end

    it "doesn't implicitly reset state or attempt if attempt is specified" do
      @flight.stub!(:params).and_return(:state => 'retry', :attempt => 3)
      @worker.should_receive(:publish).with(
        hash_including :state => 'retry', :attempt => 2)
      @flight.publish :attempt => 2
    end

    it "doesn't implicitly reset state or attempt if state is specified" do
      @flight.stub!(:params).and_return(:state => 'retry', :attempt => 3)
      @worker.should_receive(:publish).with(
        hash_including :state => 'abort', :attempt => 3)
      @flight.publish :state => 'abort'
    end
  end

  describe "#complete" do
    before :each do
      @flight.stub!(:notify)
    end

    it "acks" do
      @flight.should_receive(:ack)
      @flight.complete
    end

    it "notifies observers" do
      @flight.should_receive(:notify).with(:complete)
      @flight.complete
    end
  end

  describe "#crash" do
    before :each do
      @config.exchanges['crash'].name = 'needs_monitoring'
      @flight.stub!(:notify)
    end

    it "publishes the job to the crash exchange" do
      @flight.should_receive(:publish).with(
        hash_including :exchange => 'crash')

      @flight.crash
    end

    it "publishes the job in the crash state" do
      @flight.should_receive(:publish).with(hash_including :state => 'crash')
      @flight.crash
    end

    it "publishes nothing if no crash exchange is configured" do
      @config.exchanges.delete 'crash'
      @worker.should_not_receive(:publish)
      @flight.crash
    end

    it "acks" do
      @flight.should_receive(:ack)
      @flight.crash
    end

    it "includes the given reason, if any, in the republished job" do
      @flight.should_receive(:publish).with(
        hash_including 'reason' => "ROOF FLIES OFF!")

      @flight.crash "ROOF FLIES OFF!"
    end

    it "formats the reason if it is an exception" do
      @flight.should_receive(:format_exception).with(
        "ROOF FLIES OFF!").and_return(:formatted_message)

      @flight.should_receive(:publish).with(
        hash_including 'reason' => :formatted_message)

      @flight.crash "ROOF FLIES OFF!"
    end

    it "notifies observers" do
      @flight.should_receive(:notify).with(:crash)
      @flight.crash
    end
  end

  describe "#abort" do
    before :each do
      @config.exchanges['abort'].name = 'needs_monitoring'
      @flight.stub!(:notify)
    end

    it "publishes the job to the abort exchange" do
      @flight.should_receive(:publish).with(
        hash_including :exchange => 'abort')

      @flight.abort
    end

    it "publishes the job in the abort state" do
      @flight.should_receive(:publish).with(hash_including :state => 'abort')
      @flight.abort
    end

    it "publishes nothing if no abort exchange is configured" do
      @config.exchanges.delete 'abort'
      @worker.should_not_receive(:publish)
      @flight.abort
    end

    it "acks" do
      @flight.should_receive(:ack)
      @flight.abort
    end

    it "includes the given reason, if any, in the republished job" do
      @flight.should_receive(:publish).with(
        hash_including 'reason' => "ROOF FLIES OFF!")

      @flight.abort "ROOF FLIES OFF!"
    end

    it "formats the reason if it is an exception" do
      @flight.should_receive(:format_exception).with(
        "ROOF FLIES OFF!").and_return(:formatted_message)

      @flight.should_receive(:publish).with(
        hash_including 'reason' => :formatted_message)

      @flight.abort "ROOF FLIES OFF!"
    end

    it "notifies observers" do
      @flight.should_receive(:notify).with(:abort)
      @flight.abort
    end
  end

  describe "#reattempt" do
    before :each do
      @config.exchanges['fail'].name = 'needs_monitoring'
      @config.jobs['a_job'].attempts = 3
      @flight.stub!(:notify)
    end

    it "acks" do
      @flight.should_receive(:ack)
      @flight.reattempt
    end

    it "notifies observers" do
      @flight.should_receive(:notify).with(:reattempt)
      @flight.reattempt
    end

    describe "when the job has attempts left" do
      before :each do
        @header.stub!(:routing_key).and_return('a_job.1')
      end

      it "publishes the job to its normal exchanges" do
        @flight.should_receive(:publish).with(hash_not_including :exchange)
        @flight.reattempt
      end

      it "publishes the job in the retry state" do
        @flight.should_receive(:publish).with(
          hash_including :state => 'retry')

        @flight.reattempt
      end

      it "publishes the job on the next attempt" do
        @flight.should_receive(:publish).with(hash_including :attempt => 2)
        @flight.reattempt
      end

      it "passes along any options to #publish" do
        @flight.should_receive(:publish).with(
          hash_including :intermediate => :results)

        @flight.reattempt :intermediate => :results
      end
    end

    describe "when the job has no attempts left" do
      before :each do
        @header.stub!(:routing_key).and_return('a_job.2') # the 3rd attempt
      end

      it "publishes the job to the fail exchange" do
        @flight.should_receive(:publish).with(
          hash_including :exchange => 'fail')

        @flight.reattempt
      end

      it "publishes the job in the fail state" do
        @flight.should_receive(:publish).with(
          hash_including :state => 'fail')

        @flight.reattempt
      end

      it "publishes nothing if no fail exchange is configured" do
        @config.exchanges.delete 'fail'
        @worker.should_not_receive(:publish)
        @flight.reattempt
      end
    end
  end

  describe "#start_timeout" do
    it "starts a timer for the job's timeout" do
      EM::Timer.should_receive(:new).with(
        @config.jobs['a_job'].timeout).and_return(@timer)

      @flight.start_timeout
      @flight.timer.should == @timer
    end

    it "submits the job for retry when the timer expires" do
      # Yielding causes the block to run
      EM::Timer.should_receive(:new).and_yield
      @flight.should_receive(:reattempt)
      @flight.start_timeout
    end
  end

  describe "#defer" do
    before :each do
      # Yielding causes the block to run
      @worker.stub!(:rescue).and_yield

      # Yielding causes the block to run
      EM.stub!(:defer) do |deferred, callback|
        result = deferred.call
        callback.call(result) if callback
      end

      @deferred = mock("deferred")
      @callback = mock("callback")
    end

    it "delegates to Worker#rescue and EM.defer" do
      # Yielding causes the block to run
      @worker.should_receive(:rescue).with(@flight).and_yield

      @deferred.should_receive(:called)
      @callback.should_receive(:called)

      @flight.defer(
        lambda { @deferred.called },
        lambda { |result| @callback.called }
      )
    end

    it "does not call the callback if the deferred block raises exception" do
      # Not yielding emulates the block raising an exception before completion
      @worker.should_receive(:rescue) {}

      @callback.should_not_receive(:called)

      @flight.defer(lambda {}, lambda { |result| @callback.called })
    end

    it "does not pass the result to the callback if it has arity 0" do
      @callback.should_receive(:called)

      lambda do
        @flight.defer(lambda {}, lambda { || @callback.called })
      end.should_not raise_error ArgumentError
    end

    it "does not try to call the callback if none was passed" do
      @deferred.should_receive(:called)

      lambda do
        @flight.defer(lambda { @deferred.called })
      end.should_not raise_error
    end
  end
end
